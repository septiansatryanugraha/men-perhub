
// untuk mendapatkan jwpopup
var jwpopup3 = document.getElementById('jwpopupBox3');

// untuk mendapatkan link untuk membuka jwpopup
var mpLink = document.getElementById("jwpopupLink-login");

// untuk mendapatkan aksi elemen close
var tutup = document.getElementsByClassName("exit3");

// membuka jwpopup ketika link di klik
mpLink.onclick = function() {
    document.getElementById('jwpopupBox3').style.display = "block";
}

// membuka jwpopup ketika elemen di klik
tutup.onclick = function() {
 document.getElementById('jwpopupBox3').style.visibility = "hidden";
}

// membuka jwpopup ketika user melakukan klik diluar area popup
window.onclick = function(event) {
    if (event.target == jwpopup3) {
        jwpopup3.style.display = "none";
    }
}