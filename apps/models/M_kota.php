<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kota extends CI_Model
{
    const __tableName = 'kota';
    const __tableId = 'id_kota';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                provinsi.nama_provinsi as nama_provinsi
                FROM " . self::__tableName . "
                LEFT JOIN provinsi ON provinsi.id_provinsi = " . self::__tableName . ".id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND provinsi.deleted_date IS NULL";
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                provinsi.nama_provinsi as nama_provinsi
                FROM " . self::__tableName . "
                LEFT JOIN provinsi ON provinsi.id_provinsi = " . self::__tableName . ".id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND provinsi.deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select($where = [], $whereIn = [])
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->select('provinsi.nama_provinsi as nama_provinsi');
        $this->db->from(self::__tableName);
        $this->db->join('provinsi', 'provinsi.id_provinsi = ' . self::__tableName . '.id_provinsi', 'left');
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->where('provinsi.deleted_date IS NULL');
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        if (!empty($whereIn)) {
            $this->db->where_in(self::__tableName . '.nama_kota', array_values($whereIn));
        }
        $this->db->order_by(self::__tableName . '.nama_kota', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
