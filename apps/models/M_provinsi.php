<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_provinsi extends CI_Model
{
    const __tableName = 'provinsi';
    const __tableId = 'id_provinsi';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select()
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        if (!empty($whereIn)) {
            $this->db->where_in(self::__tableName . '.nama_provinsi', array_values($whereIn));
        }
        $this->db->order_by(self::__tableName . '.nama_provinsi', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
