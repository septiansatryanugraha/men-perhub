<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_status_kepegawaian extends CI_Model
{
    const __tableName = 'tbl_status_kepegawaian';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".kode ASC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select($where = [], $whereIn = [])
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        if (!empty($whereIn)) {
            $this->db->where_in(self::__tableName . '.nama', array_values($whereIn));
        }
        $this->db->order_by(self::__tableName . '.kode', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
