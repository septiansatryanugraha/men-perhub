<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{

    public function totalPerpanjangan()
    {
        $this->db->from('tbl_perpanjangan');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalDiklat()
    {
        $this->db->from('tbl_diklat');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalJenjang()
    {
        $this->db->from('tbl_kenaikan_jenjang');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function totalUser()
    {
        $this->db->from('tbl_user');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();
        return $data->num_rows();
    }

    function getDataPerpanjangan()
    {
        $sql = "SELECT * FROM tbl_perpanjangan WHERE deleted_date IS NULL ORDER BY id_perpanjangan DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataDiklat()
    {
        $sql = "SELECT * FROM tbl_diklat WHERE deleted_date IS NULL ORDER BY id_diklat DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataJenjang()
    {
        $sql = "SELECT * FROM tbl_kenaikan_jenjang WHERE deleted_date IS NULL ORDER BY id_jenjang DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function updateStatusPerpanjangan()
    {
        $updateQuery = "UPDATE tbl_history_perpanjangan_help SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);
        return $this->db->affected_rows();
    }

    function selectNotifPerpanjangan()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,keterangan_status FROM `tbl_history_perpanjangan_help`
                LEFT JOIN tbl_user ON tbl_history_perpanjangan_help.`id_user` = tbl_user.`id_user`
                WHERE tbl_history_perpanjangan_help.deleted_date IS NULL
                ORDER BY id_history DESC LIMIT 150";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function totalCountPerpanjangan()
    {
        $this->db->from('tbl_history_perpanjangan_help');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();
        return $data->num_rows();
    }

    function updateStatusDiklat()
    {
        $updateQuery = "UPDATE tbl_history_diklat_help SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);
        return $this->db->affected_rows();
    }

    function selectNotifDiklat()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,keterangan_status FROM `tbl_history_diklat_help`
                LEFT JOIN tbl_user ON tbl_history_diklat_help.`id_user` = tbl_user.`id_user`
                WHERE tbl_history_diklat_help.deleted_date IS NULL
                ORDER BY id_history DESC LIMIT 150";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function totalCountDiklat()
    {
        $this->db->from('tbl_history_diklat_help');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();
        return $data->num_rows();
    }

    function updateStatusJenjang()
    {
        $updateQuery = "UPDATE tbl_history_jenjang_help SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);
        return $this->db->affected_rows();
    }

    function selectNotifJenjang()
    {
        $sql = "SELECT tbl_user.photo as photo,tbl_user.nama_lengkap as user,kode_pengajuan,keterangan_status FROM `tbl_history_jenjang_help`
                LEFT JOIN tbl_user ON tbl_history_jenjang_help.`id_user` = tbl_user.`id_user`
                WHERE tbl_history_jenjang_help.deleted_date IS NULL
                ORDER BY id_history DESC LIMIT 150";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function totalCountJenjang()
    {
        $this->db->from('tbl_history_jenjang_help');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();

        return $data->num_rows();
    }

    function updateStatusInbox()
    {
        $updateQuery = "UPDATE tbl_log_email SET status_baca=1 WHERE status_baca=0 ";
        $this->db->query($updateQuery);
        return $this->db->affected_rows();
    }

    function selectNotifInbox()
    {
        $sql = "SELECT * FROM tbl_log_email ORDER BY id_history DESC LIMIT 150 ";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function totalCountInbox()
    {
        $this->db->from('tbl_log_email');
        $this->db->where('status_baca', 0);
        $data = $this->db->get();
        return $data->num_rows();
    }
}
