</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?= $this->config->item('footer_title'); ?>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<script src="<?= theme(); ?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?= theme(); ?>/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="<?= theme(); ?>/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?= theme(); ?>/global/plugins/select2/select2.min.js"></script> -->
<script src="<?= base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?= theme(); ?>/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= theme(); ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="<?= theme(); ?>/global/plugins/jquery-growl/css/jquery.growl.css"/>
<script src="<?= base_url(); ?>assets/plugins/super-treadmill/super-treadmill.js"></script>
<script src="<?= theme(); ?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/assets/scripts/layout.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/assets/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?= theme(); ?>/assets/scripts/demo.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/plugins/bootstrap-summernote/summernote.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ajax.js"></script>
<script src="<?= base_url(); ?>assets/icon-picker/fontawesome-iconpicker.js"></script>
<script src="<?= theme(); ?>/myjs/list-table.js"></script>
<script type="text/javascript">
    var baseurl = "<?= base_url() ?>";
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        ListTable.init();
    });
    // handle ajax link dengan konten
    var ajaxify = [null, null, null];
    $('.page-content-wrapper').on('click', '.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    $('.page-sidebar-menu').on('click', ' li > a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    $('.main-sidebar').on('click', 'a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    $('.navbar-nav').on('click', 'a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    $('.page-logo').on('click', 'a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    // load konten ajax
    var function_ajaxify = function (e, ele) {
        e.preventDefault();
        var url = $(ele).attr("href");
        //var pageContent = $('.page-content');
        var pageContentBody = $('.page-content-wrapper');
        if (url != ajaxify[2]) {
            ajaxify.push(url);
            history.pushState(null, null, url);
        }
        ajaxify = ajaxify.slice(-3, 5);
        $.ajax({
            type: "POST",
            beforeSend: function (xhr) {
                $("#spinner").show();
            },
            cache: false,
            url: url,
            data: {status_link: 'ajax'},
            dataType: "html",
            success: function (res) {
                if (res == 'out') {
                    window.location = base_url + 'login';
                } else {
                    //hide_loading_bar();
                    pageContentBody.html(res);
                    $("#spinner").hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: 'error/error_404',
                    data: {url: ajaxify[1], url1: ajaxify[2]},
                    dataType: "html",
                    success: function (res) {
                        if (res == 'out') {
                            window.location = base_url + 'login';
                        } else {
                            //hide_loading_bar();
                            pageContentBody.html(res);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#spinner").hide();
                    }
                });
            }
        });
    }
</script> 
</html>