<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class WebService extends CI_Controller {

  private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $this->load->model('M_service');
  }

  public function checklogin(){

    $tanggal=date('Y-m-d H:i:s');

    $email = $this->input->post('email');
    $password = base64_encode($this->input->post('password'));

    $res=$this->M_service->login(['email' => $email, 'password' => $password]);
    $email=$this->input->post('email');
    $data2=array(
      'last_login_user' => date('Y-m-d H:i:s')
    );
    $this->M_service->update_user($email,$data2); 
    if($res){
      if($res[0]->status == "Non aktif"){
        $result = array(
          'status'=> 'false',
          'pesan'=> 'Maaf akun belum aktif',
        );
      } 

      else {
        $result = array(
          'status'=> 'true',
          'pesan'=> 'Terima kasih silahkan Login',
          'Profile'=> $res,
        );
      }
    }
    else{
      $result = array(
        'status'=>  'false',
        'pesan'=> 'Maaf email atau no telp anda salah',
      );
    }
    echo json_encode($result);
  }

  public function selek_perpanjangan(){

   $id_user= $this->input->post('id_user');
   $res = $this->M_service->getPerpanjangan($id_user);
   if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false',
     'pesan'=>  'Data masih kosong, belum pernah melakukan pengajuan'
   );  
 } 	
 echo json_encode($result);
}


public function import(){

 $this->load->view('import');

}

public function ProsesImport() {
  $error = false;

  $this->form_validation->set_rules('excel', 'File', 'trim|required');
  if ($_FILES['excel']['name'] == '') {

  } else {
    $config['upload_path'] = './assets/excel/';
    $config['allowed_types'] = 'xls|xlsx';

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('excel')) {
      $error = array('error' => $this->upload->display_errors());
    } else {
      $dataUpload = $this->upload->data();

      error_reporting(E_ALL);
      date_default_timezone_set('Asia/Jakarta');

      include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

      $inputFileName = './assets/excel/' . $dataUpload['file_name'];
      $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
      $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

      $data = array();
      $index = 0;

      if (!$error) {
        foreach($sheetData as $row){
          $date = date('Y-m-d H:i:s');
          $checkFiles = $this->M_service->checkFiles($row['A']);
          if ($checkFiles == false) {
            array_push($data, array(
              'kode_registrasi'  => $row['A'],
              'nip'              => $row['B'],
              'nama'             => $row['C'],
              'instansi'         => $row['D'],
            ));

            $index++;
          }
        }
      }
      unlink('./assets/excel/' . $dataUpload['file_name']);

      if (!$error) {
        if (empty($data)) {
          $out = array('status' => 'awas', 'pesan' => 'Maaf, Data sudah ada dalam sistem silahkan cek kembali data import terakhir !');
          $error = true;
        }
      }

      if (!$error) {
        $result=$this->db->insert_batch('tbl_nrp', $data);
        if ($result > 0) {
          $out = array('status' => true, 'pesan' => ' Data berhasil di import');
        } else {
          $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
        }
      }

    }

    echo json_encode($out);
  }
}


public function selek_diklatpkb()
{
 $id_user= $this->input->post('id_user');
 $res = $this->M_service->diklatPkb($id_user);
 if($res){
  $result = array(
    'status'=>  'true',
    'data'=> $res
  );
} else {
 $result = array(
   'status'=>  'false',
   'pesan'=>  'Data masih kosong, belum pernah melakukan pengajuan'
 );  
} 	
echo json_encode($result);
}

public function selek_knkjenjang()
{
 $id_user= $this->input->post('id_user');
 $res = $this->M_service->knkJenjang($id_user);
 if($res){
  $result = array(
    'status'=>  'true',
    'data'=> $res
  );
} else {
 $result = array(
   'status'=>  'false',
   'pesan'=>  'Data masih kosong, belum pernah melakukan pengajuan'
 );  
} 	
echo json_encode($result);
}



public function selek_count_histori(){
 $id_user= $this->input->post('id_user');
 $res = $this->M_service->totalCountHistory($id_user);
 if($res){
  $result = array(
    'status'=>  'true',
    'data'=> $res
  );
} else {
 $result = array(
   'status'=>  'false',
   'pesan'=>   'Belum count histori pengajuan '
 );  
}   
echo json_encode($result);
}


public function update_histori_count(){
 $id_user= $this->input->post('id_user');
 $res = $this->M_service->updateStatusHistory($id_user);
 if($res){
  $result = array(
    'status'=>  'true',
  );
} else {
 $result = array(
   'status'=>  'false',
 );  
}   
echo json_encode($result);
}


// selek web service dokumen (id)
public function selek_detail_histori(){
 $id_user= $this->input->post('id_user');
 $res = $this->M_service->getHistory($id_user);
 if($res){
  $result = array(
    'status'=>  'true',
    'data'=> $res
  );
} else {
 $result = array(
   'status'=>  'false',
   'pesan'=>   'Belum ada histori pengajuan '
 );  
} 	
echo json_encode($result);
}


// selek web service pengumuman
public function selek_pengumuman(){
  $res = $this->M_service->selek_pengumuman();
  if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false',
     'pesan'=>   'Belum ada pengumuman '
   );  
 } 	
 echo json_encode($result);
}

// selek web service slider
public function selek_menu(){
  $menu = $this->M_service->MenuData();
  if($menu){
    $result = array(
      'status'=>  'true',
      'data'=> $menu,
    );
  } else {
   $result = array(
     'status'=>  'false'
   );  
 }  

 echo json_encode($result);

}



// selek web service slider
public function selek_count(){

  $idUser = $this->input->post('id_user');
  $total_perpanjangan = $this->M_service->totalData('id_user', $idUser);
  $total_diklat = $this->M_service->totalData2('id_user', $idUser);
  $total_jenjang = $this->M_service->totalData3('id_user', $idUser);
  $result = array(
    'status'=>  'true',
    'total_perpanjangan'=> $total_perpanjangan,
    'total_diklat' => $total_diklat,
    'total_jenjang' => $total_jenjang,
  );
  echo json_encode($result);
}


// selek web service slider
public function selek_slider(){
  $res = $this->M_service->selek_slider();
  if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false'
   );  
 } 	
 echo json_encode($result);
}



// selek web service berita
public function selek_berita(){
  $res = $this->M_service->selek_berita();
  if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false'
   );  
 } 	
 echo json_encode($result);
}


function set_profil(){ 

  $where = trim($this->input->post('id_user'));
  $res = $this->M_service->select_user($where);

  if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false'
   );  
 }  
 echo json_encode($result);
}

function GetDataPerpanjangan(){ 

  $where = trim($this->input->post('id_user'));
  $res = $this->M_service->GetDataPerpanjangan($where);

  if($res){
    $result = array(
      'status'=>  'true',
      'data'=> $res
    );
  } else {
   $result = array(
     'status'=>  'false'
   );  
 }  
 echo json_encode($result);
}



// untuk edit profile user
function edit_profil(){

  $where = trim($this->input->post('id_user'));

  $data = array(
   'nama_lengkap'            => $this->input->post('nama_lengkap'),
   'nomor_hp'                => $this->input->post('nomor_hp'),
 );

  $res = $this->db->update('tbl_user', $data, array('id_user' => $where));

  if($res){
   $result = array(
     'status'=>  'true',
     'pesan' =>  'Profil berhasil di update',
   );
 }
 else
 {
   $result = array(
     'status'=>  'false',
     'pesan' =>  'Profil gagal di update',
   );  
 }

 echo json_encode($result);
}


public function PengajuanPerpanjangan()
{
  $datetime = date('Y-m-d H:i:s');
  $date = date('Y-m-d');
  $namaLengkap = $this->input->post('nama_lengkap');

  $errCode = 0;
  $errMessage = "";

  $idJenisJenjang = $this->input->post('id_jenis_jenjang');

  if ($errCode == 0) {
    if (strlen($idJenisJenjang) == 0) {
      $errCode++;
      $errMessage = "Jenis Jenjang wajib di isi.";
    }
  }
  if ($errCode == 0) {
    $checkJenisJenjang = $this->M_service->selectById($idJenisJenjang);
    if ($checkJenisJenjang == null) {
      $errCode++;
      $errMessage = "Jenis Jenjang tidak valid.";
    }
  }
  if ($errCode == 0) {
    try {
      $kode = $this->M_service->getNextPerpanjangan();

      $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

      $data = [
        'id_user' => $this->input->post('id_user'),
        'id_jenis_jenjang' => $idJenisJenjang,
        'kode_pengajuan' => $kode,
        'email' => $this->input->post('email'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses perpanjangan kompetensi tingkat ' . $checkJenisJenjang->jenjang . ' ke sistem E-Kompetensi',
        'folder' => $folder['date'] . '/' . $folder['code'],
        'status_perpanjangan' => 'Pending',
        'tgl_ba' => $date,
        'tgl_surat' => $date,
        'tanggal' => $date,
        'created_date' => $datetime,
        'created_by' => $namaLengkap,
        'updated_date' => $datetime,
        'updated_by' => $namaLengkap,
      ];
      $result = $this->db->insert('tbl_perpanjangan', $data);

      $data2 = [
        'kode_pengajuan' => $kode,
        'id_user' => $this->input->post('id_user'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses perpanjangan kompetensi tingkat ' . $checkJenisJenjang->jenjang . ' ke sistem E-Kompetensi',
        'status' => 'Pending',
        'created_by' => 'System',
        'created_date' => $datetime,
      ];
      $result = $this->db->insert('tbl_history', $data2);

      $this->doUploadOthersImagesPerpanjangan($folder);
    } catch (Exception $ex) {
      $errCode++;
      $errMessage = $ex->getMessage();
    }
  }
  if ($errCode == 0) {
    if ($this->db->trans_status() === FALSE) {
      $errCode++;
      $errMessage = "Error saving databse.";
    }
  }

  if ($errCode == 0) {
    $this->db->trans_commit();
    $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
  } else {
    $this->db->trans_rollback();
    $out = ['status' => false, 'pesan' => $errMessage];
  }

  echo json_encode($out);
}


private function doUploadOthersImagesPerpanjangan($folder = [])
{
  $upath = './upload/berkas_peningkatan_jenjang/' . $folder['date'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }
  $upath = './upload/berkas_peningkatan_jenjang/' . $folder['date'] . '/' . $folder['code'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }

  $this->load->library('upload');

  $files = $_FILES;
  $cpt = count($_FILES['others']['name']);
  for ($i = 0; $i < $cpt; $i++) {
    unset($_FILES);
    $_FILES['others']['name'] = $files['others']['name'][$i];
    $_FILES['others']['type'] = $files['others']['type'][$i];
    $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
    $_FILES['others']['error'] = $files['others']['error'][$i];
    $_FILES['others']['size'] = $files['others']['size'][$i];
    $this->upload->initialize(array(
      'upload_path' => $upath,
      'allowed_types' => $this->allowed_img_types
    ));
    $this->upload->do_upload('others');
  }
}

public function PengajuanDiklat()
{
  $datetime = date('Y-m-d H:i:s');
  $date = date('Y-m-d');
  $namaLengkap = $this->input->post('nama_lengkap');

  $errCode = 0;
  $errMessage = "";

  $idKategoriDiklat = $this->input->post('id_kategori_diklat');

  if ($errCode == 0) {
    if (strlen($idKategoriDiklat) == 0) {
      $errCode++;
      $errMessage = "Jenis Diklat wajib di isi.";
    }
  }
  if ($errCode == 0) {
    $checkKategoriDiklat = $this->M_service->selectByIdDiklat($idKategoriDiklat);
    if ($checkKategoriDiklat == null) {
      $errCode++;
      $errMessage = "Jenis Diklat tidak valid.";
    }
  }
  if ($errCode == 0) {
    try {
      $kode = $this->M_service->getNextDiklatPkb();

      $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

      $data = [
        'id_user' => $this->input->post('id_user'),
        'id_kategori_diklat' => $idKategoriDiklat,
        'kode_pengajuan' => $kode,
        'email' => $this->input->post('email'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses pengajuan untuk mengikuti Diklat ' . $checkKategoriDiklat->kategori_diklat . ' ke sistem E-Kompetensi',
        'folder' => $folder['date'] . '/' . $folder['code'],
        'status_pengajuan' => 'Pending',
        'tgl_ba' => $date,
        'tgl_surat' => $date,
        'tanggal' => $date,
        'created_date' => $datetime,
        'created_by' => $namaLengkap,
        'updated_date' => $datetime,
        'updated_by' => $namaLengkap,
      ];
      $result = $this->db->insert('tbl_diklat', $data);

      $data2 = [
        'kode_pengajuan' => $kode,
        'id_user' => $this->session->userdata('id'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan proses pengajuan untuk mengikuti Diklat ' . $checkKategoriDiklat->kategori_diklat . ' ke sistem E-Kompetensi',
        'status' => 'Pending',
        'created_by' => 'System',
        'created_date' => $datetime,
      ];
      $result = $this->db->insert('tbl_history', $data2);
      $this->doUploadOthersDiklat($folder);
    } catch (Exception $ex) {
      $errCode++;
      $errMessage = $ex->getMessage();
    }
  }
  if ($errCode == 0) {
    if ($this->db->trans_status() === FALSE) {
      $errCode++;
      $errMessage = "Error saving databse.";
    }
  }

  if ($errCode == 0) {
    $this->db->trans_commit();
    $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
  } else {
    $this->db->trans_rollback();
    $out = ['status' => false, 'pesan' => $errMessage];
  }

  echo json_encode($out);
}


private function doUploadOthersDiklat($folder = [])
{
  $upath = './upload/berkas_diklat/' . $folder['date'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }
  $upath = './upload/berkas_diklat/' . $folder['date'] . '/' . $folder['code'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }

  $this->load->library('upload');

  $files = $_FILES;
  $cpt = count($_FILES['others']['name']);
  for ($i = 0; $i < $cpt; $i++) {
    unset($_FILES);
    $_FILES['others']['name'] = $files['others']['name'][$i];
    $_FILES['others']['type'] = $files['others']['type'][$i];
    $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
    $_FILES['others']['error'] = $files['others']['error'][$i];
    $_FILES['others']['size'] = $files['others']['size'][$i];

    $this->upload->initialize(array(
      'upload_path' => $upath,
      'allowed_types' => $this->allowed_img_types
    ));
    $this->upload->do_upload('others');
  }
}



public function PengajuanJenjang()
{
  $datetime = date('Y-m-d H:i:s');
  $date = date('Y-m-d');
  $namaLengkap = $this->input->post('nama_lengkap');

  $errCode = 0;
  $errMessage = "";

  $idJenisJenjang = $this->input->post('id_jenis_jenjang');

  if ($errCode == 0) {
    if (strlen($idJenisJenjang) == 0) {
      $errCode++;
      $errMessage = "Jenis Jenjang wajib di isi.";
    }
  }
  if ($errCode == 0) {
    $checkJenisJenjang = $this->M_service->selectByIdJenjang($idJenisJenjang);
    if ($checkJenisJenjang == null) {
      $errCode++;
      $errMessage = "Jenis Jenjang tidak valid.";
    }
  }
  if ($errCode == 0) {
    try {
      $kode = $this->M_service->getNextPeningkatanJenjang();

      $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

      $data = [
        'id_user' => $this->input->post('id_user'),
        'id_jenis_jenjang' => $idJenisJenjang,
        'kode_pengajuan' => $kode,
        'email' => $this->input->post('email'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan pengajuan untuk mengikuti proses Kenaikan Jenjang ' . $checkJenisJenjang->jenjang . ' ke sistem E-Kompetensi',
        'folder' => $folder['date'] . '/' . $folder['code'],
        'status_pengajuan' => 'Pending',
        'tgl_ba' => $date,
        'tgl_surat' => $date,
        'tanggal' => $date,
        'created_date' => $datetime,
        'created_by' => $namaLengkap,
        'updated_date' => $datetime,
        'updated_by' => $namaLengkap,
      ];
      $result = $this->db->insert('tbl_kenaikan_jenjang', $data);

      $data2 = [
        'kode_pengajuan' => $kode,
        'id_user' => $this->session->userdata('id'),
        'keterangan_status' => 'user <b>' . $namaLengkap . '</b> Sedang melakukan pengajuan untuk mengikuti proses Kenaikan Jenjang ' . $checkJenisJenjang->jenjang . ' ke sistem E-Kompetensi',
        'status' => 'Pending',
        'created_by' => 'System',
        'created_date' => $datetime,
      ];
      $result = $this->db->insert('tbl_history', $data2);

      $this->doUploadOthersJenjang($folder);
    } catch (Exception $ex) {
      $errCode++;
      $errMessage = $ex->getMessage();
    }
  }
  if ($errCode == 0) {
    if ($this->db->trans_status() === FALSE) {
      $errCode++;
      $errMessage = "Error saving databse.";
    }
  }

  if ($errCode == 0) {
    $this->db->trans_commit();
    $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
  } else {
    $this->db->trans_rollback();
    $out = ['status' => false, 'pesan' => $errMessage];
  }

  echo json_encode($out);
}


private function doUploadOthersJenjang($folder = [])
{
  $upath = './upload/berkas_peningkatan_jenjang/' . $folder['date'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }
  $upath = './upload/berkas_peningkatan_jenjang/' . $folder['date'] . '/' . $folder['code'] . '/';
  if (!file_exists($upath)) {
    mkdir($upath, 0777);
  }

  $this->load->library('upload');

  $files = $_FILES;
  $cpt = count($_FILES['others']['name']);
  for ($i = 0; $i < $cpt; $i++) {
    unset($_FILES);
    $_FILES['others']['name'] = $files['others']['name'][$i];
    $_FILES['others']['type'] = $files['others']['type'][$i];
    $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
    $_FILES['others']['error'] = $files['others']['error'][$i];
    $_FILES['others']['size'] = $files['others']['size'][$i];

    $this->upload->initialize(array(
      'upload_path' => $upath,
      'allowed_types' => $this->allowed_img_types
    ));
    $this->upload->do_upload('others');
  }
}






}
