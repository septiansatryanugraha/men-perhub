<?php
class M_service extends CI_Model{

	const __tableName = 'jenis_jenjang';
    const __tableId = 'id';

    const __tableName2 = 'tbl_kategori_diklat';
    const __tableId2 = 'id_kategori_diklat';

    const __tableName3 = 'jenis_jenjang';
    const __tableId3 = 'id';

    const __tableNamePerpanjangan = 'tbl_perpanjangan';
    const __tableNameDiklat = 'tbl_diklat';
    const __tableNameJenjang = 'tbl_kenaikan_jenjang';

   	
	public function checkExistingUser($filter = [])
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        foreach ($filter as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->where('deleted_date', null);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

     public function checkFiles($value)
    {
        $this->db->select('kode_registrasi');
        $this->db->from('tbl_nrp');
        $this->db->like('kode_registrasi', $value);
        // $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function login($filter = [])
    {
        return $this->checkExistingUser($filter);
    }

    public function MenuData() {
        $sql = " SELECT * FROM tbl_config_menu";
        $data = $this->db->query($sql);
        return $data->result();
    }


    function totalData($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableNamePerpanjangan . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableNamePerpanjangan);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    function totalData2($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableNameDiklat . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableNameDiklat);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    function totalData3($column, $value)
    {
        $this->db->where($column, $value);
        $this->db->where(self::__tableNameJenjang . '.deleted_date IS NULL');
        $query = $this->db->get(self::__tableNameJenjang);
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function select_user($id_user){
        $sql = "SELECT * FROM tbl_user WHERE id_user='$id_user'";
        $query = $this->db->query($sql);
        return $query->row();
    }

	public function selek_slider() {
		
		$sql = " SELECT * FROM tbl_slider WHERE status ='Aktif' AND deleted_date IS NULL order by id DESC limit 5";
		$data = $this->db->query($sql);
		return $data->result();
	}

	public function selek_pengumuman() {
		
		$sql = " SELECT * FROM tbl_broadcast WHERE deleted_date IS NULL order by id_broadcast DESC limit 8";
		$data = $this->db->query($sql);
		return $data->result();
	}

	public function selek_berita() {
		
		$sql = " SELECT * FROM tbl_news WHERE status ='Aktif' AND deleted_date IS NULL order by id_news desc LIMIT 6 ";
		$data = $this->db->query($sql);
		return $data->result();
	}
	
	function update_user($id,$data2)
    {
        $this->db->where('email', $id);
        $this->db->update('tbl_login', $data2);
        return TRUE;
    }

      public function getDokumen($id_user){
		$sql = "SELECT * FROM tbl_dokumen WHERE id_user='$id_user' order by id_dokumen desc";
		$query = $this->db->query($sql);
		return $query->result();
	}

	
	public function getHistory($id_user){
		$sql = "SELECT * FROM tbl_history WHERE id_user='$id_user' order by id_history desc";
		$query = $this->db->query($sql);
		return $query->result();
	}

    public function totalCountHistory($id_user)
    {
        $data = $this->db
            ->where('deleted_date IS NULL')
            ->where('id_user=', $id_user)
            ->where('status_baca=', 0)
            ->get('tbl_history');
        return $data->num_rows();
    }

    function updateStatusHistory($id)
    {
        $update_query = "UPDATE tbl_history SET status_baca=1 WHERE status_baca=0 AND id_user = '{$id}'";
        $this->db->query($update_query);
        return $this->db->affected_rows();
    }

	public function getPerpanjangan($id_user){
		$sql = "SELECT * FROM  tbl_perpanjangan WHERE id_user='$id_user'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function diklatPkb($id_user){
		$sql = "SELECT * FROM  tbl_diklat WHERE id_user='$id_user'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function knkJenjang($id_user){
		$sql = "SELECT * FROM  tbl_kenaikan_jenjang WHERE id_user='$id_user'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function selectByIdPerpanjangan($id)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByIdDiklat($id)
    {
        $sql = "SELECT " . self::__tableName2 . ".*
                FROM " . self::__tableName2 . "
                WHERE " . self::__tableName2 . ".deleted_date IS NULL
                AND " . self::__tableName2 . "." . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByIdJenjang($id)
    {
        $sql = "SELECT " . self::__tableName3 . ".*
                FROM " . self::__tableName3 . "
                WHERE " . self::__tableName3 . ".deleted_date IS NULL
                AND " . self::__tableName3 . "." . self::__tableId3 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function _getNextCodeFromFormat($keyFormat)
    {
        $keyCounter = self::_getKeyCounter($keyFormat);
        $result = $keyCounter;
        $counter = self::_getNextCounter($keyCounter);
        preg_match_all("/{([\w]*)}/", $keyFormat, $matches, PREG_SET_ORDER);
        foreach ($matches as $val) {
            $str = $val[1]; //matches str without bracket {}
            $bStr = $val[0]; //matches str with bracket {}
            $lenCounter = strlen($str);
            $padCounter = str_pad($counter, $lenCounter, "0", STR_PAD_LEFT);
            $result = str_replace($bStr, $padCounter, $result);
        }
        return $result;
    }

    public function _getKeyCounter($keyFormat)
    {
        $result = $keyFormat;
        preg_match_all("/{([\w]*)}/", $keyFormat, $matches, PREG_SET_ORDER);

        foreach ($matches as $val) {
            $str = $val[1]; //matches str without bracket {}
            $bStr = $val[0]; //matches str with bracket {}
            switch ($str) {
                case "yyyy":
                    $result = str_replace("{yyyy}", date('Y'), $result);
                    break;
                case "yy":
                    $result = str_replace("{yy}", date('y'), $result);
                    break;
                case "mm":
                    $result = str_replace("{mm}", date('m'), $result);
                    break;
                case "dd":
                    $result = str_replace("{dd}", date('d'), $result);
                    break;
                case "MM":
                    $result = str_replace("{MM}", cutils::month_romawi(date('m')), $result);
                    break;
            }
        }
        return $result;
    }

    public function _getNextCounter($keyCounter)
    {
        $nextCounter = 1;
        $isInsert = 1;

        $sql = "SELECT CASE WHEN counter IS NULL THEN 1 ELSE counter+1 END AS next_counter FROM sys_counter WHERE deleted_date IS NULL AND `key`= '{$keyCounter}'";
        $r = $this->db->query($sql)->row();

        if ($r != null) {
            $nextCounter = $r->next_counter;
            $isInsert = 0;
        }
        $cmd = "";
        if ($isInsert == 1) {
            $cmd = "INSERT INTO sys_counter(`key`,counter,created_date) VALUES ('{$keyCounter}',1,now());";
        } else {
            $cmd = "UPDATE sys_counter SET counter = counter+1, updated_date = now() WHERE `key` = '{$keyCounter}'";
        }
        $this->db->query($cmd);

        return $nextCounter;
    }

     public function getNextPerpanjangan()
    {
        $prefix = "PPJ";
        $prefix .= date('dmy') . "{nnn}";
        return self::_getNextCodeFromFormat($prefix);
    }

    public function getNextDiklatPkb()
    {
        $prefix = "DKT";
        $prefix .= date('dmy') . "{nnn}";
        return self::_getNextCodeFromFormat($prefix);
    }

    public function getNextPeningkatanJenjang()
    {
        $prefix = "KNJ";
        $prefix .= date('dmy') . "{nnn}";
        return self::_getNextCodeFromFormat($prefix);
    }
	
	
	
	
}
?>