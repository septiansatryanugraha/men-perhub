<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap_user extends CI_Model
{
    const __tableName = 'tbl_user';
    const __tableId = 'id_user';

    public function exportData($filter = [])
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*
                , jenis_jenjang.jenjang as jenis_jenjang
                , kota.nama_kota as nama_kota
                , tbl_status_kepegawaian.nama as status_kepegawaian
                FROM " . self::__tableName . "
                LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName . ".id_jenis_jenjang
                LEFT JOIN kota ON kota.id_kota = " . self::__tableName . ".id_kota
                LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = " . self::__tableName . ".id_status_kepegawaian
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".tanggal >= '{$tanggalAwal}' AND " . self::__tableName . ".tanggal <= '{$tanggalAkhir}'";
            }
        }
        $data = $this->db->query($sql);

        return $data->result();
    }
}
