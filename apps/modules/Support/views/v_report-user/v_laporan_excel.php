<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table><tr><td colspan="11" style="text-align: center; vertical-align: middle;"><h2>Report Data User Penguji</h2></td></tr></table>
<br>
<table>
    <tr><td colspan="11">Filter</td></tr>
    <tr>
        <td></td>
        <td>Tanggal Pendaftaran<span class="pull right">:</span></td>
        <td colspan="9" style="text-align: left;"><?= ($allDate > 0) ? "Semua Periode Pendaftaran" : date('d-m-Y', strtotime($tanggalAwal)) . " - " . date('d-m-Y', strtotime($tanggalAkhir)); ?></td>
    </tr>
</table>
<br><br>
<table border="1" width="80%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">No Registrasi</th>
            <th align="center">Nama</th>
            <th align="center">Email</th>
            <th align="center">Jenis Kelamin</th>
            <th align="center">Status Kepegawaian</th>
            <th align="center">Tempat / Tanggal Lahir</th>
            <th align="center">Instansi</th>
            <th align="center">Asal Instansi</th>
            <th align="center">Alamat Instansi</th>
            <th align="center">Tanggal Pendaftaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excel)) {
            foreach ($excel as $key => $data) {

                ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $data->no_reg ?></td>
                    <td><?= $data->nama_lengkap ?></td>
                    <td><?= $data->email ?></td>
                    <td><?= $data->jkel ?></td>
                    <td><?= $data->status_kepegawaian ?></td>
                    <td><?= $data->tempat_lahir . ", " . date('d-m-Y', strtotime($data->tgl_lahir)); ?>&nbsp;</td>
                    <td><?= $data->instansi ?></td>
                    <td><?= $data->asal_instansi ?></td>
                    <td><?= $data->alamat_instansi ?></td>
                    <td><?= date('d-m-Y', strtotime($data->tanggal)); ?>&nbsp;</td>
                </tr>    
                <?php
            }
        } else {

            ?>
            <tr><td colspan="11">Belum Ada Data</td></tr>
        <?php } ?>
    </tbody>
</table>