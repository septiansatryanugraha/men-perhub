<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MX_Controller
{
    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_publik');
        $this->load->model('M_provinsi');
        $this->load->model('M_kota');
        $this->load->model('M_jenis_jenjang');
        $this->load->model('M_status_kepegawaian');
        $this->load->model('M_generate_code');
        $this->load->model('M_kategori_status');
        $this->load->library(array('pagination'));
    }

    public function index()
    {

        if ($this->lib->login() != "") {
            redirect('logout-opd');
        } else {
            $this->load->model('M_publik');
            $data = [
                'posting' => $this->M_publik->selek_history(),
                'news' => $this->M_publik->selek_news(),
                'slider' => $this->M_publik->selek_slider(),
                'kontak' => $this->M_publik->selek_kontak(),
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'title' => "Homepage",
                'page' => "home",
            ];

            $this->load->view('front/header', $data);
            $this->load->view('index', $data);
            $this->load->view('front/footer', $data);
        }
    }


    function cekNoKey()
    {
      $nokey = $_POST['nokey'];

      if ($this->lib->login() != "") {
        redirect('');
    } else {
        $data = [
            'title' => 'Daftar akun - E-Kopetensi',
            'jkel' => $this->M_kategori_status->select([], ['Pria', 'Wanita']),
            'status_pegawai' => $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']),
            'instansi' => $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']),
            'provinsi' => $this->M_provinsi->select(),
            'HasilKey' => $this->M_publik->cekNoKey($nokey),
            'jenisJenjang' => $this->M_jenis_jenjang->select(),
            'statusKepegawaian' => $this->M_status_kepegawaian->select(),
        ];
        $this->load->view('front/header', $data);
        $this->load->view('register-nrp');
        $this->load->view('front/footer', $data);
    }

}


 function cekNoKeyAndroid()
    {
      $nokey = $_POST['nokey'];

      if ($this->lib->login() != "") {
        redirect('');
    } else {
        $data = [
            'title' => 'Daftar akun - E-Kopetensi',
            'jkel' => $this->M_kategori_status->select([], ['Pria', 'Wanita']),
            'status_pegawai' => $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']),
            'instansi' => $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']),
            'provinsi' => $this->M_provinsi->select(),
            'HasilKey' => $this->M_publik->cekNoKey($nokey),
            'jenisJenjang' => $this->M_jenis_jenjang->select(),
            'statusKepegawaian' => $this->M_status_kepegawaian->select(),
        ];
        $this->load->view('front/headerAndroid', $data);
        $this->load->view('register-android-nrp');
        $this->load->view('front/footerAndroid', $data);
    }

}

function ambilData(){

    $modul=$this->input->post('modul');
    $id=$this->input->post('id');

    if($modul=="nama_lengkap"){
        echo $this->M_publik->selekNama($id);
    }

    if($modul=="asal_instansi"){
        echo $this->M_publik->selekInstansi($id);
    }

    if($modul=="kode_registrasi"){
        echo $this->M_publik->selekKdRegistrasi($id);
    }

    if($modul=="nip"){
        echo $this->M_publik->selekNip($id);
    }


}

public function detail_berita($permalink)
{

    $data = array(
        'berita' => $this->M_publik->selek_news(),
            'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(),
            'berita_random' => $this->M_publik->selekBeritaRandom(),
            'detail_berita' => $this->M_publik->getberita_id($permalink),
            'title' => "Detail Berita",
            'page' => 'berita',
        );

    $this->load->view('front/header', $data);
    $this->load->view('detail-berita', $data);
    $this->load->view('front/footer', $data);
}

public function login()
{

    if ($this->lib->login() != "") {
        redirect('logout-opd');
    } else {
        $this->load->model('M_publik');
        $data = [
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'title' => "Login",
                'page' => "login",
            ];

            $this->load->view('front/header', $data);
            $this->load->view('login', $data);
            $this->load->view('front/footer', $data);
        }
    }

    public function lupa_password()
    {
        $data = array(
            'title' => 'Lupa Password - E-Kompetensi',
        );

        $this->load->view('front/header', $data);
        $this->load->view('lupa_password');
        $this->load->view('front/footer', $data);
    }

    public function info($id = 0, $backbtn = 0)
    {
        $data = [
            'history' => $this->M_publik->selek_by_id($id),
        ];
        $data["backbutton"] = $backbtn;
        $this->load->view('detail', $data);
    }


    public function DiklatPKBLolos()
    {

        $list = $this->M_publik->getDataDiklat(1,['Lolos']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->instansi_pemohon.'<br>('.$brand->kota_pemohon.')';
            $row[] = $brand->asal_instansi_pemohon;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function DiklatPKBLulus()
    {

        $list = $this->M_publik->getDataDiklat(1,['Lulus']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->nilai;
            $row[] = $brand->instansi_pemohon.'<br>('.$brand->kota_pemohon.')';
            $row[] = $brand->asal_instansi_pemohon;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function JenjangLolos()
    {

        $list = $this->M_publik->getDataJenjang(1,['Lolos']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->instansi_pemohon.'<br>('.$brand->kota_pemohon.')';
            $row[] = $brand->asal_instansi_pemohon;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }


    public function JenjangLulus()
    {

        $list = $this->M_publik->getDataJenjang(1,['Lulus']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->nilai;
            $row[] = $brand->instansi_pemohon.'<br>('.$brand->kota_pemohon.')';
            $row[] = $brand->asal_instansi_pemohon;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function login_user()
    {

        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('g-recaptcha-response', '<strong>Captcha</strong>', 'callback_getResponseCaptcha');
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if ($this->form_validation->run() == TRUE || !isset($response['success']) ||
            $response['success'] == true) {
            $email = $this->input->post('email');
        $password = base64_encode($this->input->post('password'));
        $check = $this->M_publik->login(['email' => $email], ['password' => $password]);

        if ($check == TRUE) {
            foreach ($check as $user) {
                if ($user->status == "Non aktif") {
                    $json['pesan'] = "Maaf akun belum aktif.";
                } else if ($user->status_helper == "Blokir") {
                    $json['pesan'] = "Maaf akun terblokir.";
                } else {
                    $this->session->set_userdata([
                        'email' => $user->email,
                        'id' => $user->id_user,
                        'id_jenis_jenjang' => $user->id_jenis_jenjang,
                        'id_status_kepegawaian' => $user->id_status_kepegawaian,
                        'nama_lengkap' => $user->nama_lengkap,
                        'nama' => $user->nama,
                        'password' => $user->password
                    ]);

                    $data2 = [
                        'last_login_user' => date('Y-m-d H:i:s')
                    ];

                    $this->M_publik->update_user($email, $data2);
                    $this->M_publik->update_user2($email, $data2);
                    $URL_home = base_url('beranda');
                    $json['status'] = true;
                    $json['url_home'] = $URL_home;
                    $json['pesan'] = "Success Login.";
                }
            }
        } else {
            $json['pesan'] = "Username / Password salah";
        }
    } elseif (isset($recaptcha)) {
        $json['pesan'] = "Captcha harus di isi";
    } else {
        $json['pesan'] = "Maaf tidak bisa login, silahkan cek user dan password anda";
    }

    echo json_encode($json);
}

public function logout()
{
    if ($this->lib->login() != "") {
        $this->lib->logout();
        redirect('login-user');
    } else {
        redirect('login-user');
    }
}

public function register()
{
    if ($this->lib->login() != "") {
        redirect('');
    } else {
        $data = [
            'title' => 'Daftar akun - E-Kopetensi',
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'jkel' => $this->M_kategori_status->select([], ['Pria', 'Wanita']),
                'status_pegawai' => $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']),
                'instansi' => $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']),
                'provinsi' => $this->M_provinsi->select(),
                'jenisJenjang' => $this->M_jenis_jenjang->select(),
                'statusKepegawaian' => $this->M_status_kepegawaian->select(),
            ];
            $this->load->view('front/header', $data);
            $this->load->view('register');
            $this->load->view('front/footer', $data);
        }
    }


    public function registerAndroid()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = [
                'title' => 'Daftar akun - E-Kopetensi',
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'jkel' => $this->M_kategori_status->select([], ['Pria', 'Wanita']),
                'status_pegawai' => $this->M_kategori_status->select([], ['PNS', 'P3K', 'NON ASN']),
                'instansi' => $this->M_kategori_status->select([], ['Dishub Provinsi', 'Dishub Kabupaten', 'Dishub Kota', 'Ditjen Perhubungan Darat', 'Lembaga Diklat']),
                'provinsi' => $this->M_provinsi->select(),
                'jenisJenjang' => $this->M_jenis_jenjang->select(),
                'statusKepegawaian' => $this->M_status_kepegawaian->select(),
            ];
            $this->load->view('front/headerAndroid', $data);
            $this->load->view('register-android');
            $this->load->view('front/footerAndroid', $data);
        }
    }


    public function email()
    {
        $this->load->view('email-sukses');
    }

    public function email2()
    {
        $data['nama'] = 'Moch Wahyu Sugiarto';
        $this->load->view('email',$data);
    }


    public function prosesDaftar()
    {
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();

        $idProvinsi = $this->input->post('id_provinsi');
        $idKota = $this->input->post('id_kota');
        $idJenisJenjang = $this->input->post('id_jenis_jenjang');
        $idStatusKepegawaian = $this->input->post('id_status_kepegawaian');
        $kode = $this->input->post('kode');
        $folder = $this->input->post('folder');

        $type = $this->input->post('type');
        $jkel = $this->input->post('jkel');
        $statusPegawai = $this->input->post('status_pegawai');
        $namaLengkap = trim($this->input->post('nama_lengkap'));
        $NIK = trim($this->input->post('nik'));
        $tempatLahir = $this->input->post('tempat_lahir');
        $tglLahir = $this->input->post('tgl_lahir');
        $noHp = $this->input->post('nomor_hp');
        $asalInstansi = $this->input->post('asal_instansi');
        $email = $this->input->post('emails');
        $instansi = $this->input->post('instansi');
        $alamatInstansi = $this->input->post('alamat_instansi');
        $password = $this->input->post('password');

        if ($errCode == 0) {
            if (strlen($namaLengkap) == 0) {
                $errCode++;
                $errMessage = "Nama Lengkap wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($NIK) == 0) {
                $errCode++;
                $errMessage = "NIK wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($jkel) == 0) {
                $errCode++;
                $errMessage = "Jenis Kelamin wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPegawai) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tempatLahir) == 0) {
                $errCode++;
                $errMessage = "Tempat Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglLahir) == 0) {
                $errCode++;
                $errMessage = "Tanggal Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Format Email salah.";
            }
        }
        if ($errCode == 0) {
            $checkuser = $this->M_publik->checkUsername(['email' => $email]);
            if ($checkuser) {
                $errCode++;
                $errMessage = "Akun email sudah terdaftar.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noHp) == 0) {
                $errCode++;
                $errMessage = "Nomor HP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($instansi) == 0) {
                $errCode++;
                $errMessage = "Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($asalInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idProvinsi) == 0) {
                $errCode++;
                $errMessage = "Provinsi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkProvinsi = $this->M_provinsi->selectById($idProvinsi);
            if ($checkProvinsi == null) {
                $errCode++;
                $errMessage = "Provinsi tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKota) == 0) {
                $errCode++;
                $errMessage = "Kota wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKota = $this->M_kota->selectById($idKota);
            if ($checkKota == null) {
                $errCode++;
                $errMessage = "Kota tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idJenisJenjang) == 0) {
                $errCode++;
                $errMessage = "Jenjang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkJenisJenjang = $this->M_jenis_jenjang->selectById($idJenisJenjang);
            if ($checkJenisJenjang == null) {
                $errCode++;
                $errMessage = "Jenjang tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idStatusKepegawaian) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkStatusKepegawaian = $this->M_status_kepegawaian->selectById($idStatusKepegawaian);
            if ($checkStatusKepegawaian == null) {
                $errCode++;
                $errMessage = "Status Kepegawaian tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = '"Sudah Punya No Registrasi" wajib di isi.';
            }
        }
        if ($errCode == 0) {
            if ($type == 'belum-punya') {
                $kode = $this->M_generate_code->getNextUser();
            }
            if (strlen($kode) == 0) {
                $errCode++;
                $errMessage = "Kode wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Jenjang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

            $path['link'] = "upload/foto/" . $folder['code'] . "/";
            $upath = './' . $path['link'];
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }

            $config['upload_path'] = $upath;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '2048'; //maksimum besar file 2M
            $config['file_name'] = 'file_' . time() . '_' . $_FILES['photo']['name'];
            // $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('photo');
            $photo = $this->upload->data();
            $NoReg = $kode;

            try {
                $data = [
                    'id_status_kepegawaian' => $idStatusKepegawaian,
                    'id_kota' => $idKota,
                    'id_jenis_jenjang' => $idJenisJenjang,
                    'nama_lengkap' => $namaLengkap,
                    'jkel' => $jkel,
                    'tempat_lahir' => $tempatLahir,
                    'tgl_lahir' => date('Y-m-d', strtotime($tglLahir)),
                    'nomor_hp' => $noHp,
                    'asal_instansi' => $asalInstansi,
                    'email' => $email,
                    'nik' => $NIK,
                    'instansi' => $instansi,
                    'alamat_instansi' => $alamatInstansi,
                    'status_pegawai' => $statusPegawai,
                    'no_reg' => $NoReg,
                    'folder' => $folder['date'] . '/' . $folder['code'],
                    'password' => base64_encode($password),
                    'photo' => $path['link'] . '' . $photo['file_name'],
                    'status' => 'Non aktif',
                    'created_date' => $datetime,
                    'created_by' => 'Regist',
                    'updated_date' => $datetime,
                    'updated_by' => 'Regist',
                    'tanggal' => $date,
                ];
                $result = $this->db->insert('tbl_user', $data);

                $EmailFrom = base64_encode($email);
                $data['nama'] = $namaLengkap;
                $data['url'] = site_url() . 'Publik/Web/sukses/' . $EmailFrom;


                $config = array(
                  'useragent'   => 'Codeigniter',
                  'protocol'    => 'SMTP',
                  'smtp_host'   => 'mail.kompetensipkb.com',
                  'smtp_port'   =>  465,
                  'smtp_crypto' => 'ssl',
                  'smtp_user'   => 'mail-server@kompetensipkb.com',
                  'smtp_pass'   => 'kompetensi2021#',
                  'mailtype'    => 'html', 
                  'charset'     => 'utf-8',
                  'newline'     => "\r\n"           
              );

                $this->email->initialize($config);
                $this->email->from('no-reply-mail-server@kompetensipkb.com', 'Aplikasi E-Kompetensi');
                $this->email->to($email);
                $this->email->subject('Notifikasi Aktivasi Akun');
                $message = $this->load->view('email', $data, TRUE);
                $this->email->message($message);
                $result = $this->email->send();

                $data2 = [
                    'nama' => $namaLengkap,
                    'email' => $email,
                    'status' => 'Non Aktif',
                    'tipe' => 'user',
                    'password' => base64_encode($this->input->post("password")),
                ];
                $result = $this->db->insert('tbl_login', $data2);

                $this->doUploadOthersImages($folder);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }


    public function prosesDaftar2()
    {
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();

        $idProvinsi = $this->input->post('id_provinsi');
        $idKota = $this->input->post('id_kota');
        $kode = $this->input->post('kode_registrasi');
        $folder = $this->input->post('folder');

        $jkel = $this->input->post('jkel');
        $statusPegawai = $this->input->post('status_pegawai');
        $namaLengkap = trim($this->input->post('nama_lengkap'));
        $NIK = trim($this->input->post('nik'));
        $NIP = trim($this->input->post('nip'));
        $tempatLahir = $this->input->post('tempat_lahir');
        $tglLahir = $this->input->post('tgl_lahir');
        $noHp = $this->input->post('nomor_hp');
        $asalInstansi = $this->input->post('asal_instansi');
        $email = $this->input->post('emails');
        $instansi = $this->input->post('instansi');
        $alamatInstansi = $this->input->post('alamat_instansi');
        $password = $this->input->post('password');

        if ($errCode == 0) {
            if (strlen($namaLengkap) == 0) {
                $errCode++;
                $errMessage = "Nama Lengkap wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($NIK) == 0) {
                $errCode++;
                $errMessage = "NIK wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($jkel) == 0) {
                $errCode++;
                $errMessage = "Jenis Kelamin wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPegawai) == 0) {
                $errCode++;
                $errMessage = "Status Kepegawaian wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tempatLahir) == 0) {
                $errCode++;
                $errMessage = "Tempat Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglLahir) == 0) {
                $errCode++;
                $errMessage = "Tanggal Lahir wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Format Email salah.";
            }
        }
        if ($errCode == 0) {
            $checkuser = $this->M_publik->checkUsername(['email' => $email]);
            if ($checkuser) {
                $errCode++;
                $errMessage = "Akun email sudah terdaftar.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noHp) == 0) {
                $errCode++;
                $errMessage = "Nomor HP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($instansi) == 0) {
                $errCode++;
                $errMessage = "Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($asalInstansi) == 0) {
                $errCode++;
                $errMessage = "Alamat Instansi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idProvinsi) == 0) {
                $errCode++;
                $errMessage = "Provinsi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkProvinsi = $this->M_provinsi->selectById($idProvinsi);
            if ($checkProvinsi == null) {
                $errCode++;
                $errMessage = "Provinsi tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKota) == 0) {
                $errCode++;
                $errMessage = "Kota wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKota = $this->M_kota->selectById($idKota);
            if ($checkKota == null) {
                $errCode++;
                $errMessage = "Kota tidak valid.";
            }
        }
      
      
        if ($errCode == 0) {
            if ($type == 'belum-punya') {
                $kode = $this->M_generate_code->getNextUser();
            }
            if (strlen($kode) == 0) {
                $errCode++;
                $errMessage = "Kode wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Jenjang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $folder = ['date' => date('Ymd'), 'code' => md5($kode)];

            $path['link'] = "upload/foto/" . $folder['code'] . "/";
            $upath = './' . $path['link'];
            if (!file_exists($upath)) {
                mkdir($upath, 0777);
            }

            $config['upload_path'] = $upath;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '2048'; //maksimum besar file 2M
            $config['file_name'] = 'file_' . time() . '_' . $_FILES['photo']['name'];
            // $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('photo');
            $photo = $this->upload->data();

            $KodeKota = $checkKota->kode;
            $KodeJenisJenjang = $checkJenisJenjang->kode;
            $KodeStatusKepegawaian = $checkStatusKepegawaian->kode;
            $NoReg = $KodeKota . '.' . $KodeJenisJenjang . '.' . $KodeStatusKepegawaian . '.' . $kode;

            try {
                $data = [
                    'id_status_kepegawaian' => $idStatusKepegawaian,
                    'id_kota' => $idKota,
                    'id_jenis_jenjang' => $idJenisJenjang,
                    'nama_lengkap' => $namaLengkap,
                    'jkel' => $jkel,
                    'tempat_lahir' => $tempatLahir,
                    'tgl_lahir' => date('Y-m-d', strtotime($tglLahir)),
                    'nomor_hp' => $noHp,
                    'asal_instansi' => $asalInstansi,
                    'email' => $email,
                    'instansi' => $instansi,
                    'alamat_instansi' => $alamatInstansi,
                    'status_pegawai' => $statusPegawai,
                    'no_reg' => $NoReg,
                    'nik' => $NIK,
                    'nip' => $NIP,
                    'kode_kota' => $KodeKota,
                    'kode_jenjang' => $KodeJenisJenjang,
                    'kode_kepegawaian' => $KodeStatusKepegawaian,
                    'kode_otomatis' => $kode,
                    'folder' => $folder['date'] . '/' . $folder['code'],
                    'password' => base64_encode($password),
                    'photo' => $path['link'] . '' . $photo['file_name'],
                    'status' => 'Non aktif',
                    'created_date' => $datetime,
                    'created_by' => 'Regist',
                    'updated_date' => $datetime,
                    'updated_by' => 'Regist',
                    'tanggal' => $date,
                ];
                $result = $this->db->insert('tbl_user', $data);

                $EmailFrom = base64_encode($email);
                $data['nama'] = $namaLengkap;
                $data['url'] = site_url() . 'Publik/Web/sukses/' . $EmailFrom;


                $config = array(
                  'useragent'   => 'Codeigniter',
                  'protocol'    => 'SMTP',
                  'smtp_host'   => 'mail.kompetensipkb.com',
                  'smtp_port'   =>  465,
                  'smtp_crypto' => 'ssl',
                  'smtp_user'   => 'mail-server@kompetensipkb.com',
                  'smtp_pass'   => 'kompetensi2021#',
                  'mailtype'    => 'html', 
                  'charset'     => 'utf-8',
                  'newline'     => "\r\n"           
              );

                $this->email->initialize($config);
                $this->email->from('no-reply-mail-server@kompetensipkb.com', 'Aplikasi E-Kompetensi');
                $this->email->to($email);
                $this->email->subject('Notifikasi Aktivasi Akun');
                $message = $this->load->view('email', $data, TRUE);
                $this->email->message($message);
                $result = $this->email->send();

                $data2 = [
                    'nama' => $namaLengkap,
                    'email' => $email,
                    'status' => 'Non Aktif',
                    'tipe' => 'user',
                    'password' => base64_encode($this->input->post("password")),
                ];
                $result = $this->db->insert('tbl_login', $data2);

                $this->doUploadOthersImages($folder);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function kirim_email()
    {

        $this->db->trans_begin();
        $email = $this->input->post('email');
        $check = $this->M_publik->checkEmail(['email' => $email]);
        $data['brand'] = $this->M_publik->checkEmail2($email);

        if ($check == TRUE) {
            foreach ($check as $user) {
                if ($user->status == "Non aktif") {
                    $json = array('status' => false, 'pesan' => 'Maaf akun belum aktif !');
                } else if ($user->status_helper == "Blokir") {
                    $json = array('status' => false, 'pesan' => 'Maaf akun terblokir !');
                } else {

                    $config = array(
                      'useragent'   => 'Codeigniter',
                      'protocol'    => 'SMTP',
                      'smtp_host'   => 'mail.kompetensipkb.com',
                      'smtp_port'   =>  465,
                      'smtp_crypto' => 'ssl',
                      'smtp_user'   => 'mail-server@kompetensipkb.com',
                      'smtp_pass'   => 'kompetensi2021#',
                      'mailtype'    => 'html', 
                      'charset'     => 'utf-8',
                      'newline'     => "\r\n"           
                  );

                    $this->email->initialize($config);
                    $this->email->from('no-reply-mail-server@kompetensipkb.com', 'Aplikasi E-Kompetensi');
                    $this->email->to($email);
                    $this->email->subject('Informasi Akun');
                    $message = $this->load->view('info-akun', $data, TRUE);
                    $this->email->message($message);
                    $result = $this->email->send();

                    if ($result > 0)
                    {
                        $json = array('status' => true, 'pesan' => 'Silahkan cek email anda !');
                    } else {
                        $json = array('status' => false, 'pesan' => 'Gagal kirim email !');
                    }

                }
            }
        } else {
            $json = array('status' => false, 'pesan' => 'Email tidak terdaftar !');
        }
        echo json_encode($json);
    }

    public function sukses($EmailFrom)
    {
    	$datetime = date('Y-m-d H:i:s');
        $where = base64_decode($EmailFrom);
        $DataUser = $this->M_publik->checkEmail2($where);
        $IdUser = $DataUser->id_user;
        $NamaUser = $DataUser->nama_lengkap;
        $EmailUser = $DataUser->email;
        $StatusEmail = $DataUser->status_email;

        if ($StatusEmail=='') {
         $data2 = [
            'id_user' =>   $IdUser,
            'keterangan_status' => 'User <b>' . $NamaUser . '</b> telah melakukan aktivasi akun di email',
            'email' => $EmailUser,
            'nama' =>  $NamaUser,
            'created_date' => $datetime,
        ];

        $result = $this->db->insert('tbl_log_email', $data2);
    }


    $data = array(
     'status_email'     => 'Aktif',
 );
    $result = $this->db->update('tbl_user', $data, array('email' => $where));




    if($result){
        $this->load->view('email-sukses');
    } else {
        echo 'failed';
    }

}

public function suksesDaftar()
{

    $data['title'] = 'Sukses Daftar - E-Kopetensi';

    $this->load->view('front/header', $data);
    $this->load->view('sukses-daftar');
    $this->load->view('front/footer', $data);
}

public function suksesDaftarAndroid()
{

    $data['title'] = 'Sukses Daftar - E-Kopetensi';

    $this->load->view('front/headerAndroid', $data);
    $this->load->view('sukses-daftar');
    $this->load->view('front/footerAndroid', $data);
}

private function doUploadOthersImages($folder = [])
{
    $upath = './upload/berkas_registrasi/' . $folder['date'] . '/';
    if (!file_exists($upath)) {
        mkdir($upath, 0777);
    }
    $upath = './upload/berkas_registrasi/' . $folder['date'] . '/' . $folder['code'] . '/';
    if (!file_exists($upath)) {
        mkdir($upath, 0777);
    }

    $this->load->library('upload');

    $files = $_FILES;
    $cpt = count($_FILES['others']['name']);
    for ($i = 0; $i < $cpt; $i++) {
        unset($_FILES);
        $_FILES['others']['name'] = $files['others']['name'][$i];
        $_FILES['others']['type'] = $files['others']['type'][$i];
        $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
        $_FILES['others']['error'] = $files['others']['error'][$i];
        $_FILES['others']['size'] = $files['others']['size'][$i];

        $this->upload->initialize([
            'upload_path' => $upath,
            'allowed_types' => $this->allowed_img_types
        ]);
        $this->upload->do_upload('others');
    }
}


public function informasi()
{
    if ($this->lib->login() != "") {
        redirect('');
    } else {
        $data = [
            'title' => 'informasi',
            'posting'  => $this->M_publik->selek_informasi(),
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'page' => 'informasi',
            ];
            $this->load->view('front/header', $data);
            $this->load->view('informasi');
            $this->load->view('front/footer', $data);
        }
    }

    public function berita()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = [
                'title' => 'berita',
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'page' => 'berita',
            ];
            $data['onload'] = 'onload="load_data(1)"';
            $this->load->view('front/header', $data);
            $this->load->view('berita');
            $this->load->view('front/footer', $data);
        }
    }


    public function pageNews()
    {
        $config = array();
        $jumlah_data = $this->M_publik->jumlah_data();
        $config['base_url'] = base_url() . 'page-berita';
        $config['total_rows'] = $jumlah_data;
        $config["per_page"] = 3;
        $config['attributes'] = array('class' => 'page-link');
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li class="page-item">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li class="page-item">';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = 'Next';
        $config["next_tag_open"] = '<li class="page-item">';
        $config["next_tag_close"] = '</li>';
        $config['prev_link'] = 'Previous';
        $config["prev_tag_open"] = '<li>';
        $config["prev_tag_close"] = '</li>';
        $config["cur_tag_open"] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        //$config['display_pages'] = FALSE;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";

        $this->pagination->initialize($config);
        $from = $this->uri->segment(4);
        $start = ($from - 1) * $config["per_page"];

        $data = array();
        $ambil = $this->M_publik->pagenation('tbl_news', $config["per_page"], $start);

        if (!empty($ambil)) {
            foreach ($ambil as $row) {

                $batas = word_limiter($row->deskripsi, 20);
                $judul = word_limiter($row->judul, 8);
                $sub_array = array();
                $sub_array = '<div class="col-lg-4 col-md-4 col-sm-4 masonry-box">
                <div class="blog-post masonry">
                <div class="post">
                <div class="post-thumbnail">
                <img src="'. base_url() . $row->gambar .'" alt="Single Blog Post"/>
                </div>
                <div class="post-content">
                <div class="post-details">
                <h4 class="post-title">
                <span class="post-format">
                <span class="photo-icon"></span></span>
                <a href="' . base_url() . 'detail-berita/' . $row->permalink . '">' . $judul . '</a>
                </h4>
                </div>
                <p class="latest-from-blog_item_text">' . $batas . '</p>
                <a class="read-more big" href="' . base_url() . 'detail-berita/' . $row->permalink . '" title="Single Blog Post">Read more</a>
                </div>
                <div class="clear">
                </div></div>
                <div class="post-footer">
                <img src="'. base_url() . '/assets/publik/img/avatar.png" class="avatar"/>

                <span class="post-day">' . date_indo(date('m-d', strtotime($row->created_date))) .'</span>' . date_indo(date('Y', strtotime($row->created_date))) .'</span>
                <ul class="post-meta">
                <li>'.$row->created_by.'</li>
                </ul>
                </div>
                </div>
                </div>';
                $data[] = $sub_array;
            }
        } else {
            $sub_array = '
            <div class="container" style="margin-bottom:-100px; margin-top:-30px;"><div class="row justify-content-center">
            <div class="col-md-12">
            <center><img class="not-found" src="' . base_url() . 'assets/tambahan/gambar/not-found.png" width="350px;" /></center>
            </div>
            </div></div>';

            $data[] = $sub_array;
        }

        $output = array(
            'list_link' => $this->pagination->create_links(),
            'list_nama' => $data
        );

        echo json_encode($output);
    }

    public function kontak()
    {
        if ($this->lib->login() != "") {
            redirect('');
        } else {
            $data = [
                'title' => 'kontak',
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
                'page' => 'kontak',
            ];
            $this->load->view('front/header', $data);
            $this->load->view('kontak');
            $this->load->view('front/footer', $data);
        }
    }

    public function sendInbox()
    {

        $date = date('Y-m-d H:i:s');
        $this->db->trans_begin();

        $data = [
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'nomor_hp' => $this->input->post('nomor_hp'),
            'subject' => $this->input->post('subject'),
            'message' => $this->input->post('message'),
            'created_date' => $date,
        ];

        $result = $this->db->insert('tbl_inbox', $data);
        

        if ($this->db->trans_status() === FALSE) {
            $out = ['status' => false, 'pesan' => 'Maaf pesan gagal terkirim!'];
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Pesan berhasil terkirim'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => 'Maaf pesan gagal terkirim!'];
        }
        echo json_encode($out);
    }

    function getListKota()
    {
        $idProvinsi = $this->input->post('id_provinsi');
        $data = $this->M_kota->select(['id_provinsi' => $idProvinsi]);
        echo json_encode($data);
    }
}
