<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_publik extends CI_Model
{

    const __tableName = 'tbl_diklat';
    const __tableId = 'id_diklat';
    const __tableName2 = 'tbl_kenaikan_jenjang';
    const __tableId2 = 'id_jenjang';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_user";
        return $table;
    }

    public function selectMenu($idConfig = null)
    {
        $sql = "SELECT * FROM  tbl_config_menu WHERE deleted_date IS NULL";
        if (strlen($idConfig) > 0) {
            $sql .= " AND id_config = '{$idConfig}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    function getDataDiklat($isAjaxList = 0, $whereIn = [])
    {
        $tahun = date('Y');
        $tahun2 = date('Y');

        $sql = "SELECT " . self::__tableName . ".*
        , tbl_kategori_diklat.kategori_diklat as kategori_diklat
        , tbl_user.kode_otomatis as kode_otomatis_pemohon
        , tbl_user.email as email
        , tbl_user.nama_lengkap as nama_pemohon
        , tbl_user.no_reg as no_reg_pemohon
        , tbl_user.tempat_lahir as tempat_lahir_pemohon
        , tbl_user.tgl_lahir as tgl_lahir_pemohon
        , tbl_user.asal_instansi as asal_instansi_pemohon
        , tbl_user.instansi as instansi_pemohon
        , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
        , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
        , kota.kode as kode_kota_pemohon
        , kota.nama_kota as kota_pemohon
        FROM " . self::__tableName . "
        LEFT JOIN tbl_kategori_diklat ON tbl_kategori_diklat.id_kategori_diklat = " . self::__tableName . ".id_kategori_diklat
        LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName . ".id_user
        LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
        LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
        WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (!empty($whereIn)) {
            $sql .= " AND " . self::__tableName . ".status_pengajuan IN (";
            foreach ($whereIn as $key => $value) {
                if ($key > 0) {
                    $sql .= ",";
                }
                $sql .= "'{$value}'";
            }
            $sql .= ")";
        }
        if (strlen($tahun) > 0 && strlen($tahun2) > 0) {
            $tahun = $tahun;
            $tahun2 = $tahun2;
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$tahun}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".id_diklat DESC";
        }
        $data = $this->db->query($sql);
        return $data->result();
    }


    function getDataJenjang($isAjaxList = 0, $filter = [], $whereIn = [])
    {
     $tahun = date('Y');
     $tahun2 = date('Y');

     $sql = "SELECT " . self::__tableName2 . ".*
     , jenis_jenjang.jenjang as jenis_jenjang
     , tbl_kenaikan_jenjang.nilai as nilai
     , tbl_user.kode_otomatis as kode_otomatis_pemohon
     , tbl_user.email as email
     , tbl_user.nama_lengkap as nama_pemohon
     , tbl_user.no_reg as no_reg_pemohon
     , tbl_user.tempat_lahir as tempat_lahir_pemohon
     , tbl_user.tgl_lahir as tgl_lahir_pemohon
     , tbl_user.asal_instansi as asal_instansi_pemohon
     , tbl_user.instansi as instansi_pemohon
     , tbl_status_kepegawaian.kode as kode_status_kepegawaian_pemohon
     , tbl_status_kepegawaian.nama as status_kepegawaian_pemohon
     , kota.kode as kode_kota_pemohon
     , kota.nama_kota as kota_pemohon
     FROM " . self::__tableName2 . "
     LEFT JOIN jenis_jenjang ON jenis_jenjang.id = " . self::__tableName2 . ".id_jenis_jenjang
     LEFT JOIN tbl_user ON tbl_user.id_user = " . self::__tableName2 . ".id_user
     LEFT JOIN tbl_status_kepegawaian ON tbl_status_kepegawaian.id = tbl_user.id_status_kepegawaian
     LEFT JOIN kota ON kota.id_kota = tbl_user.id_kota
     WHERE " . self::__tableName2 . ".deleted_date IS NULL";
     if (!empty($whereIn)) {
        $sql .= " AND " . self::__tableName2 . ".status_pengajuan IN (";
        foreach ($whereIn as $key => $value) {
            if ($key > 0) {
                $sql .= ",";
            }
            $sql .= "'{$value}'";
        }
        $sql .= ")";
    }
    if (strlen($tahun) > 0 && strlen($tahun2) > 0) {
        $tahun = $tahun;
        $tahun2 = $tahun2;
        $sql .= " AND YEAR(" . self::__tableName2 . ".created_date) = '{$tahun}'";
    }
    if ($isAjaxList > 0) {
        $sql .= " ORDER BY " . self::__tableName2 . ".id_jenjang DESC";
    }
    $data = $this->db->query($sql);
    return $data->result();
}

function selek_slider()
{
    $sql = "SELECT * FROM tbl_slider WHERE deleted_date IS NULL";
    $data = $this->db->query($sql);
    return $data->result();
}

function selekNrp()
{
    $sql = "SELECT * FROM tbl_nrp order by id";
    $data = $this->db->query($sql);
    return $data->result();
}

function selek_informasi()
{
    $sql = "SELECT * FROM tbl_broadcast WHERE deleted_date IS NULL ORDER BY id_broadcast DESC limit 8";
    $data = $this->db->query($sql);
    return $data->result();
}

public function cekNoKey($nokey = "")
{
    $sql = "SELECT * FROM tbl_nrp WHERE nip='$nokey' OR kode_registrasi = '$nokey' limit 1";
    $data = $this->db->query($sql);
    return $data->result();
}

function selek_news()
{
    $sql = "SELECT * FROM tbl_news WHERE deleted_date IS NULL ORDER BY id_news DESC limit 8";
    $data = $this->db->query($sql);
    return $data->result();
}

public function selekBeritaRandom() {
    $sql = " select * from tbl_news where status='Aktif' order by rand() DESC LIMIT 5 ";
    $data = $this->db->query($sql);
    return $data->result();
}

public function getberita_id($permalink) {
    $sql = "select * from tbl_news where status='Aktif' and permalink LIKE '%$permalink%'";
    $data = $this->db->query($sql);
    return $data->row();
}

function jumlah_data(){
    return $this->db->get('tbl_news')->num_rows();
}

public function pagenation($table,$limit,$offset) 
{
    $this->db->select("*");
    $this->db->from($table);  
    $this->db->limit($limit,$offset); 
    $this->db->where('status','Aktif'); 
    $this->db->order_by('id_news', 'desc');
    $query = $this->db->get();
    return $query->result();
}


public function selek_kontak()
{
    $sql = " select * from tbl_kontak WHERE deleted_date IS NULL";
    $data = $this->db->query($sql);
    return $data->result();
}

function selek_history()
{
    $sql = " select * from tbl_broadcast WHERE deleted_date IS NULL";
    $data = $this->db->query($sql);
    return $data->result();
}

function selek_by_id($id)
{
    $sql = "SELECT * FROM tbl_broadcast WHERE deleted_date IS NULL AND id_broadcast ='{$id}'";
    $data = $this->db->query($sql);
    return $data->row();
}

function login($field1, $field2, $select = '*')
{
    $table = $this->get_table();
    $this->db->select($select);
    $this->db->from($table);
    $this->db->where($field1);
    $this->db->where($field2);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows === 0) {
        return FALSE;
    } else {
        return $query->result();
    }
}

function update_user($id, $data2)
{
    $this->db->where('email', $id);
    $this->db->update('tbl_login', $data2);

    return TRUE;
}

function update_user2($id, $data2)
{
    $this->db->where('email', $id);
    $this->db->update('tbl_user', $data2);
    return TRUE;
}

public function checkUsername($data)
{
    $this->db->select('email');
    $this->db->from('tbl_user');
    $this->db->where('email', $data['email']);
    $this->db->limit(1);

    $query = $this->db->get();
    if ($query->num_rows() == 1) {
        return $query->result();
    } else {
        return false;
    }
}

function checkEmail($field1, $select = '*')
{
    $table = $this->get_table();
    $this->db->select($select);
    $this->db->from($table);
    $this->db->where($field1);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows === 0) {
        return FALSE;
    } else {
        return $query->result();
    }

}

function checkEmail2($email)
{
    $sql = "SELECT * FROM tbl_user WHERE email ='{$email}'";
    $data = $this->db->query($sql);
    return $data->row();
}


public function selekNama($id){
    $this->db->order_by('id','ASC');
    $hasil= $this->db->get_where('tbl_nrp',array('id'=>$id));
    foreach ($hasil->result_array() as $data ){
        $res.= "$data[nama]";
    }
    return $res;
}

public function selekInstansi($id){
    $this->db->order_by('id','ASC');
    $hasil= $this->db->get_where('tbl_nrp',array('id'=>$id));
    foreach ($hasil->result_array() as $data ){
        $res.= "$data[instansi]";
    }
    return $res;
}

public function selekKdRegistrasi($id){
    $this->db->order_by('id','ASC');
    $hasil= $this->db->get_where('tbl_nrp',array('id'=>$id));
    foreach ($hasil->result_array() as $data ){
        $res.= "$data[kode_registrasi]";
    }
    return $res;
}

public function selekNip($id){
    $this->db->order_by('id','ASC');
    $hasil= $this->db->get_where('tbl_nrp',array('id'=>$id));
    foreach ($hasil->result_array() as $data ){
        $res.= "$data[nip]";
    }
    return $res;
}


}
