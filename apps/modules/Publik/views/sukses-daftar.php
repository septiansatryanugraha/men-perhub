<style type="text/css">
	.table-form-kontak,td,tr {
		padding: 5px;
		border:none;
	}

	.batas-atas {
		margin-top: 50px;
	}

</style>

<div class="container">
	<div class="row">
		<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
			<center><p style="font-family: sans-serif; font-size: 30px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Selamat Pendaftaran Anda Berhasil !!</b></p></center>

			 <center><img class="logo" src="<?= base_url() ?>/assets/tambahan/gambar/aktive_successful.gif"  width="220px;" style="margin-bottom: 25px;"></center>

			 <center><p style="font-family: sans-serif; font-size: 20px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>Silahkan Cek Notifikasi Email Anda lalu  login ke Aplikasi E-KOMPETENSI</b></p></center>
			<div class="clearfix clearfix-height40">
			</div>
		</section>
	</div>
</div>