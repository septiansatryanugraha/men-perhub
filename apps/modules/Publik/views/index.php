<div class="container">
	
	<div class="row">

		<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
			<h3 class="special-text wow animated fadeInDown marine-header5-special-h3" data-animation="fadeInDown">Visi & Misi Kami</h3>
			<h6 class="align-center marine-header4-h6" data-animation="">Memberikan Pelayanan Prima, Cepat dan mengutamakan Keselamatan Kendaraan</h6>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="<?= base_url() ?>assets/publik/img/service1.png" class="aligncenter dont_scale wow animated fadeInDown" data-animation="fadeInDown" alt="">
					<div class="special-text marine-header4-special1" data-animation="">
						<b>Meningkatkan Pelayanan Publik Yang Optimal.</b>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="<?= base_url() ?>assets/publik/img/service5new.png" class="aligncenter dont_scale wow animated fadeInDown" data-animation="fadeInDown" alt="">
					<div class="special-text marine-header4-special1" data-animation="">
						<b>Peningkatan Kompetensi Penguji yang Terampil beperilku Gesit, dan Profesionalisme</b>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="<?= base_url() ?>assets/publik/img/service3.png" class="aligncenter dont_scale wow animated fadeInDown" data-animation="fadeInDown" alt="">
					<div class="special-text marine-header4-special1" data-animation="">
						<b>Pelayanan yang ramah, sopan serta lugas.</b>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="<?= base_url() ?>assets/publik/img/service4.png" class="aligncenter dont_scale wow animated fadeInDown" data-animation="fadeInDown" alt="">
					<div class="special-text marine-header4-special1" data-animation="">
						<b>Menciptakan Lingkungan Kerja Lebih Kondusif dalam rangka meningkatkan Mutu Pelayanan.</b>
					</div>
				</div>
			</div>
		</section>

		<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
			<div class="clearfix clearfix-height40">
			</div>
			<div class="row">
				


				<div class="full_bg full-width-bg marine-header2-full_bg1" data-animation="">
					<div class="timeline-container-wrap">
						<div class="timeline-date-tooltip">
							<span>Berita Terbaru</span>
						</div>
						<div class="row timeline-row">
							<div class="masonry-container timeline-container">

								<?php 
								if (!empty($news)) {
									foreach ($news as $key) { 
										$judul = word_limiter($key->judul, 4);
										$desk = word_limiter($key->deskripsi, 30);
										?>

										<div class="col-lg-6 col-md-6 col-sm-6 masonry-box">
											<div class="blog-post masonry">
												<!-- POST FOOTER -->
												<div class="post-footer">
													<img alt='' src='<?= base_url() ?>/assets/publik/img/avatar.png' class='avatar'/>
													<span class="post-date">
														<span class="post-day"><?php echo date_indo(date('Y-m-d', strtotime($key->created_date))); ?></span>
													</span>
													<ul class="post-meta">
														<li><?= $key->created_by ?></li>
														
													</ul>
												</div>
												<!-- END POST FOOTER -->
												<div class="post">
													<div class="post-thumbnail">
														<img src="<?= base_url(); ?><?= $key->gambar ?>" width="800" height="320" alt="A Simple Text Post"/>
														<div class="post-hover">
															<a class="link-icon" href="#"></a><a class="search-icon prettyPhoto" href="<?= base_url(); ?><?= $key->gambar ?>"></a>
														</div>
													</div>
													<div class="post-content">
														<div class="post-details">
															<h4 class="post-title">
																<span class="post-format">
																	<span class="document-icon"></span></span>
																	<a href="<?php echo base_url() ?>detail-berita/<?php echo $key->permalink ?>"><?= $judul ?></a>
																</h4>
															</div>
															<p class="latest-from-blog_item_text">
																<?= $desk ?>
															</p>
															<a href="<?php echo base_url() ?>detail-berita/<?php echo $key->permalink ?>">Baca</a>
														</div>
														<div class="clear">
														</div>
													</div>
												</div>
											</div>
										<?php } } else { ?>

											<div class="container" style="margin-bottom:-100px; margin-top:-30px;"><div class="row justify-content-center">
												<div class="col-md-12">
													<center><img class="not-found" src="<?= base_url() ?>/assets/tambahan/gambar/not-found.png" width="300px;" /></center>
												</div>
											</div></div>

										<?php }  ?>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
					
					<div class="row">

						<div class="clearfix clearfix-height40">
						</div>
						<div class="full_bg full-width-bg" data-animation="">
							<div class="col-lg-6 col-md-6 col-sm-12">
								<div class="clearfix clearfix-height30">
								</div>
								<img src="<?= base_url() ?>/assets/publik/img/mockup.png" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 wow animated fadeInRight">

								<h3 class="big marine-3eader6-services-h3"><span class="marine-header2-span-color">Silahkan Download</span> di playstore untuk memudahkan dalam mendapatkan notifikasi secara realtime</h3>

								<a href=""><img src="<?= base_url() ?>/assets/publik/img/google-play.png" class="alignnone dont_scale" data-animation="fadeInLeft" alt="" width="150px;"></a>
								
								<div class="clearfix">
								</div>
							</div>
							<div class="clearfix">
							</div>
						</div>

						<div class="clearfix clearfix-height30">
						</div>
					</section>
					
					<!-- /Container -->
					