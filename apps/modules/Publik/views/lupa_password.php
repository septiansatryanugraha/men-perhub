<?php
$page = 'login';
$pageTitle = 'Login Pengajuan';
$deskripsi = 'Where can i get some ?';
include('header.php');

?>
<div class="container">
    <div class="row">
        <section class="col-lg-12 col-md-12 col-sm-12 small-padding">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 wow slideInLeft" >
                    <img class="logo" src="<?= base_url() ?>/assets/publik/img/flat-design-login.png"  width="530px;">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 wow fadeInRight">
                    <center><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="250px;"></center>
                    <b><center><p class="title-left" style="margin-top: 10px">Lupa password di Aplikasi E-KOMPETENSI</p></center></b>
                    <form class="get-in-touch contact-form light align-left" action="<?= base_url('send-email-forgot-password'); ?>" method="post" id="FormLogin2">
                        <div class="iconic-input">
                            <input type="text" name="email" id="email" placeholder="Email">
                            <i class="icons icon-mail"></i>
                        </div>
                        <br>
                        <div id='btn_loading2'></div>
                        <div id="hilang2">
                            <div class="form-actions">      
                                <button type="submit" id="btnSignUp" class="btn btn-primary pull-right">
                                    <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Kirim
                                </button>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- /Container -->
<?php include('footer.php'); ?>

<script type="text/javascript">
    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $(function () {
        $('#FormLogin2').submit(function (e) {
            var error = 0;
            var message = "";

            var email = $("#email").val();
            var email = email.trim();

            if (error == 0) {
                if (email.length == 0) {
                    error++;
                    message = " Email harus diisi !!";
                } else if (!cekemail(email)) {
                    error++;
                    message = "Format Email tidak sesuai (admin@gmail.com). ";
                }
            }
            if (error == 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#hilang2").hide();
                        $("#btn_loading2").html("<div class='form-actions'><button class='btn btn-primary pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
                        $("#btn_loading2").show();
                    },
                    url: $(this).attr('action'),
                    type: "POST",
                    cache: false,
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (json) {
                        if (json.status == true) {
                            document.getElementById("FormLogin2").reset();
                            $("#btn_loading2").hide();
                            $("#hilang2").show();
                            toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        } else {
                            document.getElementById("FormLogin2").reset();
                            $("#btn_loading2").hide();
                            $("#hilang2").show();
                            toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                "closeButton": true});
                        }
                    }
                });
                e.preventDefault();
            } else {
                toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                return false;
            }
        });
    });
</script>