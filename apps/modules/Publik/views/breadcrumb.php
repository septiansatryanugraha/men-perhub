  <?php if(isset($page)) { ?>
    <section class="full-width dark-blue-bg page-heading position-left-top size-cover">
        <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?php echo (isset($page)) ? $page : ''; ?></h1>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p class="breadcrumbs">
                    <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="index.php">Beranda</a></span><span class="delimiter">|</span><span class="current"><?php echo (isset($page)) ? $page : ''; ?></span>
                </p>
            </div>
        </div>
    </div>
</section>
<?php } ?>
