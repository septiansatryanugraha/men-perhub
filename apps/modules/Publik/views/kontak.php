<style type="text/css">
    .table-form-kontak,td,tr {
        padding: 5px;
        border:none;
    }

    #btn_loading2 {
        display: none;
    }

    .batas-atas {
        margin-top: 50px;
    }

</style>
<?php include('breadcrumb.php'); ?>
<div class="container">
    <div class="batas-atas"></div>
    <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9710.820383670456!2d106.82105068755222!3d-6.175596316115559!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d4f3aec069%3A0xbc7a9626bf97d3c!2sJl.%20Medan%20Merdeka%20Barat%20No.8%2C%20RT.2%2FRW.3%2C%20Gambir%2C%20Kecamatan%20Gambir%2C%20Kota%20Jakarta%20Pusat%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2010110!5e0!3m2!1sid!2sid!4v1640240570688!5m2!1sid!2sid" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe></p>
    <div class="row">
        <section class="col-lg-12 col-md-12 col-sm-12 small-padding">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="accordions">
                        <div class="accordion accordion-active">
                            <div class="accordion-header">
                                <div class="accordion-icon">
                                </div>
                                <h5><i class="icons icon-phone"></i> Hotline</h5>
                            </div>
                            <div class="accordion-content display-block">
                                <p>021-3811308, 3505006</p>
                            </div>
                        </div>
                        <div class="accordion ">
                            <div class="accordion-header">
                                <div class="accordion-icon">
                                </div>
                                <h5><i class="icons icon-mail"></i> Email</h5>
                            </div>
                            <div class="accordion-content">
                                INFO151@DEPHUB.GO.ID
                            </div>
                        </div>
                        <div class="accordion ">
                            <div class="accordion-header">
                                <div class="accordion-icon">
                                </div>
                                <h5><i class="icons icon-location"></i> Alamat</h5>
                            </div>
                            <div class="accordion-content">
                                JL. MEDAN MERDEKA BARAT NO. 8 JAKARTA PUSAT DKI JAKARTA 10110
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-6">
                    <form class="get-in-touch contact-form light align-left" id="form-simpan" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="contact-form-value" value="1"/>
                        <div class="iconic-input">
                            <input type="text" name="name" id="name" placeholder="Nama Lengkap*">
                            <i class="icons icon-user-1"></i>
                        </div>
                        <div class="iconic-input">
                            <input type="text" name="no_hp" id="no_hp" placeholder="No Handphone*">
                            <i class="icons icon-mobile-6"></i>
                        </div>
                        <div class="iconic-input">
                            <input type="text" name="email" id="email" placeholder="Email*">
                            <i class="icons icon-mail"></i>
                        </div>
                        <div class="iconic-input">
                            <input type="text" name="subject" id="subject" placeholder="Subjek*">
                        </div>
                        <textarea name="message" id="message" placeholder="Pesan*"></textarea>
                        <button id="hilang2" type="submit" style="margin-top: 20px;" class='action-button btn-fill btn-info btn-wd btn-md'><i class="fa fa-paper-plane" aria-hidden="true"></i></i>&nbsp; Kirim</button>
                        <button id="btn_loading2" type='submit' style='margin-top: 20px;' class='action-button btn-fill btn-info btn-wd btn-md' disabled><i class='fa fa-refresh fa-spin'></i>&nbsp;Wait...</button>
                        <button type='reset' style='margin-top: 20px;' class='action-button btn-fill btn-danger btn-wd btn-md'>cancel</button>
                    </form>
                    <div class="msg">
                    </div>
                </div>
            </div>
            <div class="clearfix clearfix-height40"></div>
        </section>
    </div>
</div>
<script type="text/javascript">
    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    $('#form-simpan').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var nama_lengkap = $("#name").val();
            var nama_lengkap = nama_lengkap.trim();
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama wajib di isi.";
            }
        }
        if (error == 0) {
            var no_hp = $("#no_hp").val();
            var no_hp = no_hp.trim();
            if (no_hp.length == 0) {
                error++;
                message = "No HP wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = " Email harus diisi !!";
            } else if (!cekemail(email)) {
                error++;
                message = "Format Email tidak sesuai (admin@gmail.com). ";
            }
        }
        if (error == 0) {
            var subjek = $("#subject").val();
            var subjek = subjek.trim();
            if (subjek.length == 0) {
                error++;
                message = "Subjek wajib di isi.";
            }
        }
        if (error == 0) {
            var message = $("#message").val();
            var message = message.trim();
            if (subject.length == 0) {
                error++;
                message = "Pesan wajib di isi.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#hilang2").hide();
                    $("#btn_loading2").show();
                },
                url: '<?= base_url('send-inbox'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-simpan").reset();
                    $("#btn_loading2").hide();
                    $("#hilang2").show();
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});

                } else {
                    $("#btn_loading2").hide();
                    $("#hilang2").show();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
        }
    });
</script>