<?php include('breadcrumb.php'); ?>
<!-- Container -->
<div class="container">
	<section class="row">
		<div id="post-items">
			<div id="content"></div>
		</div>
		<div id="loader"></div>
		<div class="clearfix clearfix-height40"></div>
	</section>
	<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
		<center><div id="pagination_link"></div></center>
	</section>
</div>
<div class="clearfix clearfix-height30">
</div>
	<!-- /Container -->