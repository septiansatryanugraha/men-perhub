<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Marine Main 1">
	<meta name="keywords" content="Homepage, Marine">
	<title><?= $title . " - E-Kompetensi - Kementerian Perhubungan"; ?></title>

	<link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" id='twitter-bootstrap-css' href="<?= base_url(); ?>assets/publik/css/bootstrap.min.css" media='all'/>

	<link rel='stylesheet' id='fontello-css' href='<?= base_url(); ?>assets/publik/css/fontello.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/font-awesome.min.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/popout.css' type='text/css' media='all'/>

	<link rel='stylesheet' id='prettyphoto-css-css' href='<?= base_url(); ?>assets/publik/js/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/animate.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='flexSlider-css' href='<?= base_url(); ?>assets/publik/css/flexslider.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='perfectscrollbar-css' href='<?= base_url(); ?>assets/publik/css/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='jquery-validity-css' href='<?= base_url(); ?>assets/publik/css/jquery.validity.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='jquery-ui-css' href='<?= base_url(); ?>assets/publik/css/jquery-ui.min.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='style-css' href='<?= base_url(); ?>assets/publik/css/style.css' type='text/css' media='all'/>
	<link rel='stylesheet' id='mobilenav-css' href='<?= base_url(); ?>assets/publik/css/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/publik/css/style.revslider.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/publik/js/rs-plugin/css/settings.css" media="screen"/>
	<!-- jQuery -->
	<script src="<?= base_url(); ?>assets/publik/js/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
	<script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>

	<?= $script_captcha; // javascript recaptcha ?>
</head>
<body class="w1170 headerstyle1">
	<!-- Marine Content Wrapper -->
	<div id="marine-content-wrapper">
		<!-- /Header -->

		<script>
			$(document).ready(function($){
				$("#show2").click(function(){
					$("#jwpopupBox3").fadeIn('slow');
				});

				$("#hide2").click(function(){
					$("#jwpopupBox3").fadeOut('slow');
				});

			});
		</script>

		<!-- Content Inner -->
		<div id="marine-content-inner">
			<!-- Main Content -->
			<section id="main-content">
