<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Marine Main 1">
<meta name="keywords" content="Homepage, Marine">
<title><?= $title . " - E-Kompetensi - Kementerian Perhubungan"; ?></title>

<link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800,900' rel='stylesheet' type='text/css'>

<link rel="stylesheet" id='twitter-bootstrap-css' href="<?= base_url(); ?>assets/publik/css/bootstrap.min.css" media='all'/>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/super-treadmill/super-treadmill.css" type="text/css" /> 

<link rel='stylesheet' id='fontello-css' href='<?= base_url(); ?>assets/publik/css/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/font-awesome.min.css' type='text/css' media='all'/>
<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/popout.css' type='text/css' media='all'/>

<link rel='stylesheet' id='prettyphoto-css-css' href='<?= base_url(); ?>assets/publik/js/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' href='<?= base_url(); ?>assets/publik/css/animate.css' type='text/css' media='all'/>
<link rel='stylesheet' id='flexSlider-css' href='<?= base_url(); ?>assets/publik/css/flexslider.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='<?= base_url(); ?>assets/publik/css/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='<?= base_url(); ?>assets/publik/css/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='<?= base_url(); ?>assets/publik/css/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='<?= base_url(); ?>assets/publik/css/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='<?= base_url(); ?>assets/publik/css/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/publik/css/style.revslider.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/publik/js/rs-plugin/css/settings.css" media="screen"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/DataTables/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/popup/jquery.fancybox.css">
<!-- jQuery -->
<script src="<?= base_url(); ?>assets/publik/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
<script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>

<?= $script_captcha; // javascript recaptcha ?>
</head>
<body <?php
if (isset($onload)) { echo $onload;} ?> class="w1170 headerstyle1">
<!-- Marine Content Wrapper -->
<div id="marine-content-wrapper">
    <!-- Header -->
<header id="header" class="style1">
<!-- Main Header -->
<div id="main-header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="col-lg-4 col-md-4 col-sm-4 logo">
				<a href="index.php" rel="home"><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="230px;"></a>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col-sm-8 align-right">
			<!-- <ul class="text-list">
			<i class="fa fa-lock"></i><a href="#" id="show"><li>&nbsp; Login</li></a></ul>
			</ul> -->

			<ul class="text-list">
			<i class="fa fa-paper-plane"></i><a href="<?php echo site_url(); ?>daftar-akun"><li>&nbsp Daftar</li></a>
			</ul>
			</div>
		</div>
	</div>
</div>

<!-- /Main Header -->
<!-- Lower Header -->
<div id="lower-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="lower-logo">
					<a href="index.html" title="Marine" rel="home"><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="125px;"></a>
				</div>
				<!-- Main Navigation -->
				<ul id="main-nav" class="menu">
				<li class="<?php echo (isset($page) AND $page == 'home') ? 'menu-item active' : ''; ?>" ><a href="<?php echo site_url(); ?>page-home.html" ><i class="fa fa-home"></i>&nbsp; Beranda</a></li>
				<li class="<?php echo (isset($page) AND $page == 'informasi') ? 'menu-item active' : ''; ?>" ><a href="<?php echo site_url(); ?>page-informasi.html" ><i class="fa fa-info-circle"></i>&nbsp; Informasi</a></li>
				<li class="<?php echo (isset($page) AND $page == 'berita') ? 'menu-item active' : ''; ?>" ><a href="<?php echo site_url(); ?>page-berita.html" ><i class="fa fa-newspaper-o"></i>&nbsp; Berita Terbaru</a></li>
				<li class="<?php echo (isset($page) AND $page == 'kontak') ? 'menu-item active' : ''; ?>" ><a href="<?php echo site_url(); ?>page-kontak.html" ><i class="fa fa-user"></i>&nbsp; Kontak Kami
				</a></li>
				<li class="<?php echo (isset($page) AND $page == 'login') ? 'menu-item active' : ''; ?>" ><a href="<?php echo site_url(); ?>login-user.html" ><i class="fa fa-lock"></i>&nbsp; Login
				</a></li>

				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /Lower Header -->
</header>
    <!-- /Header -->


<script>
$(document).ready(function($){
 $("#show").click(function(){
    $("#jwpopupBox3").fadeIn('slow');
  });

  $("#hide").click(function(){
    $("#jwpopupBox3").fadeOut('slow');
  });
 
});
</script>


    <!-- Content Inner -->
    <div id="marine-content-inner">
	<!-- Main Content -->
	<section id="main-content">
		<?php if(isset($page) AND $page == 'home') { ?>
		<section id="slider">
		<div class="container">
			<!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->
			<div id="rev_slider_22_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#eee;padding:0px;margin-top:0px;margin-bottom:0px;max-height:550px;">
				<div id="rev_slider_22_1" class="rev_slider fullwidthabanner" style="display:none;max-height:550px;">
					<ul>	<!-- SLIDE  -->
					<?php foreach ($slider as $key) { ?>
						<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-thumb="<?= base_url(); ?><?= $key->gambar ?>"  data-saveperformance="off"  data-title="Slide">
							<!-- MAIN IMAGE -->
							<img src="<?= base_url(); ?><?= $key->gambar ?>"  alt="slide1-2" data-lazyload="<?= base_url(); ?><?= $key->gambar ?>" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
						</li>
						<?php } ?>
					</ul>
					<div class="tp-bannertimer"></div>	</div>

					<script type="text/javascript">
						/******************************************
							-	PREPARE PLACEHOLDER FOR SLIDER	-
						******************************************/
						var setREVStartSize = function() {
							var	tpopt = new Object();
								tpopt.startwidth = 1000;
								tpopt.startheight = 550;
								tpopt.container = jQuery('#rev_slider_22_1');
								tpopt.fullScreen = "off";
								tpopt.forceFullWidth="off";
								tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
						};
						/* CALL PLACEHOLDER */
						setREVStartSize();
						var tpj=jQuery;
						// tpj.noConflict();
						var revapi22;
						tpj(document).ready(function() {
						if(tpj('#rev_slider_22_1').revolution == undefined)
							revslider_showDoubleJqueryError('#rev_slider_22_1');
						else
						   revapi22 = tpj('#rev_slider_22_1').show().revolution(
							{
								dottedOverlay:"none",
								delay:5000,
								startwidth:1000,
								startheight:550,
								hideThumbs:200,
								thumbWidth:100,
								thumbHeight:50,
								thumbAmount:2,	
								simplifyAll:"off",
								navigationType:"none",
								navigationArrows:"solo",
								navigationStyle:"preview4",
								touchenabled:"on",
								onHoverStop:"on",
								nextSlideOnWindowFocus:"off",
								swipe_threshold: 0.7,
								swipe_min_touches: 1,
								drag_block_vertical: false,
								parallax:"mouse",
								parallaxBgFreeze:"on",
								parallaxLevels:[4,5,3,2,5,4,3,5,5,0],
								parallaxDisableOnMobile:"on",
								keyboardNavigation:"off",
								navigationHAlign:"center",
								navigationVAlign:"bottom",
								navigationHOffset:0,
								navigationVOffset:20,
								soloArrowLeftHalign:"left",
								soloArrowLeftValign:"center",
								soloArrowLeftHOffset:20,
								soloArrowLeftVOffset:0,
								soloArrowRightHalign:"right",
								soloArrowRightValign:"center",
								soloArrowRightHOffset:20,
								soloArrowRightVOffset:0,
								shadow:0,
								fullWidth:"on",
								fullScreen:"off",
								spinner:"spinner4",
								stopLoop:"off",
								stopAfterLoops:-1,
								stopAtSlide:-1,
								shuffle:"off",
								autoHeight:"off",
								forceFullWidth:"off",						
								hideThumbsOnMobile:"off",
								hideNavDelayOnMobile:1500,
								hideBulletsOnMobile:"off",
								hideArrowsOnMobile:"off",
								hideThumbsUnderResolution:0,
								hideSliderAtLimit:0,
								hideCaptionAtLimit:0,
								hideAllCaptionAtLilmit:0,
								startWithSlide:0
							});
						});	/*ready*/
					</script>
			</div><!-- END REVOLUTION SLIDER -->
			<script>
				/* Fix The Revolution Slider Loading Height issue */
				$(document).ready(function(){
					$('.rev_slider_wrapper').each(function(){
						$(this).css('height','');
						var revStartHeight = parseInt($('>.rev_slider', this).css('height'));
						$(this).height(revStartHeight);
						$(this).parents('#slider').height(revStartHeight);
						
						$(window).load(function(){
							$('#slider').css('height','');
						});
					});
				});
			</script>
		</div>
    </section>
	<?php } ?>
		