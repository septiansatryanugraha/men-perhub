</section>
<!-- /Main Content -->
</div>

<!-- /Footer -->
</div>
<!-- /Marine Content Wrapper -->
<!-- JavaScript -->
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/bootstrap.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/animate.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery-ui.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/SmoothScroll.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/modernizr.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/wow.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.sharre.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.knob.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.fitvids.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/jquery.validity.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/tweetie.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/script.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/rs-plugin/js/jquery.themepunch.enablelog.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/rs-plugin/js/jquery.themepunch.revolution.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='<?= base_url(); ?>assets/publik/js/rs-plugin/js/jquery.themepunch.tools.min.js'></script>

<style type="text/css">  
.gap-pop-up {
  margin-bottom: 20px;
}
</style>

<div id="jwpopupBox3" class="jwpopup3">
  <!-- jwpopup content -->
  <div class="jwpopup-content3 ">
    <div class="jwpopup-head">

      <span class="exit3" id="hide2">×</span>

      <div class="jwpopup-main">

        <div class="omb_login"> 
          <div class="title-javascript ">
            <br>
            <center><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="200px;"></center>
            <b><center><p class="title-left" style="margin-top: 10px">Sebelum melakukan pendaftaran silahkan cari NIP / Kode Registrasi Anda</p></center></b>
          </div>

          <div class="row omb_row-sm-offset-3">
            <div class="col-lg-8 col-md-8 col-sm-6">
            <form class="align-left" action="<?= base_url('Publik/Web/cekNoKeyAndroid'); ?>" method="POST">
                <input type="text" name="nokey" class="form-control" placeholder="NIP / Kode Registrasi">
                <br>

                <div class="form-actions">      
                  <button type="submit" class="btn btn-primary pull-right gap-pop-up">
                    <i class="fa fa-search" aria-hidden="true"></i> &nbsp;Cari
                  </button>
                <div class="iconic-button"></div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

    <a id="button-top"></a>

    <script type="text/javascript">

      var btn_top = $('#button-top');
      $(window).scroll(function(){ 
        if ($(this).scrollTop() > 300) { 
          btn_top.addClass('show');
        } else { 
          btn_top.removeClass('show'); 
        } 
      }); 
      $('#button-top').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
      }); 

    </script>

    <script type="text/javascript">
      $(function () {
        $("#buka").hide();
        $('#FormLogin').submit(function (e) {
          e.preventDefault();
          $.ajax({
            beforeSend: function () {
              $("#buka").hide();
              $("#hilang").hide();
              $("#btn_loading").html("<div class='form-actions'><button class='btn btn-primary pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
              $("#btn_loading").show();
            },
            url: $(this).attr('action'),
            type: "POST",
            cache: false,
            data: $(this).serialize(),
            dataType: 'json',
            success: function (json) {
              if (json.status == true) {
                $("#btn_loading").hide();
                $("#buka").show();
                toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                  "closeButton": true});
                window.location = json.url_home;
              } else {
                $("#btn_loading").hide();
                $("#hilang").show();
                toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                  "closeButton": true});
              }
            }
          });
        });
      });


      function ShowPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }

      function ShowPassword2() {
        var x = document.getElementById("password2");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }

      function ShowPassword3() {
        var x = document.getElementById("password3");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }
    </script>



  </body>
  </html>