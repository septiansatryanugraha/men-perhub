<section class="full-width dark-blue-bg page-heading position-left-top size-cover">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1>Single Blog Post</h1>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p class="breadcrumbs" xmlns:v="#"><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php echo base_url(); ?>page-home.html">Beranda</a></span><span class="delimiter">|</span> <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php echo base_url(); ?>page-berita.html">Berita</a></span><span class="delimiter">|</span> <span class="current">here</span></p>
            </div>
        </div>
    </div>
</section>
<!-- Container -->
<div class="container">
    <!-- Container -->
    <div class="row">
        <section class="main-content col-lg-9 col-md-8 col-sm-8 small-padding">
            <div class="row">
                <div id="post-items">
                    <!-- Post Item -->
                    <div class="post blog-post blog-post-classic col-lg-12 col-md-12 col-sm-12 margin-top20">
                        <div class="blog-post-list">
                            <div class="blog-post-meta">
                                <span class="post-date">
                                    <span class="post-day"><?php echo date_indo(date('d', strtotime($detail_berita->created_date))); ?></span><br/>
                                    <?php echo date_indo(date('Y-m', strtotime($detail_berita->created_date))); ?></span>
                                <span class="post-format">
                                    <span class="photo-icon"></span>
                                </span>
                                <img alt='' src='img/avatar.png' class='avatar'/>
                                <span class="author"><?= $detail_berita->created_by ?></span>
                            </div>
                            <div class="blog-post-content">
                                <!-- Post Image -->
                                <div class="post-thumbnail">
                                    <img src="<?= base_url(); ?><?= $detail_berita->gambar ?>" alt="Single Blog Post"/>
                                    <div class="post-hover">
                                        <a class="link-icon" href="#"></a>
                                        <a class="search-icon prettyPhoto" href="<?= base_url(); ?><?= $detail_berita->gambar ?>"></a>
                                    </div>
                                </div>
                                <!-- /Post Image -->
                                <!-- Post Content -->
                                <div class="post-content">
                                    <br>
                                    <p><?= $detail_berita->deskripsi ?></p>

                                </div>
                                <!-- /Post Item -->
                            </div>
                        </div>

                        </section>
                        <aside class="sidebar col-lg-3 col-md-4 col-sm-4">

                            <div id="multi-posts-2" class="widget widget_multi_posts_entries">
                                <div class="tabs">
                                    <div class="tab-header">
                                        <ul>
                                            <li><a href="#tab1">Berita Lainya</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div id="tab1" class="tab">
                                            <ul class="posts-list">
                                                <?php
                                                foreach ($berita_random as $key) {

                                                    $judul = word_limiter($key->judul, 4);
                                                    $desk = word_limiter($key->deskripsi, 15);

                                                    ?>
                                                    <li class="post-item">
                                                        <div class="featured-image">
                                                            <img src="<?= base_url(); ?><?= $key->gambar ?>" alt="Hello world!"/>
                                                        </div>
                                                        <div class="post-content">
                                                            <ul class="post-meta">
                                                                <li><?php echo date_indo(date('Y-m-d', strtotime($key->created_date))); ?></li>
                                                                <li><?= $key->created_by ?></li>
                                                            </ul>
                                                            <a href="<?php echo base_url() ?>detail-berita/<?php echo $key->permalink ?>"><?= $judul ?></a>
                                                            <p>
                                                                <?= $desk ?>
                                                            </p>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>


                                    </div>
                                </div>
                                <!-- /Tabs -->
                            </div>
                        </aside>
                    </div>
                </div>