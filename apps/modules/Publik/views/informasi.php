<?php
include('breadcrumb.php');
$this->load->model('M_publik');
$selectMenu1 = $this->M_publik->selectMenu(1);
$selectMenu2 = $this->M_publik->selectMenu(2);

?>
<div class="container">
    <div class="row">
        <section class="col-lg-12 col-md-12 col-sm-12 small-padding marine-contents-container">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel-heading panel-heading-nav">
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active">
                            <a href="#one" aria-controls="one" role="tab" data-toggle="tab">Pengumuman</a>
                        </li>
                        <?php if ($selectMenu1->status == 'Open') { ?>
                            <li role="presentation">
                                <a href="#two" aria-controls="two" role="tab" data-toggle="tab">Diklat PKB</a>
                            </li>
                        <?php } ?>
                        <?php if ($selectMenu2->status == 'Open') { ?>
                            <li role="presentation">
                                <a href="#three" aria-controls="three" role="tab" data-toggle="tab">Kenaikan Jenjang</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="one">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><center>INFORMASI PENGUMUMAN</center></h3>
                                        </div>
                                        <div class="panel-body">
                                            <div id="mytreadmill" class="treadmill list"> 
                                                <?php
                                                foreach ($posting as $key) {
                                                    $desk = word_limiter($key->konten, 60);
                                                    echo "<div class='treadmill-unit'>";
                                                    echo anchor('Publik/Web/info/' . $key->id_broadcast, "
                                <small class='pull-right time'><i class='fa fa-clock-o'> &nbsp;" . date('d-m-Y', strtotime($key->created_date)) . "</i>&nbsp; </small>
                                <h4><i class='fa fa-info-circle'></i>&nbsp; " . $key->judul . "</h4> 
                                <p>" . $desk . "</p>", "class='clear fancybox fancybox.iframe'");
                                                    echo "</div>";
                                                }

                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="two">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="gap"></div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><center>DATA PESERTA LOLOS SELEKSI BERKAS DIKLAT PKB</center></h3>
                                        </div>
                                        <div class="panel-body">
                                            <table id="tableku" class="table table-striped table-bordered " style="width:100%">
                                                <thead>
                                                    <tr style="text-align: center;">
                                                        <td><b>#</b></td>
                                                        <td><b>Kode Pengajuan</b></td>
                                                        <td><b>Nama</b></td>
                                                        <td><b>Email</b></td>
                                                        <td><b>Instansi</b></td>
                                                        <td><b>Asal Instansi</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="gap"></div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><center>DATA PESERTA LULUS UJIAN DIKLAT PKB</center></h3>
                                        </div>
                                        <div class="panel-body">
                                            <table id="tableku2" class="table table-striped table-bordered " style="width:100%">
                                                <thead>
                                                    <tr style="text-align: center;">
                                                        <td><b>#</b></td>
                                                        <td><b>Kode Pengajuan</b></td>
                                                        <td><b>Nama</b></td>
                                                        <td><b>Nilai</b></td>
                                                        <td><b>Instansi</b></td>
                                                        <td><b>Asal Instansi</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="three">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="gap"></div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><center>DATA PESERTA LOLOS SELEKSI BERKAS KENAIKAN JENJANG TINGKAT</center></h3>
                                        </div>

                                        <div class="panel-body">
                                            <table id="tableku3" class="table table-striped table-bordered " style="width:100%">
                                                <thead>
                                                    <tr style="text-align: center;">
                                                        <td><b>#</b></td>
                                                        <td><b>Kode Pengajuan</b></td>
                                                        <td><b>Nama</b></td>
                                                        <td><b>Email</b></td>
                                                        <td><b>Instansi</b></td>
                                                        <td><b>Asal Instansi</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="gap"></div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><center>DATA PESERTA LULUS UJIAN KENAIKAN JENJANG TINGKAT</center></h3>
                                        </div>

                                        <div class="panel-body">
                                            <table id="tableku4" class="table table-striped table-bordered " style="width:100%">
                                                <thead>
                                                    <tr style="text-align: center;">
                                                        <td><b>#</b></td>
                                                        <td><b>Kode Pengajuan</b></td>
                                                        <td><b>Nama</b></td>
                                                        <td><b>Nilai</b></td>
                                                        <td><b>Instansi</b></td>
                                                        <td><b>Asal Instansi</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix clearfix-height30"></div>
        </section>
    </div>
</div>
<!-- /Container -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#mytreadmill').startTreadmill({
            runAfterPageLoad: true,
            direction: "down",
            speed: "slow",
            viewable: 5,
            pause: false
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.fancybox').fancybox({
            'width': '80%',
            'height': '500',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

    });
</script>
<script type="text/javascript">
    //untuk load data table ajax  
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table = $('#tableku').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Belum Ada Data Di Server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('diklat-pkb-lolos') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

</script>
<script type="text/javascript">
    //untuk load data table ajax  
    var table2;

    $(document).ready(function () {
        reloadTable2();
    });

    function reloadTable2() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table2 = $('#tableku2').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Belum Ada Data Pengajuan Di Server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('diklat-pkb-lulus') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

</script>
<script type="text/javascript">
    //untuk load data table ajax  
    var table3;

    $(document).ready(function () {
        reloadTable3();
    });

    function reloadTable3() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table3 = $('#tableku3').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Belum Ada Data Pengajuan Di Server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('jenjang-lolos') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }
</script>
<script type="text/javascript">
    //untuk load data table ajax  
    var table4;

    $(document).ready(function () {
        reloadTable4();
    });

    function reloadTable4() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();

        table4 = $('#tableku4').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Belum Ada Data Pengajuan Di Server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('jenjang-lulus') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }
</script>