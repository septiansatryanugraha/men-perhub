<?php
$page = 'login';
$pageTitle = 'Login Pengajuan';
$deskripsi = 'Where can i get some ?';
include('header.php');

?>
<div class="container">
    <div class="row">
        <section class="col-lg-12 col-md-12 col-sm-12 small-padding">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 wow slideInLeft" >
                    <img class="logo" src="<?= base_url() ?>/assets/publik/img/flat-design-login.png"  width="530px;">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 wow fadeInRight">
                    <center><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="250px;"></center>
                    <b><center><p class="title-left" style="margin-top: 10px">Silahkan login di Aplikasi E-KOMPETENSI</p></center></b>
                    <form class="get-in-touch contact-form light align-left" action="<?= base_url('login-web-user'); ?>" method="post" id="FormLogin2">
                        <div class="iconic-input">
                            <input type="text" name="email" placeholder="Email">
                            <i class="icons icon-mail"></i>
                        </div>
                        <div class="iconic-input">
                            <input  type="password"  id="password2" name="password" placeholder="Password">
                            <i class="icons fa fa-fw fa-lock field-icon toggle-password"></i>
                        </div>
                        <?= $captcha ?><br>
                        <div class="row">
                            <div class="iconic-input col-lg-5" style="margin-left: 15px; margin-right: 30px;">
                                <label class="checker">
                                    <input type="checkbox" onclick="ShowPassword2()">
                                    <span class="checkmark"></span>
                                </label><span class="tulisan-show" style="margin-left: 10px;">Show Password</span>
                            </div>
                            <div class="iconic-input col-lg-6" style="margin-left: 0px;" >
                                Belum punya akun? Silahkan daftar <a href="register-user"><b>disini</b></a>
                            </div>
                            <div class="iconic-input col-lg-5" style="margin-left: 15px; margin-right: 30px;">
                            </div>
                            <div class="iconic-input col-lg-6" style="margin-left: 0px;" >
                                Lupa Password ?  <a href="lupa-password.html"><b>disini</b></a>
                            </div>
                        </div>
                        <br>
                        <div id='btn_loading2'></div>
                        <div id="hilang2">
                            <div class="form-actions">      
                                <button type="submit" id="btnSignUp" class="btn btn-primary pull-right">
                                    <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login
                                </button>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- /Container -->
<?php include('footer.php'); ?>
<script type="text/javascript">
    $(function () {
        $('#FormLogin2').submit(function (e) {
            e.preventDefault();
            $.ajax({
                beforeSend: function () {
                    $("#hilang2").hide();
                    $("#btn_loading2").html("<div class='form-actions'><button class='btn btn-primary pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
                    $("#btn_loading2").show();
                },
                url: $(this).attr('action'),
                type: "POST",
                cache: false,
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {
                    if (json.status == true) {
                        $("#btn_loading2").hide();
                        toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                        window.location = json.url_home;
                    } else {
                        $("#btn_loading2").hide();
                        $("#hilang2").show();
                        toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                }
            });
        });
    });
</script>