<style type="text/css">
	.jarak-label {
		margin-top: 20px;
	}
	#slider {
		margin-left: -65%;
	}
	#btn_loading {
		display: none;
	}
	.field-icon {
		float: left;
		margin-left: 90%;
		margin-top: -25px;
		font-size: 17px;
		position: relative;
		z-index: 2;
		color: #ccc;
	}
	.pilih-reg{
		width: 150px;
	}
	.select-jenis-jenjang{
		width: 290px;
	}
	.select-status-kepegawaian{
		width: 290px;
	}
	.select-jkel{
		width: 290px;
	}
	.select-pegawai{
		width: 290px;
	}
	.select-instansi{
		width: 285px;
	}
	.select-provinsi{
		width: 300px;
	}
	.select-kota{
		width: 290px;
	}
	.jarak-label {
		margin-top: 20px;
	}
	.footer2{
		margin-top: 30px
	}
</style>
<link rel="stylesheet" href="<?= base_url(); ?>assets/publik/style-register.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/publik/css/ionicons.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/eksternal/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/publik/registrasi/css/gsdk-bootstrap-wizard.css" rel="stylesheet" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/publik/registrasi/css/demo.css" rel="stylesheet" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/select2.min.css">
<script src="<?= base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<div class="container">
	<section class="col-lg-12 col-md-12 col-sm-12 small-padding">

		<?php foreach ($HasilKey as $res) {
			$KodeRegistrasi =  $res->kode_registrasi;
			$KodeNip =  $res->nip;
			$InstansiRes =  $res->instansi;
			$NamaRes =  $res->nama;
		} ?>


		<?php if ($KodeRegistrasi == "") { ?>

			<center><img class="logo" src="<?= base_url() ?>/assets/tambahan/gambar/not-found.png"  width="300px;"></center>

		<?php } else  { ?>


			<section class="multi_step_form wow slideInUp">  
				<form id="msform" method="POST" action="" enctype="multipart/form-data">
					<input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
					<!-- Tittle -->
					<div class="tittle">
						<h2 style="margin-top: -55px;">Pendaftaran <b>Akun Baru</b></h2>
						<p style="margin-top: -20px;">Silahkan daftar dan buat akun baru di E-Kopetensi / Sudah punya akun ? <b><a href="login-user">Silahkan login</a></b></p>
					</div>
					<!-- progressbar -->
					<ul id="progressbar">
						<li class="active"><b>DATA DIRI</b></li>  
						<li><b>DATA DIRI</b></li> 
						<li><b>KELENGKAPAN DOKUMEN</b></li>
					</ul>
					<!-- fieldsets -->
					<fieldset>
						<h3><b>Silahkan lengkapi data diri anda</b></h3>
						<input type="hidden"  name="kode_registrasi" id="kode_registrasi" value="<?php echo $KodeRegistrasi ?>">
						<input type="hidden"  name="nip" id="nip" value="<?php echo $KodeNip ?>">


						<div class="form-row"> 
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<label>Nama Lengkap</label>
									<input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Auto" value="<?php echo $NamaRes ?>">
								</div>
							</div>
							<div class="col-sm-5">
								<div class="form-group">
									<label>NIK KTP</label>
									<input type="text" class="form-control" name="nik" id="nik" placeholder="NIK KTP">
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label>Asal Instansi</label>
									<input type="text" name="asal_instansi" class="form-control" id="asal_instansi" placeholder="Auto" autocomplete="off" value="<?php echo $InstansiRes ?>">
								</div>
							</div>
							<div class="col-sm-4 col-sm-offset-1">
								<div class="form-group">
									<label>Kategori Instansi Asal</label>
									<select class="select-instansi" name="instansi" id="instansi">
										<option></option>
										<?php foreach ($instansi as $data) { ?>
											<option value="<?php echo $data->nama_kategori ?>">
												<?php echo $data->nama_kategori; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-6 ">
								<div class="form-group">
									<label>Alamat Instansi</label>
									<input type="text" name="alamat_instansi" class="form-control" id="alamat_instansi" placeholder="Alamat Instansi" autocomplete="off">
								</div>
							</div>
							<div class="col-sm-5 col-sm-offset-1">
								<div class="form-group">
									<label>Jenis Kelamin</label>
									<select name="jkel" class="form-control select-jkel" id="jkel">
										<option></option>
										<?php foreach ($jkel as $data) { ?>
											<option value="<?php echo $data->nama_kategori ?>">
												<?php echo $data->nama_kategori; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-5 ">
								<div class="form-group">
									<label>Status Kepegawaian</label>
									<select name="status_pegawai" class="form-control select-pegawai" id="status_pegawai">
										<option></option>
										<?php foreach ($status_pegawai as $data) { ?>
											<option value="<?php echo $data->nama_kategori ?>"><?php echo $data->nama_kategori; ?>
										</option>
									<?php } ?>
								</select>
							</div>
						</div>

					</div>
					<button type="button" style="margin-top: 20px;" class="next action-button">Next<i style="margin-left:10px; " class="fa fa-chevron-right"></i></button>  
				</fieldset>
				<fieldset>
					<h3><b>Silahkan lengkapi data diri anda</b></h3>

					<div class="col-sm-5 col-sm-offset-1">
						<div class="form-group">
							<label>Tempat Lahir</label>
							<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="tempat lahir" onkeyup="javascript:capitalize(this.id, this.value);">
						</div>
					</div>
					<div class="col-sm-5 ">
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input type="text" class="form-control tgl_lahir" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir">
						</div>
					</div>
					<div class="col-sm-5 col-sm-offset-1">
						<div class="form-group">
							<label>Email</label>
							<input type="text" class="form-control" name="emails" id="emails" placeholder="Masukan Email" autocomplete="off">
						</div>
					</div>
					<div class="col-sm-5 ">
						<div class="form-group">
							<label>No Handphone</label>
							<input type="text" class="form-control" name="nomor_hp" id="no_hp" placeholder="Nomor Handphone" onkeypress="return hanyaAngka(event)">
						</div>
					</div> 


					<div class="col-sm-10 col-sm-offset-1"></div>

					<div class="row col-sm-offset-1">
						<div class="col-sm-4 ">
							<div class="form-group">
								<label>Provinsi</label>
								<select name="id_provinsi" id="id_provinsi" class="select-provinsi">
									<option></option>
									<?php foreach ($provinsi as $data) { ?>
										<option value="<?= $data->id_provinsi; ?>"><?= $data->nama_provinsi; ?></option>
									<?php } ?>
								</select>
							</div>
							<div id="loadingImg">
								<img src="<?= base_url(); ?>assets/publik/img/loading-bubble.gif">
								<p><font face="raleway" size="2" color="red">Tunggu sebentar...</font></p>
							</div>
						</div>
						<div class="col-sm-4 ">
							<div class="form-group">
								<label>Kab / Kota</label>
								<select name="id_kota" id="id_kota" class="select-kota">
								</select>
							</div>
						</div>
					</div>





					<button type="button" style="margin-top: 20px;" class="action-button previous previous_button"><i style="margin-right:10px; " class="fa fa-chevron-left"></i>Back</button>
					<button type="button" style="margin-top: 20px;" class="next action-button">Next<i style="margin-left:10px; " class="fa fa-chevron-right"></i></button>   
				</fieldset>
				<fieldset>
					<h3><b>Silahkan lengkapi berkas file pendaftaran</b></h3> 
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label >KTP</label>
							<input type="file" name="others[]" class="form-control" id="ktp" onchange="return cekMandatoryPdf('ktp')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label>Sertifikat Jenjang Kopetensi</label>
							<input type="file" name="others[]" class="form-control" id="sertifikat_kopetensi"  onchange="return cekMandatoryPdf('sertifikat_kopetensi')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label>STTPL</label>
							<input type="file" name="others[]" class="form-control" id="sttpl" onchange="return cekMandatoryPdf('sttpl')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label>Ijazah / SKL ( Surat Keterangan Lulus )</label>
							<input type="file" name="others[]" class="form-control" id="ijazah" onchange="return cekMandatoryPdf('ijazah')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label>SIM</label>
							<input type="file" name="others[]" class="form-control" id="sim" onchange="return cekMandatoryPdf('sim')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-7 col-sm-offset-1">
						<div class="form-group">
							<label>TTD Penguji</label>
							<input type="file" name="others[]" class="form-control" id="ttd" onchange="return cekMandatoryPdf('ttd')"/>
						</div>
					</div>
					<div class="col-sm-3 jarak-label ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .pdf</span></h4>
						</div>
					</div>
					<div class="col-sm-5 col-sm-offset-1">
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" placeholder="password" id="password2" name="password" aria-describedby="sizing-addon2">
						</div>
						<div class="iconic-input col-lg-5" style="margin-left: 15px; margin-right: 30px;">
							<label class="checker">
								<input type="checkbox" onclick="ShowPassword2()">
								<span class="checkmark"></span>
							</label><span class="tulisan-show" style="margin-left: -30px;">Show Password</span>
						</div>
					</div>
					<div class="col-sm-5 ">
						<div class="form-group">
							<label>Konfirmasi Password</label>
							<input type="password" class="form-control" placeholder="password" id="password3" name="password" aria-describedby="sizing-addon2">
						</div>
						<div class="iconic-input col-lg-5" style="margin-left: 15px; margin-right: 30px;">
							<label class="checker">
								<input type="checkbox" onclick="ShowPassword3()">
								<span class="checkmark"></span>
							</label><span class="tulisan-show" style="margin-left: -30px;">Show Password</span>
						</div>
					</div>

					<div class="form-group">
						<div id="slider">
							<img class="img-thumbnail" style="margin-top: 20px;" src="<?= base_url() ?>/assets/publik/img/tidak-ada.jpg" width="150px" />
						</div>
					</div>
					<div class="col-sm-8 col-sm-offset-1">
						<div class="form-group">
							<input type="file"  name="photo" id="gambar" onchange="return foto_regis()"/>
						</div>
					</div>
					<div class="col-sm-3 ">
						<div class="form-group">
							<h4><span class="label label-default">Max: 300 kb. Ekstensi .jpg | .png</span></h4>
						</div>
					</div>
					<button type="button" style="margin-top: 20px;" class="action-button previous previous_button"><i style="margin-right:10px; " class="fa fa-chevron-left"></i>Back</button>
					<button id="hilang2" type="submit" style="margin-top: 20px;" class='action-button btn-fill btn-info btn-wd btn-sm'><i class="fa fa-save"></i>&nbsp; Daftar</button>
					<button id="btn_loading2" type='submit' style='margin-top: 20px;' class='action-button btn-fill btn-info btn-wd btn-sm' disabled><i class='fa fa-refresh fa-spin'></i>&nbsp;Wait...</button>
				</fieldset> 
			</form> 
		</section>
		<div class="col-sm-10 col-sm-offset-1">
			<div class="header" style="margin-top: -40px;"><center><h3><b>Informasi !!</b></h3></center></div><br>
			<ul style="margin-top: -25px;">
				<li>* Foto terbaru bagi ASN mengenakan Pakaian Penguji / PDH dan NON ASN mengenakan kemeja putih latar abu-abu.</li>
				<li>* Notifikasi pengaktifan akun akan dikirim melalui email ( <b>email yang di daftarkan</b> ).</li>
				<li>* Pastikan data sesuai dengan ketentuan yang telah tercantum di form registrasi karena data yang sesuai akan kami proses. </li>
			</ul>
		</div> 
	</section>
<?php } ?>
</div>

<script  src="<?= base_url(); ?>assets/publik/script-register.js"></script>
<!--   Core JS Files   -->
<script src="<?= base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>assets/publik/registrasi/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/publik/registrasi/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/publik/registrasi/js/gsdk-bootstrap-wizard.js"></script>
<script src="<?= base_url(); ?>assets/publik/registrasi/js/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#btn_loading2").hide();
	$('#msform').submit(function (e) {
		e.preventDefault();
		var error = 0;
		var message = "";

		if (error == 0) {
			var nama_lengkap = $("#nama_lengkap").val();
			var nama_lengkap = nama_lengkap.trim();
			if (nama_lengkap.length == 0) {
				error++;
				message = "Nama wajib di isi.";
			}
		}

		if (error == 0) {
			var nik = $("#nik").val();
			var nik = nik.trim();
			if (nik.length == 0) {
				error++;
				message = "NIK wajib di isi.";
			}
		}

		if (error == 0) {
			var asal_instansi = $("#asal_instansi").val();
			var asal_instansi = asal_instansi.trim();
			if (asal_instansi.length == 0) {
				error++;
				message = "Asal Instansi wajib di isi.";
			}
		}

		if (error == 0) {
			var instansi = $("#instansi").val();
			var instansi = instansi.trim();
			if (instansi.length == 0) {
				error++;
				message = "Instansi wajib di isi.";
			}
		}
		if (error == 0) {
			var alamat_instansi = $("#alamat_instansi").val();
			var alamat_instansi = alamat_instansi.trim();
			if (alamat_instansi.length == 0) {
				error++;
				message = "Alamat Instansi wajib di isi.";
			}
		}


		if (error == 0) {
			var jkel = $("#jkel").val();
			var jkel = jkel.trim();
			if (jkel.length == 0) {
				error++;
				message = "Jenis Kelamin wajib di isi.";
			}
		}
		if (error == 0) {
			var status_pegawai = $("#status_pegawai").val();
			var status_pegawai = status_pegawai.trim();
			if (status_pegawai.length == 0) {
				error++;
				message = " Status Pegawai wajib di isi.";
			}
		}
		if (error == 0) {
			var tempat_lahir = $("#tempat_lahir").val();
			var tempat_lahir = tempat_lahir.trim();
			if (tempat_lahir.length == 0) {
				error++;
				message = " Tempat Lahir wajib di isi.";
			}
		}
		if (error == 0) {
			var tgl_lahir = $("#tgl_lahir").val();
			var tgl_lahir = tgl_lahir.trim();
			if (tgl_lahir.length == 0) {
				error++;
				message = " Tanggal Lahir wajib di isi.";
			}
		}
		if (error == 0) {
			var emails = $("#emails").val();
			var emails = emails.trim();
			if (emails.length == 0) {
				error++;
				message = " Email harus diisi !!";
			} else if (!cekemail(emails)) {
				error++;
				message = "Format Email tidak sesuai (admin@gmail.com). ";
			}
		}
		if (error == 0) {
			var no_hp = $("#no_hp").val();
			var no_hp = no_hp.trim();
			if (no_hp.length == 0) {
				error++;
				message = "No Telpon wajib di isi.";
			}
		}

		if (error == 0) {
			var provinsi = $("#id_provinsi").val();
			var provinsi = provinsi.trim();
			if (provinsi.length == 0) {
				error++;
				message = "Provinsi wajib di isi.";
			}
		}
		if (error == 0) {
			var kota = $("#id_kota").val();
			var kota = kota.trim();
			if (kota.length == 0) {
				error++;
				message = "Kota wajib di isi.";
			}
		}
		
		if (error == 0) {
			var ktp = $("#ktp").val();
			var ktp = ktp.trim();
			if (ktp.length == 0) {
				error++;
				message = "File KTP wajib di isi.";
			}
		}
		if (error == 0) {
			var sertifikat_kopetensi = $("#sertifikat_kopetensi").val();
			var sertifikat_kopetensi = sertifikat_kopetensi.trim();
			if (sertifikat_kopetensi.length == 0) {
				error++;
				message = "File Sertifikasi Kopetensi wajib di isi.";
			}
		}
		if (error == 0) {
			var sttpl = $("#sttpl").val();
			var sttpl = sttpl.trim();
			if (sttpl.length == 0) {
				error++;
				message = "File STTPL wajib di isi.";
			}
		}
		if (error == 0) {
			var ijazah = $("#ijazah").val();
			var ijazah = ijazah.trim();
			if (ijazah.length == 0) {
				error++;
				message = "File Ijazah / SKL wajib di isi.";
			}
		}
		if (error == 0) {
			var sim = $("#sim").val();
			var sim = sim.trim();
			if (sim.length == 0) {
				error++;
				message = "File SIM wajib di isi.";
			}
		}
		if (error == 0) {
			var ttd = $("#ttd").val();
			var ttd = ttd.trim();
			if (ttd.length == 0) {
				error++;
				message = "File Tanda Tangan Penguji wajib di isi.";
			}
		}
		if (error == 0) {
			var password2 = $("#password2").val();
			var password2 = password2.trim();
			if (password2.length == 0) {
				error++;
				message = "Password wajib di isi.";
			}
		}
		if (error == 0) {
			var password3 = $("#password3").val();
			var password3 = password3.trim();
			if (password3.length == 0) {
				error++;
				message = "Kofirmasi Password wajib di isi.";
			}
		}
		if (error == 0) {
			var gambar = $("#gambar").val();
			var gambar = gambar.trim();
			if (gambar.length == 0) {
				error++;
				message = "Foto wajib di isi.";
			}
		}
		if (error == 0) {
			$.ajax({
				method: 'POST',
				beforeSend: function () {
					$("#hilang2").hide();
					$("#btn_loading2").show();
				},
				url: '<?= base_url('save-akun-nrp'); ?>',
				type: "post",
				data: new FormData(this),
				processData: false,
				contentType: false,
				cache: false,
			}).done(function (data) {
				var result = jQuery.parseJSON(data);
				if (result.status == true) {
					document.getElementById("msform").reset();
					$('img.img-thumbnail').attr("src", "'<?= base_url(); ?>'assets/publik/img/images/tidak-ada.jpg");
					$('img.img-thumbnail').attr("width", "150px");
					$("#btn_loading2").hide();
					$("#hilang2").show();
					toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
						"closeButton": true});
					setTimeout("window.location='<?= base_url('sukses-daftar'); ?>'", 5000);
				} else {
					$("#btn_loading2").hide();
					$("#hilang2").show();
					toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
						"closeButton": true});
				}
			});
		} else {
			toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
				"closeButton": true});
		}
	});

$("#loadingImg").hide();
$('#id_provinsi').change(function () {
	var id_provinsi = $(this).val();
	console.log(id_provinsi);
	$.ajax({
		beforeSend: function () {
			$("#loadingImg").show();
		},
		url: "<?= base_url(); ?>ajax-list-kota",
		method: "POST",
		data: {id_provinsi: id_provinsi},
		async: false,
		dataType: 'json',
		success: function (data) {
			var html = '';
			var i;
			for (i = 0; i < data.length; i++) {
				html += '<option value="' + data[i].id_kota + '" kode="' + data[i].kode + '">' + data[i].nama_kota + '</option>';
			}
			$('#id_kota').html(html);
			$("#loadingImg").fadeOut();
		}
	});
});

$(function(){
	$("#loadingImg3").hide();
	$.ajaxSetup({
		type:"POST",
		url: "<?php echo base_url('ajax-nrp') ?>",
		cache: true,
	});

	$("#id_nrp").change(function(){
		var value=$(this).val();
		if(value>0){
			$.ajax({
				beforeSend: function (){
					$("#loadingImg3").fadeIn();
				},
				data:{modul:'nama_lengkap',id:value},
				success: function(respond){
					$("#nama_lengkap").val(respond);
					$("#loadingImg3").fadeOut(); 
					console.log(respond);
				}
			})
		}
	});

	$("#id_nrp").change(function(){
		var value=$(this).val();
		if(value>0){
			$.ajax({
				beforeSend: function (){
					$("#loadingImg3").fadeIn();
				},
				data:{modul:'asal_instansi',id:value},
				success: function(respond){
					$("#asal_instansi").val(respond);
					console.log(respond);
				}
			})
		}
	});

	$("#id_nrp").change(function(){
		var value=$(this).val();
		if(value>0){
			$.ajax({
				beforeSend: function (){
					$("#loadingImg3").fadeIn();
				},
				data:{modul:'kode_registrasi',id:value},
				success: function(respond){
					$("#kode_registrasi").val(respond);
					$("#loadingImg3").fadeOut(); 
					console.log(respond);
				}
			})
		}
	});

	$("#id_nrp").change(function(){
		var value=$(this).val();
		if(value>0){
			$.ajax({
				beforeSend: function (){
					$("#loadingImg3").fadeIn();
				},
				data:{modul:'nip',id:value},
				success: function(respond){
					$("#nip").val(respond);
					$("#loadingImg3").fadeOut(); 
					console.log(respond);
				}
			})
		}
	});

})


jQuery(document).ready(function () {
	jQuery('#nama_lengkap').keyup(function ()
	{
		var str = jQuery('#nama_lengkap').val();
		var spart = str.split(" ");
		for (var i = 0; i < spart.length; i++)
		{
			var j = spart[i].charAt(0).toUpperCase();
			spart[i] = j + spart[i].substr(1);
		}
		jQuery('#nama_lengkap').val(spart.join(" "));

	});
});

$(".select-jenis-jenjang").select2({
	placeholder: "Pilih Jenjang",
	allowClear: true
});
$(".select-status-kepegawaian").select2({
	placeholder: "Pilih Status Kepegawaian",
	allowClear: true
});
$(".pilih-reg").select2({
	placeholder: "Silahkan Pilih",
	allowClear: true
});
$(".select-jkel").select2({
	placeholder: "Pilih Jenis Kelamin",
	allowClear: true
});
$(".select-nrp").select2({
	placeholder: "Pilih Nama / NIP / Kode Registrasi",
	allowClear: true
});


$(".select-pegawai").select2({
	placeholder: "Pilih status pegawai",
	allowClear: true
});
$(".select-instansi").select2({
	placeholder: "Pilih Instansi",
	allowClear: true
});
$(".select-provinsi").select2({
	placeholder: "Pilih Provinsi",
	allowClear: true
});
$(".select-kota").select2({
	placeholder: "Pilih Provinsi Dahulu",
	allowClear: true
});
$(".tgl_lahir").datepicker({
	orientation: "left",
	autoclose: !0,
	format: 'dd-mm-yyyy'
});

    // untuk show hide password
    $(".toggle-password").click(function () {
    	$(this).toggleClass("fa-eye fa-eye-slash");
    	var input = $($(this).attr("toggle"));
    	if (input.attr("type") == "password") {
    		input.attr("type", "text");
    	} else {
    		input.attr("type", "password");
    	}
    });

    function capitalize(textboxid, str) {
        // string with alteast one character
        if (str && str.length >= 1)
        {
        	var firstChar = str.charAt(0);
        	var remainingStr = str.slice(1);
        	str = firstChar.toUpperCase() + remainingStr;
        }
        document.getElementById(textboxid).value = str;
    }

    function hanyaAngka(evt) {
    	var charCode = (evt.which) ? evt.which : event.keyCode
    	if (charCode > 31 && (charCode < 48 || charCode > 57))
    		return false;
    	return true;
    }

    //untuk foto registrasi
    function foto_regis() {
    	var fileInput = document.getElementById('gambar');
    	var filePath = fileInput.value;
    	var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    	if (!allowedExtensions.exec(filePath)) {
    		toastr.error('maaf masukan gambar dengan format .jpeg/.jpg/.png only', 'Warning', {timeOut: 5000}, toastr.options = {
    			"closeButton": true});
    		fileInput.value = '';
    		return false;
    	} else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
            	var reader = new FileReader();
            	reader.onload = function (e) {
            		document.getElementById('slider').innerHTML = '<img class="img-thumbnail" src="' + e.target.result + '" width="150" height="auto"/>';
            	};
            	reader.readAsDataURL(fileInput.files[0]);
            }
        }
        var ukuran = document.getElementById("gambar");
        if (ukuran.files[0].size > 307200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 5MB", "warning");
            toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
            	"closeButton": true});
            ukuran.value = '';
    //                    setTimeout(location.reload.bind(location), 500);
    return false;
}
}

function cekMandatoryPdf(variable) {
	var fileInput = document.getElementById(variable).value;
	if (fileInput != '')
	{
		var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                // swal("Peringatan", "File harus format .pdf", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                	"closeButton": true});
                document.getElementById(variable).value = '';
                return false;
            }
            var ukuran = document.getElementById(variable);
            if (ukuran.files[0].size > 307200)  // validasi ukuran size file
            {
                // swal("Peringatan", "File harus maksimal 5MB", "warning");
                toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                	"closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    function cekemail(a) {
    	re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    	return re.test(a);
    }

    $(function () {
    	$('.pencet').hide();
    	$("#check-all").click(function () {
    		if ($(this).is(":checked")) {
    			$(".check-item").prop("checked", true);
    			$('.pencet').fadeIn("slow");
    		} else {
    			$('.pencet').fadeOut("slow");
    			$(".check-item").prop("checked", false);
    		}
    	});
    });

    $(function () {
    	$('.pencet').hide();
    	$(".check-item").click(function () {
    		if ($(this).is(":checked")) {
    			$('.pencet').fadeIn("slow");
    		} else {
    			$('.pencet').fadeOut("slow");
    		}
    	});
    });
</script>