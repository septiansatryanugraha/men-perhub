<?php 
$page = 'tentang';
$pageTitle = 'Tentang Kami';
$deskripsi = 'Where can i get some ?';
include('header.php'); 
?>
	<div class="container">
	  <?php include('breadcrumb.php'); ?>
		<div class="row">
			<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="flexslider flexslider-thumbnail-gallery ">
						<ul class="slides">
							<li data-tooltip="" data-thumb="http://placehold.it/111x75"><img src="http://placehold.it/555x368" alt=""/>
							<div class="project-hover">
								<a class="search-icon prettyPhoto" href="http://placehold.it/1160x350"></a>
							</div>
							</li>
							<li data-tooltip="" data-thumb="http://placehold.it/111x75"><img src="http://placehold.it/555x368" alt=""/>
							<div class="project-hover">
								<a class="search-icon prettyPhoto" href="http://placehold.it/1160x350"></a>
							</div>
							</li>
							<li data-tooltip="" data-thumb="http://placehold.it/111x75"><img src="http://placehold.it/555x368" alt=""/>
							<div class="project-hover">
								<a class="search-icon prettyPhoto" href="http://placehold.it/1160x350"></a>
							</div>
							</li>
							<li data-tooltip="" data-thumb="http://placehold.it/111x75"><img src="http://placehold.it/555x368" alt=""/>
							<div class="project-hover">
								<a class="search-icon prettyPhoto" href="http://placehold.it/1160x350"></a>
							</div>
							</li>
							<li data-tooltip="" data-thumb="http://placehold.it/111x75"><img src="http://placehold.it/555x368" alt=""/>
							<div class="project-hover">
								<a class="search-icon prettyPhoto" href="http://placehold.it/1160x350"></a>
							</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h3 class="special-text wow animated fadeInDown services-special-h3" data-animation="fadeInDown">What We Do</h3>
					<p>
						 Maecenas varius, purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.
					</p>
					<p>
						 Maecenas varius, purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.
					</p>
					<p>
						 Purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.
					</p>
				</div>
			</div>
			<div class="clearfix clearfix-height40">
			</div>
			<div class="full_bg full-width-bg services-full-bg" data-animation="">
				<h2 class="special-text marine-header6-h2" data-animation="">Our Services </h2>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="marine-icon-color icons icon-heart-empty"></i>
							<h3 class="marine-header6-h3-color">Fully Customizable</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="marine-icon-color icons icon-eye-outline"></i>
							<h3 class="marine-header6-h3-color">Retina Ready</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="marine-icon-color icons icon-trophy"></i>
							<h3 class="marine-header6-h3-color">Global Solutions</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix clearfix-height30">
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="marine-icon-color icons icon-gauge"></i>
							<h3 class="marine-header6-h3-color">100% Responsive</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="icons icon-lightbulb marine-icon-color"></i>
							<h3 class="marine-header6-h3-color">User Friendly</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<ul class="services-list ">
							<li>
							<i class="icons icon-back-in-time marine-icon-color"></i>
							<h3 class="marine-header6-h3-color">Live Support</h3>
							<p class="marine-header6-p-color">
								 Curabitur non sagittis velit. Nam vehicula tempor ultricies. Sed vulputate mauris et elementum sodales.
							</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h3>Tabs</h3>
					<div id='tab-24803' class='tabs animated fadeIn style2'>
						<div class="tab-header">
							<ul class=''>
								<li><a href="#tab1"><i class="icons icon-rocket"></i>WEB DEVELOPMENT</a></li>
								<li><a href="#tab2"><i class="icons icon-desktop"></i>HTML/CSS3</a></li>
								<li><a href="#tab3"><i class="icons icon-tablet"></i>CUSTOMIZATION</a></li>
								<li><a href="#tab4"><i class="icons icon-mobile"></i>ONLINE SUPPORT</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div id="tab1" class="tab">
								<h4><span class="services-span-color">Web Developement</span></h4>
								<p>
									 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo erat at lacus convallis convallis.
								</p>
								<ul class="list check-style">
									<li>Branding</li>
									<li>HTML/CSS</li>
									<li>Illustration</li>
								</ul>
							</div>
							<div id="tab2" class="tab">
								<h3><span class="services-span-color">Web Developement</span></h3>
								<p>
									 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo erat at lacus convallis convallis.
								</p>
								<ul class="list check-style">
									<li>Branding</li>
									<li>HTML/CSS</li>
									<li>ILLUSTRATIONS</li>
								</ul>
							</div>
							<div id="tab3" class="tab">
							<p>
								 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo erat at lacus convallis convallis.
							</p>
							<ul class="list check-style">
								<li>Branding</li>
								<li>HTML/CSS</li>
								<li>ILLUSTRATIONS</li>
							</ul>
						</div>
						<div id="tab4" class="tab">
						<p>
							 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo erat at lacus convallis convallis.
						</p>
						<ul class="list check-style">
							<li>Branding</li>
							<li>HTML/CSS</li>
							<li>ILLUSTRATIONS</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<h3>Our Skills</h3>
			<div class=" style1">
				<p>
					 Responsive Web Design
				</p>
				<div class="progressbar " data-percent="68">
					<div class="progress-width">
					</div>
					<span class="progress-percent"></span>
				</div>
			</div>
			<div class=" style1">
				<p>
					 IOS Development
				</p>
				<div class="progressbar " data-percent="86">
					<div class="progress-width">
					</div>
					<span class="progress-percent"></span>
				</div>
			</div>
			<div class=" style1">
				<p>
					 Branding
				</p>
				<div class="progressbar " data-percent="40">
					<div class="progress-width">
					</div>
					<span class="progress-percent"></span>
				</div>
			</div>
			<div class=" style1">
				<p>
					 Java Script Game Development
				</p>
				<div class="progressbar " data-percent="90">
					<div class="progress-width">
					</div>
					<span class="progress-percent"></span>
				</div>
			</div>
			<div class=" style1">
				<p>
					 Graphic Design
				</p>
				<div class="progressbar " data-percent="70">
					<div class="progress-width">
					</div>
					<span class="progress-percent"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix clearfix-height40">
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<h3>Accordion</h3>
			<div class="accordions">
				<div class="accordion accordion-active">
					<div class="accordion-header">
						<div class="accordion-icon">
						</div>
						<h5>Vivamus orci sem sectetur</h5>
					</div>
					<div class="accordion-content display-block">
						<p>
							 Sed entum velit vel ipsum bibendum em lacus, itor et aliquam eget, iaculis id lacus. Praesent tudin.
						</p>
						<p>
							 Aiquam eget, iaculis id lacus. Praesent tudin. Ut sem lacus, ttitor putate uam mi nec hendrerit.
						</p>
					</div>
				</div>
				<div class="accordion ">
					<div class="accordion-header">
						<div class="accordion-icon">
						</div>
						<h5>Sed entum velit vel ipsum</h5>
					</div>
					<div class="accordion-content">
						 Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
					</div>
				</div>
				<div class="accordion ">
					<div class="accordion-header">
						<div class="accordion-icon">
						</div>
						<h5>Portittor et aliquam eget</h5>
					</div>
					<div class="accordion-content">
						 Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<h3 data-animation="">Testimonials</h3>
			<div class="testimonial ">
				<div class="testimonial-header">
					<div class="testimonial-image">
						<img src="http://placehold.it/81x81" alt="Jane Blue"/>
					</div>
					<div class="testimonial-meta">
						<span class="testimonial-author">Jane Blue</span>
						<span class="testimonial-job">CEO, Big Company </span>
					</div>
				</div>
				<blockquote class="testimonial-quote">
					 Ert vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.
				</blockquote>
				<div class="testimonial-desc">
					<p>
						 Vivamus orci sem, consectetur ut vestibulum a, semper ac dui. Proin vulputate aliquam mi nec rerit.
					</p>
					<p>
						 Sed entum velit vel ipsum bibendum tristique. Ut sem lacus, ttitor putate liquam mi nec hendrerit. Sed entum velit vel ipsum bibendum.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix clearfix-height40">
	</div>
	<div class="full_bg full-width-bg services-full-bg-solid" data-animation="">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6">
				<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
				<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
				<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6">
				<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
			</div>
		</div>
		<div class="clearfix">
		</div>
	</div>
	</section>
</div>
</div>
<!-- /Container -->
<?php include('footer.php'); ?>