<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width"/>
  <style type="text/css">
    * { margin: 0; padding: 0; font-size: 100%; font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; line-height: 1.65; }
    img { max-width: 100%; margin: 0 auto; display: block; }
    body, #body-wrap { width: 100% !important; height: 100%; background: #f8f8f8; }
    a { color: #71bc37; text-decoration: none; }
    a:hover { text-decoration: underline; }
    #text-center { text-align: center; }
    #text-right { text-align: right; }
    #text-left { text-align: left; }
    #button { display: inline-block; color: white; background: #301d6e; border: solid #301d6e; border-width: 10px 20px 8px; font-weight: bold; border-radius: 4px; }
    #button:hover { text-decoration: none; }
    h1, h2, h3, h4, h5, h6 { margin-bottom: 20px; line-height: 1.25; }
    h1 { font-size: 32px; }
    h2 { font-size: 28px; }
    h3 { font-size: 24px; }
    h4 { font-size: 20px; }
    h5 { font-size: 16px; }
    p, ul, ol { font-size: 16px; font-weight: normal; margin-bottom: 20px; }
    #container { display: block !important; clear: both !important; margin: 0 auto !important; max-width: 580px !important; }
    #container table { width: 100% !important; border-collapse: collapse; }
    #container #masthead { padding: 15px 0; background: white; border-bottom: #301d6e solid 10px; color: white; }
    #container #masthead h1 { margin: 0 auto !important; max-width: 90%; text-transform: uppercase; }
    #container #content { background: white; padding: 30px 35px; }
    #container #content.footer { background: none; }
    #container #content.footer p { margin-bottom: 0; color: #888; text-align: center; font-size: 14px; }
    #container #content.footer a { color: #888; text-decoration: none; font-weight: bold; }
    #container #content.footer a:hover { text-decoration: underline; }

    .table1 {
      font-family: sans-serif;
      color: #444;
      border-collapse: collapse;
      width: 50%;
      border: 3px solid #f2f5f7;
    }

    .table1 tr th{
      background: #35A9DB;
      color: #fff;
      font-weight: normal;
    }

    .table1, th, td {
      padding: 8px 20px;
    }

    .table1 tr:hover {
      background-color: #f5f5f5;
    }

    .table1 tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    .gap_jarak {
      margin-bottom: 15px;
    }

  </style>
</head>
<body>

  <table id="body-wrap">
    <tr>
      <td id="container">

        <!-- Message start -->
        <table>
          <tr>
            <td align="center" id="masthead">
              <center><img class="logo" src="<?= base_url() ?>/assets/publik/img/logo-e_kopetensi.png"  width="300px;"></center>
            </td>
          </tr>
          
          <tr>
            <td id="content">

              <center><h2>Hallo, <?php echo $brand->nama_lengkap; ?></h2></center>
              <center>Berikut informasi Username dan Akun Anda yang terdaftar di sistem.</p></center>
              <div class="gap_jarak"></div>
              <table border="1" class="table1" style="margin-bottom: 22px;">
                <tr>
                  <td width="30%"><b>Username</b></td>
                  <td width="70%"><?php echo $brand->email; ?></td>
                </tr>

                <tr>
                  <td width="30%"><b>Password</b></td>
                  <td width="40%"><?php echo base64_decode($brand->password); ?></td>
                </tr>

              </table>
              
              <center><p><b>Silahkan ubah password di Dashboard User anda <br> (Jika dibutuhkan)</b></p></center>
              <br><br>

              <center><p><b><em>E-KOMPETENSI - Kementerian Perhubungan</em></b></p></center>
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
</body>
