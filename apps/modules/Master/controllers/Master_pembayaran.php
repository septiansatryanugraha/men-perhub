<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_pembayaran extends AUTH_Controller
{
    const __tableName = 'tbl_bukti_pembayaran';
    const __tableName2 = 'tbl_diklat';
    const __tableId = 'id_pembayaran';
    const __tableId2 = 'kode_pengajuan';
    const __folder = 'v_pembayaran/';
    const __kode_menu = 'master-pembayaran';
    const __title = 'Pembayaran Biling PNBP  ';
    const __model = 'M_pembayaran';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_pembayaran->getData(1, $filter);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-red">Belum Lunas</span>';
            if ($brand->status == 'Lunas') {
                $status = '<span class="badge bg-blue">Lunas</span>';
            }

            $KeteranganBayar = '' . $brand->keterangan . '<hr>  <span class="badge bg-red">Belum ada bukti pembayaran </span>';
            if ($brand->file_upload_pembayaran != '') {
                $KeteranganBayar = '' . $brand->keterangan . '<hr> Bukti Pembayaran PNBP : <br><a class="merah" href="' . $brand->file_upload_pembayaran . '" target="_blank"><i class="fa fa-file-pdf-o pdf" aria-hidden="true"></i>&nbsp; ' . $brand->nama_file_upload . '</br>';
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $KeteranganBayar;
            $row[] = $status;

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status != 'Lunas' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-pembayaran') . "/" . $brand->id_pembayaran . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-pembayaran' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_pembayaran . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_pembayaran->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Belum Lunas', 'Lunas']);
                $this->loadkonten(self::__folder . 'edit-pembayaran', $data);
            } else {
                echo "<script>alert('" . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpload($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $status = $this->input->post('status');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pembayaran->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status Pembayaran wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['status' => $status], ['id_pembayaran' => $id]);
                $this->db->update('tbl_perpanjangan', ['status_pembayaran' => $status], ['kode_pengajuan' => $checkValid->kode_pengajuan]);
                $this->db->update('tbl_diklat', ['status_pembayaran' => $status], ['kode_pengajuan' => $checkValid->kode_pengajuan]);
                $this->db->update('tbl_kenaikan_jenjang', ['status_pembayaran' => $status], ['kode_pengajuan' => $checkValid->kode_pengajuan]);

                $data3 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => 'Pembayaran Billing PNBP user <b>' . $checkValid->nama_pemohon . '</b> sudah lunas dan sertifikat Kompetensi akan segera di kirim ke tempat anda. ',
                    'status' => 'Pembayaran sudah lunas',
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert('tbl_history', $data3);

                $config = [
                    'useragent' => 'Codeigniter',
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_port' => 587,
                    'smtp_crypto' => 'tls',
                    'smtp_timeout' => 100,
                    'smtp_user' => 'angkasamuda20@gmail.com',
                    'smtp_pass' => 'agkasamuda2020',
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
                ];

                $this->load->library('email');
                $this->email->initialize($config);

                $this->email->from('angkasamuda20@gmail.com');
                $this->email->to($checkValid->email);

                $this->email->subject('Notifikasi Pembayaran Billing PNBP');
                $message = '
		<h3 align="center">Informasi Pembayaran Billing PNBP</h3>
		<center><table border="1" width="50%" cellpadding="5">
		<tr>
       	<td width="30%">No Kode Pengajuan</td>
	   	<td width="70%">' . $checkValid->kode_pengajuan . '</td>
       	</tr>

       	<tr>
       	<td width="30%">Nama User</td>
	   	<td width="40%">' . $checkValid->nama_pemohon . '</td>
       	</tr>

       	<tr>
       	<td width="30%">Status Pembayaran</td>
	   	<td width="40%">' . $status . '</td>
       	</tr>

       	<tr>
       	<td width="30%">Keterangan</td>
	   	<td width="40%">Telah melunasi pembayaran Billing PNBP</td>
       	</tr>

       	</table></center>';
                $this->email->message($message);
                $this->email->send();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pembayaran->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                //hapus unlink file surat
                if (file_exists($checkValid->file_pnbp)) {
                    unlink($checkValid->file_pnbp);
                }
                if (file_exists($checkValid->file_upload_pembayaran)) {
                    unlink($checkValid->file_upload_pembayaran);
                }
                if (file_exists($checkValid->file_perintah)) {
                    unlink($checkValid->file_perintah);
                }

                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
