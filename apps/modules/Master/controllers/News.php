<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends AUTH_Controller
{
    const __tableName = 'tbl_news';
    const __tableId = 'id_news';
    const __folder = 'v_news/';
    const __kode_menu = 'news';
    const __title = 'Berita ';
    const __model = 'M_news';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    function upload_gambar()
    {
        if (isset($_FILES["image"]["name"])) {
            $config['upload_path'] = './upload/news/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $this->upload->display_errors();
                return FALSE;
            } else {
                $data = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './upload/news/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '60%';
                $config['width'] = 800;
                $config['height'] = 800;
                $config['new_image'] = './upload/news/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'upload/news/' . $data['file_name'];
            }
        }
    }

    function delete_image()
    {
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if (unlink($file_name)) {
            echo 'File Delete Successfully';
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_news->getData();
        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $status = '<span class="badge bg-green">Non Aktif</span">';
            if ($brand->status == 'Aktif') {
                $status = '<span class="badge bg-red">Aktif</span">';
            }

            if ($brand->gambar == '') {
                $gambar = '<img src="' . base_url() . '/upload/foto/tidak-ada.png" width="100" /></center>';
            } else {

                $gambar = '<a href="' . $brand->gambar . '" target="_blank"><img class="img-thumbnail" src="' . base_url() . $brand->gambar . '"   width="100" /></a>';
            }

            $limit = word_limiter($brand->deskripsi, 50);

            $row = [];
            $row[] = $no;
            $row[] = $gambar;
            $row[] = $brand->judul;
            $row[] = $limit;
            $row[] = $status;
            $row[] = $brand->created_by;

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-news') . "/" . $brand->id_news . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-news' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_news . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['status'] = $this->M_utilities->selectStatusGrup([], ['Non aktif', 'Aktif']);
            $this->loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $judul = $this->input->post('judul');
        $deskripsi = $this->input->post('deskripsi');
        $status = $this->input->post('status');
        $judulBerita = replace_title($this->input->post('judul'));
        $slug = $judulBerita . '.html'; // tambahkan ektensi .html pada slug


        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'judul' => $judul,
                    'deskripsi' => $deskripsi,
                    'permalink' => $slug,
                    'status' => $status,
                    'created_by' => $username,
                    'created_date' => $datetime,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                $config['upload_path'] = "./upload/news/";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2048'; //maksimum besar file 2M
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload("gambar")) {
                    $image_data = $this->upload->data();
                    $path['link'] = "upload/news/";

                    $data = array_merge($data, [
                        'gambar' => $path['link'] . '' . $image_data['file_name'],
                    ]);
                }
                $result = $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_news->selectById($id);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['datastatus'] = $this->M_utilities->selectStatusGrup([], ['Non aktif', 'Aktif']);
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $judul = $this->input->post('judul');
        $deskripsi = $this->input->post('deskripsi');
        $status = $this->input->post('status');
        $judulBerita = replace_title($this->input->post('judul'));
        $slug = $judulBerita . '.html'; // tambahkan ektensi .html pada slug

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_news->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'judul' => $judul,
                    'deskripsi' => $deskripsi,
                    'permalink' => $slug,
                    'status' => $status,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                $config['upload_path'] = "./upload/news/";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2048'; //maksimum besar file 2M
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload("gambar")) {
                    if (file_exists($lok = FCPATH . $checkValid->gambar)) {
                        unlink($lok);
                    }

                    $image_data = $this->upload->data();
                    $path['link'] = "upload/news/";

                    $data = array_merge($data, [
                        'gambar' => $path['link'] . '' . $image_data['file_name'],
                    ]);
                }

                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_news->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                //hapus unlink file photo
                if (file_exists($checkValid->gambar)) {
                    unlink($checkValid->gambar);
                }

                $result = $this->db->update(self::__tableName, ['gambar' => null, 'deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
