<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_diklat extends AUTH_Controller
{
    const __tableName = 'tbl_diklat';
    const __tableName2 = 'tbl_history';
    const __tableId = 'id_diklat';
    const __folder = 'v_diklat/';
    const __kode_menu = 'master-diklat';
    const __kode_menu2 = 'master-diklat-validasi';
    const __kode_menu3 = 'master-diklat-approve';
    const __title = 'Master Diklat PKB ';
    const __title3 = 'Master Diklat PKB';
    const __model = 'M_diklat';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
        $this->load->model('M_user');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_diklat->getData(1, $filter, ['Pending', 'Belum Verifikasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-red">Status Pending</span>';
            if ($brand->status_pengajuan == 'Proses Verifikasi') {
                $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            } else if ($brand->status_pengajuan == 'Approve') {
                $status = '<span class="badge bg-green">Approve</span>';
            } else if ($brand->status_pengajuan == 'Belum Verifikasi') {
                $status = '<span class="badge bg-red">Belum Verifikasi</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br> Pengajuan Diklat PKB di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_pengajuan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-diklat') . "/" . $brand->id_diklat . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-diklat' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_diklat . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Pending', 'Proses Verifikasi', 'Belum Verifikasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Proses Verifikasi', 'Belum Verifikasi']);
                $data['fold'] = $this->M_diklat->getOneproduct($id);
                $data["berkas"] = glob('upload/berkas_diklat/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $statusPengajuan = $this->input->post('status_pengajuan');
        $catatan = $this->input->post('catatan_diklat');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_diklat->selectById($id, ['Pending', 'Proses Verifikasi', 'Belum Verifikasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($statusPengajuan == 'Proses Verifikasi') {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' sedang di ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi.';
                } else {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi .karena ada beberapa dokumen yang belum memenuhi syarat.';
                }

                $data = [
                    'keterangan_status' => $keteranganStatus,
                    'catatan_diklat' => $catatan,
                    'status_pengajuan' => $statusPengajuan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => $keteranganStatus,
                    'status' => $statusPengajuan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName2, $data2);
                $result = $this->db->insert('tbl_history_diklat_help', $data2);

                $idUser = $checkValid->id_user;
                $API_ACCESS_KEY = "AAAATS1_J3I:APA91bHh840OcSPNiFUFcCTZm1fYw8yzWDndt4OQpv8dvuss64fv8DJqFGWM6i6wypqu7IUS1wciD38MxUtBVj7vUP3fpTiy1N39VL_BwRt8zQp-AyIDPWvVMiNL0dA3cqmatE5Ly5aZ";
                $url = 'https://fcm.googleapis.com/fcm/send';

                $fields = [
                    'to' => '/topics/' . $idUser,
                    'notification' => [
                        'title' => 'E-Kompetensi',
                        'click_action' => 'Notifikasi',
                        'body' => $keteranganStatus,
                        "sound" => "Enabled",
                        "priority" => "High",
                        "android_channel_id" => "1000",
                    ],
                ];
                $fields = json_encode($fields);

                $headers = [
                    'Authorization: key=' . $API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeValidasi()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-validasi';
        $data['title'] = 'Validasi Diklat PKB';
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_validasi', $data);
        }
    }

    public function ajaxValidasi()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu2);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu2);
        $list = $this->M_diklat->getData(1, $filter, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-blue">Proses Verifikasi</span>';
            if ($brand->status_pengajuan == 'Proses Validasi') {
                $status = '<span class="badge bg-blue">Proses Validasi</span>';
            } else if ($brand->status_pengajuan == 'Belum Validasi') {
                $status = '<span class="badge bg-red">Belum Validasi</span>';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon;
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br> Pengajuan Diklat PKB di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status_pengajuan != 'Approve' && $accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-diklat-validasi') . "/" . $brand->id_diklat . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-diklat' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_diklat . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editValidasi($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-validasi';
        $data['title'] = 'Validasi Diklat PKB';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_diklat->M_utilities->selectStatusGrup([], ['Belum Validasi', 'Proses Validasi']);
                $data['fold'] = $this->M_diklat->getOneproduct($id);
                $data["berkas"] = glob('upload/berkas_diklat/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_validasi', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateValidasi($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPengajuan = $this->input->post('status_pengajuan');
        $catatan = $this->input->post('catatan_validasi');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_diklat->selectById($id, ['Proses Verifikasi', 'Proses Validasi', 'Belum Validasi']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($statusPengajuan == 'Proses Validasi') {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' sedang di ' . $statusPengajuan . ' oleh admin sistem Kompetensi.';
                } else {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi .karena ada beberapa dokumen yang belum memenuhi syarat.';
                }

                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan_validasi' => $catatan,
                    'keterangan_status' => $keteranganStatus,
                    'status_pengajuan' => $statusPengajuan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => $keteranganStatus,
                    'status' => $statusPengajuan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);
                $this->db->insert('tbl_history_diklat_help', $data2);
                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);

                $idUser = $checkValid->id_user;
                $API_ACCESS_KEY = "AAAATS1_J3I:APA91bHh840OcSPNiFUFcCTZm1fYw8yzWDndt4OQpv8dvuss64fv8DJqFGWM6i6wypqu7IUS1wciD38MxUtBVj7vUP3fpTiy1N39VL_BwRt8zQp-AyIDPWvVMiNL0dA3cqmatE5Ly5aZ";
                $url = 'https://fcm.googleapis.com/fcm/send';

                $fields = [
                    'to' => '/topics/' . $idUser,
                    'notification' => [
                        'title' => 'E-Kompetensi',
                        'click_action' => 'Notifikasi',
                        'body' => $keteranganStatus,
                        "sound" => "Enabled",
                        "priority" => "High",
                        "android_channel_id" => "1000",
                    ],
                ];
                $fields = json_encode($fields);

                $headers = [
                    'Authorization: key=' . $API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function homeApprove()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-approve';
        $data['title'] = 'Approve Diklat PKB';
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('' . self::__folder . 'home_approve', $data);
        }
    }

    public function ajaxApprove()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = [
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        ];

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_diklat->getData(1, $filter, ['Proses Validasi', 'Lolos', 'Belum Lolos']);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<span class="badge bg-blue">Proses Validasi</span>';
            if ($brand->status_pengajuan == 'Lolos') {
                $status = '<span class="badge bg-green">Lolos Validasi</span>';
            } else if ($brand->status_pengajuan == 'Belum Lolos') {
                $status = '<span class="badge bg-red">Belum Lolos</span>';
            }

            $statusKelulusan = '<span class="badge bg-blue">Belum Diketahui</span>';
            if ($brand->status_kelulusan == 'Lulus') {
                $statusKelulusan = '<span class="badge bg-green">Lulus</span> dengan nilai ' . $brand->nilai . ' ';
            } else if ($brand->status_kelulusan == 'Tidak Lulus') {
                $statusKelulusan = '<span class="badge bg-red">Tidak Lulus</span> dengan nilai ' . $brand->nilai . ' ';
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->kode_pengajuan;
            $row[] = $brand->nama_pemohon . '<br><br> Status kelulusan : ' . $statusKelulusan . '';
            $row[] = $brand->email;
            $row[] = $brand->status_kepegawaian_pemohon;
            $row[] = $status . '<br> ' . $brand->status . '</br>' . date('d-m-Y', strtotime($brand->updated_date)) . '<br>Pengajuan Diklat PKB di Proses oleh : <b>' . $proses . '</b>';

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-diklat-approve') . "/" . $brand->id_diklat . "' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($brand->status_kelulusan == 'Lulus') {
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . base_url('view-nilai-diklat') . "/" . $brand->id_diklat . "' class='klik ajaxify'><i class='fa fa-eye'></i> Lihat Nilai</a></li>";
                    $action .= "    <li><a href='" . base_url('cetak-diklat') . "/" . $brand->id_diklat . "' class='klik' target='__blank'><i class='fa fa-print'></i> Print</a></li>";
                }
            } else {
                if ($brand->status_pengajuan == 'Lolos') {
                    $action .= "    <li><a href='" . base_url('edit-nilai-diklat') . "/" . $brand->id_diklat . "' class='klik ajaxify'><i class='fa fa-tags'></i> Ubah Nilai Diklat</a></li>";
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-diklat' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_diklat . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }
        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];

        echo json_encode($output);
    }

    public function editApprove($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-approve';
        $data['title'] = 'Approve Diklat PKB';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Proses Validasi', 'Lolos', 'Belum Lolos']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['status'] = $this->M_utilities->selectStatusGrup([], ['Lolos', 'Belum Lolos']);
                $data['fold'] = $this->M_diklat->getOneproduct($id);
                $data["berkas"] = glob('upload/berkas_diklat/' . $brand->folder . "/*");
                $this->loadkonten(self::__folder . 'update_approve', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateApprove($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noReg = $this->input->post('no_reg');
        $noBa = $this->input->post('no_ba');
        $tglBa = $this->input->post('tgl_ba');
        $noSertifikat = $this->input->post('no_sertifikat');
        $statusPengajuan = $this->input->post('status_pengajuan');
        $catatan = $this->input->post('catatan_approve');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_diklat->selectById($id, ['Proses Validasi', 'Lolos']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noReg) == 0) {
                $errCode++;
                $errMessage = "No registrasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "No Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglBa) == 0) {
                $errCode++;
                $errMessage = "Tanggal Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSertifikat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sertifikat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($statusPengajuan) == 0) {
                $errCode++;
                $errMessage = "Status Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($statusPengajuan == 'Lolos') {
                    $keteranganStatus = 'user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' telah di nyatakan ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi dan akan dimasukan ke daftar list calon peserta diklat.';
                } else {
                    $keteranganStatus = 'Pengajuan user <b>' . $checkValid->nama_pemohon . '</b> untuk proses Diklat ' . $checkValid->kategori_diklat . ' ' . $statusPengajuan . ' oleh admin sistem E-Kompetensi. Karena ada beberapa dokumen yang belum memenuhi syarat.';
                }

                $data = [
                    'no_reg' => $noReg,
                    'no_ba' => $noBa,
                    'tgl_ba' => date('Y-m-d', strtotime($tglBa)),
                    'no_sertifikat' => $noSertifikat,
                    'catatan_approve' => $catatan,
                    'keterangan_status' => $keteranganStatus,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                $data2 = [
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_user' => $checkValid->id_user,
                    'keterangan_status' => $keteranganStatus,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $this->db->insert(self::__tableName2, $data2);
                $this->db->insert('tbl_history_diklat_help', $data2);
                $this->db->update('tbl_user', ['no_reg' => $noReg], ['id_user' => $checkValid->id_user]);

                $idUser = $checkValid->id_user;
                $API_ACCESS_KEY = "AAAATS1_J3I:APA91bHh840OcSPNiFUFcCTZm1fYw8yzWDndt4OQpv8dvuss64fv8DJqFGWM6i6wypqu7IUS1wciD38MxUtBVj7vUP3fpTiy1N39VL_BwRt8zQp-AyIDPWvVMiNL0dA3cqmatE5Ly5aZ";
                $url = 'https://fcm.googleapis.com/fcm/send';

                $fields = [
                    'to' => '/topics/' . $idUser,
                    'notification' => [
                        'title' => 'E-Kompetensi',
                        'click_action' => 'Notifikasi',
                        'body' => $keteranganStatus,
                        "sound" => "Enabled",
                        "priority" => "High",
                        "android_channel_id" => "1000",
                    ],
                ];
                $fields = json_encode($fields);

                $headers = [
                    'Authorization: key=' . $API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function editNilai($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-approve';
        $data['title'] = 'Nilai Diklat PKB';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['brand'] = $brand;
                $data['ValData'] = $this->M_diklat->selectHistoryDiklat(['id_user' => $brand->id_user])->result();
                $this->loadkonten(self::__folder . 'edit_nilai', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function prosesUpdateNilai($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $jenisJenjang = "";
        $KodeJenjang = "";
        $IdJenjang = "";

        $ne1 = $this->input->post('ne1');
        $na1 = $this->input->post('na1');
        $ne2 = $this->input->post('ne2');
        $na2 = $this->input->post('na2');
        $ne3 = $this->input->post('ne3');
        $na3 = $this->input->post('na3');
        $nilai = $this->input->post('nilai');
        $statusKelulusan = $this->input->post('status_kelulusan');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_diklat->selectById($id, ['Lolos']);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkUser = $this->M_user->selectById($checkValid->id_user);
            if ($checkUser == null) {
                $errCode++;
                $errMessage = "User tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne1) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne2) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ne3) == 0) {
                $errCode++;
                $errMessage = "Nilai wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nilai' => $nilai,
                    'status_kelulusan' => $statusKelulusan,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];

                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);

                if ($statusKelulusan == 'Lulus') {
                    $jenisDiklat = $checkValid->id_kategori_diklat;
                    if ($jenisDiklat == '1') {
                        $jenisJenjang = 'Pembantu Penguji';
                        $KodeJenjang = 'PP1';
                        $IdJenjang = '1';
                    } elseif ($jenisDiklat == '3') {
                        $jenisJenjang = 'Penguji Tingkat Satu';
                        $namaDiklat = 'Penguji Tingkat Satu';
                        $KodeJenjang = 'PT1';
                        $IdJenjang = '3';
                    } elseif ($jenisDiklat == '5') {
                        $jenisJenjang = 'Penguji Tingkat Tiga';
                        $namaDiklat = 'Penguji Tingkat Tiga';
                        $KodeJenjang = 'PT3';
                        $IdJenjang = '5';
                    } elseif ($jenisDiklat == '7') {
                        $jenisJenjanFg = 'Penguji Tingkat Lima';
                        $namaDiklat = 'Penguji Tingkat Lima';
                        $KodeJenjang = 'PT5';
                        $IdJenjang = '7';
                    }

                    $data3 = [
                        'no_reg' => '' . $checkUser->kode_kota . '.' . $KodeJenjang . '.' . $checkUser->kode_kepegawaian . '.' . $checkUser->kode_otomatis . '',
                        'kode_jenjang' => $KodeJenjang,
                        'id_jenis_jenjang' => $IdJenjang,
                    ];

                    $result = $this->db->update('tbl_user', $data3, ['id_user' => $checkValid->id_user]);
                }

                $data2 = [
                    'id_user' => $checkValid->id_user,
                    'kode_pengajuan' => $checkValid->kode_pengajuan,
                    'id_kategori_diklat' => $checkValid->id_kategori_diklat,
                    'ne1' => $ne1,
                    'na1' => $na1,
                    'ne2' => $ne2,
                    'na2' => $na2,
                    'ne3' => $ne3,
                    'na3' => $na3,
                    'kumulatif' => $nilai,
                    'status' => $statusKelulusan,
                    'created_by' => 'System',
                    'created_date' => $datetime,
                ];
                $result = $this->db->insert('tbl_history_diklat', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function lihatNilai($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-approve';
        $data['title'] = 'Nilai Diklat PKB';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['brand'] = $this->M_diklat->selectHistoryDiklat(['kode_pengajuan' => $brand->kode_pengajuan])->row();
                $this->loadkonten(self::__folder . 'lihat_nilai', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function Cetak($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'master-diklat-approve';
        $data['title'] = 'Nilai Diklat PKB';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url($data['page']) . ">Data " . $data['title'] . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_diklat->selectById($id, ['Lolos']);
            if ($brand != null) {
                $data['datamaster'] = $brand;
                $this->load->view(self::__folder . 'print', $data);
            } else {
                echo "<script>alert('Data " . $data['title'] . " tidak tersedia.'); window.location = '" . base_url($data['page']) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('berkas')) {
            $berkas = $this->input->post('berkas');
            foreach ($berkas as $res) {
                $this->zip->read_file($res);
            }
            $this->zip->download('berkas-diklat' . time() . '.zip');
        }
    }

    public function removeFiles()
    {
        if ($this->input->is_ajax_request()) {
            $img = './upload/berkas_diklat/' . $_POST['folder'] . '/' . $_POST['image'];
            unlink($img);
        }
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_diklat->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");
                delete_files('upload/berkas_diklat/' . $checkValid->folder, true, false, 1);

                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
                $result = $this->db->update(self::__tableName2, ['deleted_date' => date('Y-m-d H:i:s')], ['kode_pengajuan' => $checkValid->kode_pengajuan]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
