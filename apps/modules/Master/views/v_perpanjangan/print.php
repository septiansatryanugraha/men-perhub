<html>
    <style>
        body {
            background: rgb(204,204,204); 
            font-family: "Bookman Old Style";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
            width: 29.7cm;
            height: 20cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:20px;
            font-size: 16px
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .container {
            max-width: 53em;
            margin: 0 auto;
            padding: 0 11%;
            width: 100%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:10px;
            font-size: 14px
        }
        .text2 {
            margin-top:-16px;
            margin-left: 390px;
            font-size: 14px
        }
        .text3 {
            margin-top:-17px;
            margin-left: 575px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5{
            font-size: 14px;
            margin-left: 450px;
        }
        .text6{
            font-size: 13px;
            margin-top: 50px;
            margin-left: 60px;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 20px;
            margin-left:280px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .kotak-panjang {
            width: 22em;
            height: 2.3em;
            margin-top: 130px;
            margin-left:0px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }

        .kotak-panjang p {
            margin-left:80px;
            margin-top:3px;
            font-size: 12px;

        }

        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-judul-kapol {
            margin-top:0px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:515px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        img {
            position:absolute;
            width:130px;
            padding-left:420px;
            margin-top:-30px;
        }
        .table-ok {
            font-family: "Bookman Old Style";
            font-size: 14px;
        }
        .table-ok, th, td {
            padding: 10px 0px 0px 50px;
            margin-left: -100px;
            text-align: left;
        }
    </style>
    <page size="A4">
        <div class="no_sertifikat"><b><center><?= $datamaster->no_sertifikat; ?></center></b></div>
        <div class="margin-judul-1"><b><center>KEMENTERIAN PERHUBUNGAN</center></b></div>
        <div class="margin-judul-2"><b><center>DIREKTORAT JENDERAL PERHUBUNGAN DARAT</center></b></div>
        <div class="margin-judul-3"><b><center>SERTIFIKAT KOMPETENSI PENGUJI BERKALA KENDARAAN BERMOTOR</center></b></div>
        <br><br>
        <div class="container">
            <div class="text1">Berdasarkan Hasil Berita Acara Uji Kompetensi Nomor :</div>
            <div class="text2"><?= $datamaster->no_ba; ?></div>
            <div class="text3">,Tanggal <?= date_indo(date($datamaster->tgl_ba)); ?></div>
            <table class="table-ok">
                <tr>
                    <td>Nama</td><td>:</td><td><?= $datamaster->nama_pemohon ?></td>
                </tr>
                <tr>
                    <td>Tempat dan Tanggal Lahir</td><td>:</td><td><?= $datamaster->tempat_lahir_pemohon ?>, <?= date_indo(date($datamaster->tgl_lahir_pemohon)) ?></td>
                </tr>
                <tr>
                    <td>Nomor Registrasi Penguji</td><td>:</td><td><?= $datamaster->no_reg_pemohon ?></td>
                </tr>
                <tr>
                    <td>Instansi</td><td>:</td><td><?= $datamaster->asal_instansi_pemohon ?></td>
                </tr>
            </table>
            <div class="gap1"></div>
            <p class="text4">Dinyatakan telah <b>LULUS UJI KOMPETENSI</b> sebagai Penguji Berkala Kendaraan Bermotor dan yang bersangkutan diberikan tugas, kewenangan dan tanggung jawab sebagai <b>Penguji Berkala Kendaraan Bermotor dengan Jenjang Kompetensi <?= $datamaster->jenis_jenjang ?>.</b></p>
            <p class="text4">Sertifikat ini berlaku selama <b>2 (tahun)</b> sejak tanggal ditetapkan dan wajib di perpanjang paling lambat <b>1 (satu) bulan sebelum masa berlaku habis.</b></p>
            <div class="spasi"></div>
            <p class="text5">Ditetapkan di : Jakarta</p>
            <p class="text5">Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?= date_indo(date('Y-m-d')); ?> </p>
            <p class="text6">Tanda Tangan :</p>
            <div class="kotak"><p>4 x 6</p></div>
            <div class="kotak-panjang"><p style="margin-left: 150px;">Perhatian</p><p align="center"style="margin-top: -13px; margin-left: 1px; width: 350px; font-size: 7px;">Apabila Penguji melakukan pelanggaran sebagaimana dalam ketentuan dalam PM.156 Tahun 2016 pasal 64 ayat(2) huruf d maka Sertifikat Kompetensi yang diberikan dapat dicabut.</p></div>
            <div class="margin-judul-kapol"><b>a.n DIREKTUR JENDERAL PERHUBUNGAN DARAT</b></div>
            <div class="margin-judul-kapol2"><b>DIREKTUR SARANA TRANSPORTASI JALAN</b></div>
            <div class="margin-nama-kapol"><b>IR.DANTO RESTYAWAN, MT</b></div>
            <div class="margin-nama-kapol2"><b>NIP. 19640829 199403 1 003</b></div>
        </div>
    </page>
    <script>
        window.print();
    </script>
</html>