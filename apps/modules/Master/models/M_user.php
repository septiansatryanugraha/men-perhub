<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model
{
    const __tableName = 'tbl_user';
    const __tableId = 'id_user';
    const __tableId2 = 'email';
    const __tableDinas = 'tbl_cat_opd';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = [])
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_user DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectById2($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectHistoryDiklat($id)
    {
        $sql = "SELECT * FROM tbl_history_diklat
                WHERE deleted_date IS NULL AND id_user = '{$id}' ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectHistoryJenjang($id)
    {
        $sql = "SELECT * FROM tbl_history_jenjang
                WHERE deleted_date IS NULL AND id_user = '{$id}' ";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function checkEmail($data)
    {
        $this->db->select('email');
        $this->db->from('tbl_user');
        $this->db->where('deleted_date IS NULL');
        $this->db->where('email', $data['email']);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function selectDinas()
    {
        $this->db->from(self::__tableDinas);
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->result();
    }
}
