<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Tambah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Tambah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-pencil-square"></i> Master Data Administrator</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form"> 
                    <form class="form-horizontal" id="form-tambah" method="POST">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama User</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="nama_user" placeholder="nama user" name="nama" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" placeholder="email user" name="email" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="username" placeholder="username" name="username" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" id="password-field" placeholder="password"  name="password" aria-describedby="sizing-addon2"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Grup</label>
                                <div class="col-sm-4">
                                    <select name="grup_id" class="form-control select2" aria-describedby="sizing-addon2">
                                        <?php foreach ($datagrup as $data) { ?>
                                            <option value="<?= $data->grup_id ?>"><?= $data->nama_grup; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div id="buka">
                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                        <a href="<?= base_url('user'); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                    </div>
                                    <div id="btn_loading">
                                        <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>              
    </div>
</div>

<script type="text/javascript">
    $('#form-tambah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var nama_user = $("#nama_user").val();
            var nama_user = nama_user.trim();
            if (nama_user.length == 0) {
                error++;
                message = "Nama User wajib di isi.";
            }
        }
        if (error == 0) {
            var email = $("#email").val();
            var email = email.trim();
            if (email.length == 0) {
                error++;
                message = "Email wajib di isi.";
            } else if (!cekemail(email)) {
                error++;
                message = " Format Email tidak sesuai (admin@gmail.com). ";
            }
        }
        if (error == 0) {
            var username = $("#username").val();
            var username = username.trim();
            if (username.length == 0) {
                error++;
                message = "Username wajib di isi.";
            }
        }
        if (error == 0) {
            var password = $("#password-field").val();
            var password = password.trim();
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }
        }
        if (error == 0) {
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('save-user'); ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout(location.reload.bind(location), 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    // untuk select2 original
    $(function () {
        $(".select2").select2({});
    });

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>