<style type="text/css">
    #btn_loading {
        display: none;
    }
</style>
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        Ubah <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <?= $breadcrumb ?>
            <li><i class="fa fa-angle-right"></i>Ubah <?= $title ?></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-database"></i> Hak Akses</div>
                    <div class="actions">
                        <a href="<?= base_url($page) ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        <form id="form-update" method="post">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width='5%'><center>#</center></th>
                                        <th width='30%'>Menu Dashboard</th>
                                        <th width='20%'>Sub Menu</th>
                                        <th width='7%'><center>View</center></th>							
                                        <th width='7%'><center>Add</center></th>
                                        <th width='7%'><center>Edit</center></th>
                                        <th width='7%'><center>Delete</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($privilege as $row) {
                                        $view_pos = strpos($row->menu_file, "iew");
                                        $view = ($row->view == 1 and $view_pos != 0) ? 'checked=checked' : '';
                                        $viewavail = ($view_pos != 0) ? '' : 'disabled=disabled';

                                        $add_pos = strpos($row->menu_file, "dd");
                                        $add = ($row->add == 1 and $add_pos != 0) ? 'checked=checked' : '';
                                        $addavail = ($add_pos != 0) ? '' : 'disabled=disabled';

                                        $edit_pos = strpos($row->menu_file, "dit");
                                        $edit = ($row->edit == 1 and $edit_pos != 0) ? 'checked=checked' : '';
                                        $editavail = ($edit_pos != 0) ? '' : 'disabled=disabled';

                                        $del_pos = strpos($row->menu_file, "el");
                                        $del = ($row->del == 1 and $del_pos != 0) ? 'checked=checked' : '';
                                        $delavail = ($del_pos != 0) ? '' : 'disabled=disabled';

                                        echo "
										<tr class='odd gradeX'>
										<input type='hidden' name='id_menu[]' value='$row->menus' />
										<input type='hidden' name='grup_id' />

										<td><center>$no</center></td>
										<td>$row->nama_menu</td>	
										<td><b>$row->menuparent</b></td>
										<td class='center'><center><input type='checkbox'  value='1' name='view[$row->menus]' $view $viewavail /></center></td>
										<td class='center'><center><input type='checkbox' value='1' name='add[$row->menus]' $add $addavail /></center></td>
										<td class='center'><center><input type='checkbox'  value='1' name='edit[$row->menus]' $edit $editavail /></center></td>
										<td class='center'><center><input type='checkbox'  value='1' name='del[$row->menus]' $del $delavail /></center></td>
										</tr>";
                                        $no++;
                                    }

                                    ?>				
                                </tbody>
                            </table>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <div id="buka">
                                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Simpan</button>
                                            <a href="<?= base_url($page); ?>" type="button" class="btn red ajaxify klik">Kembali</a>
                                        </div>
                                        <div id="btn_loading">
                                            <button type="submit" class="btn green" disabled><i class='fa fa-refresh fa-spin'></i> Wait...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>	
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#form-update').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            swal({
                title: "Simpan Data?",
                text: "Yakin Memproses Data Ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $.ajax({
                    method: 'POST',
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    url: '<?= base_url('update-hak-akses') . '/' . $groupname->grup_id; ?>',
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("#buka").show();
                    $("#btn_loading").hide();
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                        setTimeout("window.location='<?= base_url($page); ?>'", 500);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                });
            });
        })
    });
</script>