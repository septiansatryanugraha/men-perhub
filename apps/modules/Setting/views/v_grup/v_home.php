<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <h3 class="page-title">
        <?= (isset($title)) ? $title : ''; ?>
        <small><?= (isset($subtitle)) ? $subtitle : ''; ?></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><i class="fa fa-home"></i><a class="ajaxify klik" href="<?= base_url() ?>Dashboard">Home</a></li>
            <li><i class="fa fa-angle-right"></i><?= $title ?></li>
        </ul>
    </div>
    <!-- /.box-header -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-list"></i> Data <?= $title ?></div>
                    <?php if ($accessAdd > 0) { ?>
                        <div class="actions">
                            <a href="<?= base_url('add-grup') ?>" class="btn default btn-sm ajaxify klik"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                    <?php } ?>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Grup</th>
                                    <th>Modul Kategori</th>
                                    <th>Menu Akses</th>
                                    <th>Deskripsi</th>
                                    <th style="width:200px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($dataWarna as $data) {
                                    $grupId = $data->grup_id;
                                    $dataakses = $this->M_grup->aksesGrup($grupId);

                                    ?>
                                    <tr>
                                        <td><?= $no; ?></td>
                                        <td><?= $data->nama_grup; ?></td>
                                        <td><?php
                                            foreach ($dataakses as $data1) {
                                                if ($data1->view == 1) {

                                                    ?><?= '' . $data1->menuparent . '<br>'; ?><?php
                                                }
                                            }

                                            ?></td>
                                        <td><?php
                                            foreach ($dataakses as $data1) {
                                                if ($data1->view == 1) {

                                                    ?><?= '<li>' . $data1->nama_menu . '</li>'; ?><?php
                                                }
                                            }

                                            ?></td>
                                        <td><?= $data->deskripsi; ?></td>
                                        <td>
                                            <div class='btn-group'>
                                                <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>
                                                <ul class='dropdown-menu align-left pull-right'>
                                                    <?php if ($accessEdit > 0) { ?>
                                                        <li><a href='<?= base_url('edit-grup') ?>/<?= $grupId; ?>' class='klik ajaxify'><i class='fa fa-edit'></i> Ubah</a></li>
                                                        <li><a href='<?= base_url('edit-hak-akses') ?>/<?= $grupId; ?>' class='klik ajaxify'><i class='fa fa-cogs'></i> Konfigurasi Akses</a></li>
                                                    <?php } ?>
                                                    <?php if ($accessDel > 0) { ?>
                                                        <li><a href='#' class='hapus-grup' data-toggle='tooltip' data-placement='top' data-id='<?= $grupId; ?>'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>
                                                        <?php } ?>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var save_method; //for save method string
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#table').DataTable({
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            }
        });
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-grup", function () {
        var grup_id = $(this).attr("data-id");
        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?= base_url('hapus-grup'); ?>",
                data: "grup_id=" + grup_id,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + grup_id + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        swal("Success", result.pesan, "success");
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    // hapus_berhasil();
                    setTimeout(location.reload.bind(location), 400);
                }
            });
        });
    });
</script>