<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup extends AUTH_Controller
{
    const __title = 'Grup Administrator ';
    const __kode_menu = 'user-grup';
    const __folder = 'v_grup/';
    const __tableName = 'grup';
    const __tableId = 'grup_id';
    const __model = 'M_grup';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
            $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['accessEdit'] = $accessEdit->menuview;
            $data['accessDel'] = $accessDel->menuview;
            $data['dataWarna'] = $this->M_grup->getData();
            $this->loadkonten(self::__folder . 'v_home', $data);
        }
    }

    public function Add()
    {
        $data['userdata'] = $this->session->userdata();
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">" . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten(self::__folder . 'v_tambah-grup', $data);
        }
    }

    public function prosesTambah()
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $namaGrup = $this->input->post('nama_grup');
        $deskripsi = $this->input->post('deskripsi');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaGrup) == 0) {
                $errCode++;
                $errMessage = "Nama Grup wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nama_grup' => $namaGrup,
                    'deskripsi' => $deskripsi,
                    'created_by' => $username,
                    'created_date' => $datetime,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = self::__title;
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">" . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_grup->selectById($id);
            if ($brand != null) {
                $data['datagrup'] = $brand;
                $this->loadkonten(self::__folder . 'v_update-grup', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $namaGrup = $this->input->post('nama_grup');
        $deskripsi = $this->input->post('deskripsi');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_grup->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaGrup) == 0) {
                $errCode++;
                $errMessage = "Nama Grup wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'nama_grup' => $namaGrup,
                    'deskripsi' => $deskripsi,
                    'updated_by' => $username,
                    'updated_date' => $datetime,
                ];
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function hakAkses($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__kode_menu;
        $data['title'] = 'Hak Akses';
        $data['breadcrumb'] = "<li><i class='fa fa-angle-right'></i><a class='ajaxify' href=" . base_url(self::__kode_menu) . ">" . self::__title . "</a></li>";
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $brand = $this->M_grup->selectById($id);
            if ($brand != null) {
                $this->form_validation->set_rules('id_menu', 'Menu', '');
                $data['groupname'] = $brand;
                $data['privilege'] = $this->M_grup->aksesGrup($id);
                $this->loadkonten(self::__folder . 'v_hak_akses', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdateHakAkses($id)
    {
        $errCode = 0;
        $errMessage = "";

        $menu = $this->input->post('id_menu');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_grup->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (!is_array($menu)) {
                $errCode++;
                $errMessage = "Menu tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                // reset auto increment
                $this->db->delete('menu_akses', ['grup_id' => $id]);
                $this->db->query("ALTER TABLE menu_akses DROP id_menuakses");
                $this->db->query("ALTER TABLE menu_akses ADD id_menuakses INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");

                $menu = $this->input->post('id_menu');
                foreach ($menu as $row => $id_menu) {
                    $view = isset($_REQUEST['view'][$id_menu]);
                    $add = isset($_REQUEST['add'][$id_menu]);
                    $edit = isset($_REQUEST['edit'][$id_menu]);
                    $del = isset($_REQUEST['del'][$id_menu]);

                    $doSaveData = true;
                    if ($view == 0 && $add == 0 && $edit == 0 && $del == 0) {
                        $doSaveData = false;
                    }
                    if ($doSaveData) {
                        $data = [
                            'id_menu' => $id_menu,
                            'grup_id' => $id,
                            'view' => $view,
                            'add' => $add,
                            'edit' => $edit,
                            'del' => $del,
                        ];
                        $this->db->insert('menu_akses', $data);
                    }
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di simpan'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_grup->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $result = $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data berhasil di hapus'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
