<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_config extends CI_Model
{
    const __tableName = 'tbl_config_menu';
    const __tableId = 'id_config';

    public function getData()
    {
        $data = $this->db->get(self::__tableName);
        $this->db->where('deleted_date IS NULL');

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
