<style>
    #btn_loading {
        display: none;
    }
    .margin-callout{
        margin-left: 20px;
        margin-top: 20px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-9">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengajuan Perpanjangan Kompetensi</h3>
                </div>
                <div class="callout callout-danger margin-callout">
                    <h4>Perhatikan dengan baik !</h4>
                    <p>Pastikan sebelum melakukan upload dokumen Perpanjangan Kompetensi harus sesuai dengan persyaratan yang terlampir karena dokumen yang sesuai dengan persyaratan akan kami verifikasi :</p>
                    <p>1. Upload pengantar permohonan dari Direktur/Kadis/Ka. BPTD/Ka. UPT/Pimpinan Pengujian APM/Swasta untuk perpanjangan sertifikat.</p>
                    <p>2. Upload surat pernyataan dari Direktur/Kadis/Ka. BPTD/Ka. UPT/Pimpinan Pengujian APM/Swasta bahwa ybs sedang ditugaskan di bidang pengujian. </p>
                    <p>3. Upload Penilaian Prestasi kerja bernilai Baik 1 tahun terakhir/DP3. </p>
                    <p>4. Upload DRH. </p>
                    <p>5. Upload sertifikat/ dokumen lain di bidang PKB 1 tahun terakhir yang pernah diikuti. </p>
                </div>     
                <form class="form-horizontal" id="form-pengajuan" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jenis Jenjang</label>
                            <div class="col-sm-4">
                                <select name="id_jenis_jenjang" class="form-control select-jenis" id="id_jenis_jenjang">
                                    <option></option>
                                    <?php foreach ($jenis as $data) { ?>
                                        <option value="<?= $data->id ?>"><?= $data->jenjang; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Pengantar Permohonan<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="pengantar_permohonan"  onchange="return cekMandatoryPdf('pengantar_permohonan')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Surat Pernyataan<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="surat_pernyataan"  onchange="return cekMandatoryPdf('surat_pernyataan')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Penilaian Prestasi<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="penilaian_prestasi"  onchange="return cekMandatoryPdf('penilaian_prestasi')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">DRH<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="drh"  onchange="return cekMandatoryPdf('drh')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Sertifikat / Dokumen bidang PKB<sup style="color:red; font-weight:bold;">*</sup></label>
                            <div class="col-sm-5">
                                <input type="file"  name="others[]" id="dokum_bidang"  onchange="return cekMandatoryPdf('dokum_bidang')"/>
                            </div>
                            <div class="col-sm-2">
                                <small class="label pull-center bg-red">format .pdf | max 300 kb </small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka">
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                            <div id="btn_loading">
                                <button type="submit" class="btn btn-success btn-flat" disabled><i class='fa fa-refresh fa-spin'></i> Tunggu...</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /.box -->
            </div>
            <!-- /.row -->
        </div>
    </div>
</section> 

<script type="text/javascript">
    $('#form-pengajuan').submit(function (e) {
        e.preventDefault();
        var error = 0;
        var message = "";

        if (error == 0) {
            var id_jenis_jenjang = $("#id_jenis_jenjang").val();
            var id_jenis_jenjang = id_jenis_jenjang.trim();
            if (id_jenis_jenjang.length == 0) {
                error++;
                message = "Jenis jenjang wajib di isi.";
            }
        }
        if (error == 0) {
            var pengantar_permohonan = $("#pengantar_permohonan").val();
            var pengantar_permohonan = pengantar_permohonan.trim();
            if (pengantar_permohonan.length == 0) {
                error++;
                message = "Surat Permohonan wajib di isi.";
            }
        }
        if (error == 0) {
            var surat_pernyataan = $("#surat_pernyataan").val();
            var surat_pernyataan = surat_pernyataan.trim();
            if (surat_pernyataan.length == 0) {
                error++;
                message = "Surat Pernyataan wajib di isi.";
            }
        }
        if (error == 0) {
            var drh = $("#drh").val();
            var drh = drh.trim();
            if (drh.length == 0) {
                error++;
                message = "File DRH wajib di isi.";
            }
        }
        if (error == 0) {
            var dokum_bidang = $("#dokum_bidang").val();
            var dokum_bidang = dokum_bidang.trim();
            if (dokum_bidang.length == 0) {
                error++;
                message = "File Sertifikat / Dokumen Bidang wajib di isi.";
            }
        }
        if (error == 0) {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('save-perpanjangan'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout("window.location='<?= base_url("data-perpanjangan"); ?>'", 1000);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            });
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

    function cekMandatoryPdf(variable) {
        var fileInput = document.getElementById(variable).value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                // swal("Peringatan", "File harus format .pdf", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById(variable).value = '';
                return false;
            }
            var ukuran = document.getElementById(variable);
            if (ukuran.files[0].size > 307200)  // validasi ukuran size file
            {
                // swal("Peringatan", "File harus maksimal 5MB", "warning");
                toastr.error('File harus maksimal 300 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    $(function () {
        $(".select-jenis").select2({
            placeholder: " -- Pilih jenis perpanjangan jenjang -- "
        });
    });
</script>