</div><!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <p>Dashboard User</p>
    </div>
    Copyright &copy; <?= date("Y"); ?>&nbsp;
    E-Kopetensi. All rights reserved - Develop by <a href="#" target="_blank">Kementerian Perhubungan
    </a>
</footer>
</div><!-- ./wrapper -->
<!-- Bootstrap 3.3.2 JS -->
<script src="<?= base_url('admin-lte/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('admin-lte/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>admin-lte/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url('admin-lte/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?= base_url('admin-lte/plugins/fastclick/fastclick.min.js') ?>'></script>
<!-- AdminLTE App -->
<script src="<?= base_url('admin-lte/dist/js/app.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('admin-lte/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ajax.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/toastr/toastr.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/highchart/highcharts.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/highchart/modules/exporting.js"></script>
<script src="<?= base_url(); ?>admin-lte/plugins/highchart/modules/offline-exporting.js"></script>