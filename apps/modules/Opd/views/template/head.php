<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $title . " - E-Kompetensi"; ?></title>
        <link rel="icon" href="<?= base_url() ?>/assets/tambahan/gambar/logo-dishub.png">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?= base_url('admin-lte/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
        <!-- Font Awesome Icons -->
        <link href="<?= base_url('admin-lte/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('admin-lte/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('admin-lte/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>admin-lte/plugins/datatables/dataTables.bootstrap.css">
        <!--datepicker-->
        <link rel="stylesheet" type="text/css" href="<?= base_url('admin-lte/plugins/datepicker/datepicker3.css') ?>">
        <link rel="stylesheet" href="<?= base_url(); ?>admin-lte/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>admin-lte/plugins/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="<?= base_url(); ?>admin-lte/plugins/toastr/toastr.css">
        <!-- jQuery 2.2.3 -->
        <script src="<?= base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <style type="text/css">
            /* START TOOLTIP STYLES */
            [tooltip] {
                position: relative; /* opinion 1 */
            }
            /* Applies to all tooltips */
            [tooltip]::before,
            [tooltip]::after {
                text-transform: none; /* opinion 2 */
                font-size: .9em; /* opinion 3 */
                line-height: 1;
                user-select: none;
                pointer-events: none;
                position: absolute;
                display: none;
                opacity: 0;
            }
            [tooltip]::before {
                content: '';
                border: 5px solid transparent; /* opinion 4 */
                z-index: 1001; /* absurdity 1 */
            }
            [tooltip]::after {
                content: attr(tooltip); /* magic! */
                /* most of the rest of this is opinion */
                font-family: Helvetica, sans-serif;
                text-align: center;
                /* 
                  Let the content set the size of the tooltips 
                  but this will also keep them from being obnoxious
                */
                min-width: 3em;
                max-width: 21em;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 1ch 1.5ch;
                border-radius: .3ch;
                box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
                background: #333;
                color: #fff;
                z-index: 1000; /* absurdity 2 */
            }
            /* Make the tooltips respond to hover */
            [tooltip]:hover::before,
            [tooltip]:hover::after {
                display: block;
            }
            /* don't show empty tooltips */
            [tooltip='']::before,
            [tooltip='']::after {
                display: none !important;
            }
            /* FLOW: UP */
            [tooltip]:not([flow])::before,
            [tooltip][flow^="up"]::before {
                bottom: 150%;
                border-bottom-width: 0;
                border-top-color: #333;
            }
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::after {
                bottom: calc(150% + 5px);
            }
            [tooltip]:not([flow])::before,
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::before,
            [tooltip][flow^="up"]::after {
                left: 50%;
                transform: translate(-50%, -.5em);
            }
            /* KEYFRAMES */
            @keyframes tooltips-vert {
                to {
                    opacity: .9;
                    transform: translate(-50%, 0);
                }
            }
            @keyframes tooltips-horz {
                to {
                    opacity: .9;
                    transform: translate(0, -50%);
                }
            }
            /* FX All The Things */ 
            [tooltip]:not([flow]):hover::before,
            [tooltip]:not([flow]):hover::after,
            [tooltip][flow^="up"]:hover::before,
            [tooltip][flow^="up"]:hover::after,
            [tooltip][flow^="down"]:hover::before,
            [tooltip][flow^="down"]:hover::after {
                animation: tooltips-vert 300ms ease-out forwards;
            }
            [tooltip][flow^="left"]:hover::before,
            [tooltip][flow^="left"]:hover::after,
            [tooltip][flow^="right"]:hover::before,
            [tooltip][flow^="right"]:hover::after {
                animation: tooltips-horz 300ms ease-out forwards;
            }
            #spinner {
                position:fixed;
                width: 80px;
                height: 80px;
                display: inline-block;
                margin-top: 20%;
                opacity: 0.8;
                margin-left: 50%;
                background: #74a6f2;
                padding: 10px;
                z-index: 9999;
                border-radius: 10px;
            }
            #spinner div {
                width: 6%;
                height: 16%;
                background: #FFF;
                position: absolute;
                left: 49%;
                top: 31%;
                opacity: 0;
                -webkit-border-radius: 50px;
                -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.2);
                -webkit-animation: fade 1s linear infinite;
            }
            @-webkit-keyframes fade {
                from {opacity: 1;}
                to {opacity: 0.25;}
            }
            #spinner div.bar1 {
                -webkit-transform:rotate(0deg) translate(0, -130%);
                -webkit-animation-delay: 0s;
            }    
            #spinner div.bar2 {
                -webkit-transform:rotate(30deg) translate(0, -130%); 
                -webkit-animation-delay: -0.9167s;
            }
            #spinner div.bar3 {
                -webkit-transform:rotate(60deg) translate(0, -130%); 
                -webkit-animation-delay: -0.833s;
            }
            #spinner div.bar4 {
                -webkit-transform:rotate(90deg) translate(0, -130%); 
                -webkit-animation-delay: -0.7497s;
            }
            #spinner div.bar5 {
                -webkit-transform:rotate(120deg) translate(0, -130%); 
                -webkit-animation-delay: -0.667s;
            }
            #spinner div.bar6 {
                -webkit-transform:rotate(150deg) translate(0, -130%); 
                -webkit-animation-delay: -0.5837s;
            }
            #spinner div.bar7 {
                -webkit-transform:rotate(180deg) translate(0, -130%); 
                -webkit-animation-delay: -0.5s;
            }
            #spinner div.bar8 {
                -webkit-transform:rotate(210deg) translate(0, -130%); 
                -webkit-animation-delay: -0.4167s;
            }
            #spinner div.bar9 {
                -webkit-transform:rotate(240deg) translate(0, -130%); 
                -webkit-animation-delay: -0.333s;
            }
            #spinner div.bar10 {
                -webkit-transform:rotate(270deg) translate(0, -130%); 
                -webkit-animation-delay: -0.2497s;
            }
            #spinner div.bar11 {
                -webkit-transform:rotate(300deg) translate(0, -130%); 
                -webkit-animation-delay: -0.167s;
            }
            #spinner div.bar12 {
                -webkit-transform:rotate(330deg) translate(0, -130%); 
                -webkit-animation-delay: -0.0833s;
            }
            .tulisan {
                margin-top: 46px;
                margin-left: 3px;
                position: absolute;
                color:white;
                font-family: verdana;
                -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.2);
                -webkit-animation: fade 1s linear infinite;
            }
        </style>
    <div id="spinner" style="display: none">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
        <div class="bar6"></div>
        <div class="bar7"></div>
        <div class="bar8"></div>
        <div class="bar9"></div>
        <div class="bar10"></div>
        <div class="bar11"></div>
        <div class="bar12"></div>
        <p class="tulisan">Loading</p>
    </div>