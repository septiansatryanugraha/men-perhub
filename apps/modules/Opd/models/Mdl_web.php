<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_web extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function selectMenu($idConfig = null)
    {
        $sql = "SELECT * FROM  tbl_config_menu WHERE deleted_date IS NULL";
        if (strlen($idConfig) > 0) {
            $sql .= " AND id_config = '{$idConfig}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }
}
