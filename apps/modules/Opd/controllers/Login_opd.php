<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_opd extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_login');
    }

    public function index()
    {
        if ($this->lib->login() != "") {
            redirect('Login_opd/logout');
        } else {

            $this->form_validation->set_rules('_password', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_error_delimiters('<p style="color:red;">', '</p>');


            if ($this->form_validation->run() == TRUE) {
                $email = $this->input->post('_email');
                $password = base64_encode($this->input->post('_password'));
                $check = $this->Mdl_login->login(['email' => $email]), ['password' => $password]);


                if ($check == TRUE) {
                    foreach ($check as $user) {
                        if ($user->status == "Non aktif") {
                            $this->session->set_flashdata('error_msg', 'Maaf akun belum aktif');
                            redirect('log-opd');
                        } else {
                            $this->session->set_userdata([
                                'email' => $user->email,
                                'id' => $user->id_user,
                                'id_jenis_jenjang' => $user->id_jenis_jenjang,
                                'id_status_kepegawaian' => $user->id_status_kepegawaian,
                                'nama_lengkap' => $user->nama_lengkap,
                                'nik' => $user->nik,
                                'jabatan' => $user->jabatan,
                                'dinas' => $user->dinas,
                                'nama' => $user->nama,
                                'password' => $user->password
                            ]);

                            $this->Mdl_login->update_user($email, ['last_login_user' => date('Y-m-d H:i:s')]);
                            redirect('beranda');
                        }
                    }
                } else {

                    $this->session->set_flashdata('error_msg', 'Username atau Password salah');
                    redirect('log-opd');
                }
            } else {

                $data = [
                    'title' => 'Login',
                    'kontak' => $this->Mdl_login->selekKontak(),
                ];

                $this->load->view('login', $data);
            }
        }
    }

    function lupa()
    {
        $data = [
            'title' => 'Lupa Password',
            'style' => '<style type="text/css">body {background-color: #DADADA;}.ui.two.column.centered.grid {margin-top: 1px;}</style>'
        ];
        $this->load->view('lupa-password', $data);
    }

    function logout()
    {
        if ($this->lib->login() != "") {
            $this->lib->logout();
            redirect('Homepage');
        } else {
            redirect('Opd/Dashboard');
        }
    }
}
