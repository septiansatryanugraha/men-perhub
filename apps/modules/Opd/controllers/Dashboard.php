<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_products');
        $this->load->model('M_user');
    }

    public function history()
    {
        $idUser = $this->session->userdata('id');
        $data['user'] = $this->M_user->selectById($idUser);

        $view = $this->input->post('view');
        if ($view != '') {
            $update_status = $this->Mdl_products->updateStatusHistory($idUser);
        }
        $selek_notif = $this->Mdl_products->selectNotifHistory($idUser);

        if (!empty($selek_notif)) {
            foreach ($selek_notif as $data) {
                $output .= '
    	 <li><a class="ajaxify" href="' . base_url() . 'data-history">Kode pengajuan :
                       ' . $data->kode_pengajuan . '
     	<p>' . $data->keterangan_status . '</p></a>
                  </li><hr>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a>
                  		</li>';
        }

        $count = $this->Mdl_products->totalCountHistory($idUser);
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function notifPembayaran()
    {
        $idUser = $this->session->userdata('id');
        $data['user'] = $this->M_user->selectById($idUser);

        $view = $this->input->post('view');
        if ($view != '') {
            $update_status = $this->Mdl_products->updateStatusPembayaran($idUser);
        }
        $selek_notif = $this->Mdl_products->selectNotifPembayaran($idUser);

        if (!empty($selek_notif)) {
            foreach ($selek_notif as $data) {
                $output .= '
    	 <li><a class="ajaxify" href="' . base_url() . 'data-pembayaran">Kode pengajuan :
                       ' . $data->kode_pengajuan . '
     	<p>' . $data->keterangan . '</p></a>
                  </li><hr>';
            }
        } else {
            $output .= '<li><a href="#"><i class="fa fa-remove text-red"></i> tidak ada notifikasi</a>
                  		</li>';
        }

        $count = $this->Mdl_products->totalCountPembayaran($idUser);
        $data = [
            'notification' => $output,
            'unseen_notification' => $count
        ];

        echo json_encode($data);
    }

    public function index()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('homepage');
        } else {
            $data = [
                'sessid' => $this->session->userdata('id'),
                'nama' => $this->session->userdata('nama_lengkap'),
                'title' => "Dashboard",
                'judul' => "Dashboard",
                'deskripsi' => "Manage Data Dashboard",
            ];

            $idUser = $this->session->userdata('id');
            $data['user'] = $this->M_user->selectById($idUser);

            $data['total_perpanjangan'] = $this->Mdl_products->totalData('id_user', $idUser);
            $data['total_diklat'] = $this->Mdl_products->totalData2('id_user', $idUser);
            $data['total_jenjang'] = $this->Mdl_products->totalData3('id_user', $idUser);
            $data['history_diklat'] = $this->Mdl_products->selectHistoryDiklat($idUser);
            $data['history_jenjang'] = $this->Mdl_products->selectHistoryJenjang($idUser);

            $data['konten'] = $this->Mdl_products->selekKonten();

            $this->load->view('template/head', $data);
            $this->load->view('template/topbar', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('_heading/_headerContent', $data);
            $this->load->view('dashboard', $data);
            $this->load->view('template/js');
            $this->load->view('template/foot');
        }
    }
}
